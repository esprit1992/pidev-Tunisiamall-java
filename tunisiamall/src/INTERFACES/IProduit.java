package INTERFACES;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import ENTITY.Produit;
import java.util.ArrayList;

/**
 *
 * @author siwar-pc
 */
public interface IProduit {
    int addProd(Produit p);
    void deleteprod(Produit p);
    void getprod();
    ArrayList<Produit> getprods();
    void getprodsboutique();
    void getprodscat();
    void getprodsnom();
}
