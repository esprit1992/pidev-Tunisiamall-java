/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import ENTITY.Pack;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author siwar-pc
 */
public interface Ipack {
    void addpack(Pack p);
    void deletepack(Pack p);
    void updatepack(Pack lml);
  ObservableList<Pack>  getpack();
}
