/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tunisiamallMain;

import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ENTITY.Utilisateur;
import com.github.plushaze.traynotification.notification.Notification;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javafx.animation.PauseTransition;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 *
 * @author user
 */
public class Tunisiamall extends Application {

    private Stage stage;
    private Utilisateur loggedUser;
    private static Tunisiamall instance;
    private Scene scene;
    private Map<Integer, Integer> panier;
    //listeImageToUpload.put(file.getName(), file.getPath());
    public Tunisiamall() throws IOException, InterruptedException {
        this.panier = new HashMap<Integer, Integer>();
        instance = this;
        loggedUser = null;
        scene = new Scene(FXMLLoader.load(getClass().getResource("/GUI/login_1.fxml")));

    }

    public static Tunisiamall getInstance() {
        return instance;
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;

        stage.setScene(this.scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.centerOnScreen();
        stage.show();

    }

    public void changescene(Scene scene) {
        this.scene = scene;
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void setLoggedUser(Utilisateur loggedUser) {
        this.loggedUser = loggedUser;
    }

    public Utilisateur getLoggedUser() {
        return loggedUser;
    }

    public Map<Integer, Integer> getPanier() {
        return panier;
    }
    
    
    public void addProduitToPanier(int idProduit,int Quantite)
    {
        affichierPanier();
        if(panier.containsKey(idProduit))
            panier.put(idProduit, panier.get(idProduit) + Quantite);
        else
            panier.put(idProduit,Quantite);
        affichierPanier();
    }
    public void affichierPanier()
    {
      for (int mapKey : panier.keySet()) 
      {
          System.out.println("mapKey:"+mapKey+"=>"+panier.get(mapKey));
      }
    }

}
