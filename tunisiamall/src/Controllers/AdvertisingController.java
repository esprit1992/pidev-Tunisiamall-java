/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DPack;
import Dao.dCarte;
import ENTITY.Boutique;
import ENTITY.Carte_fidelite;
import ENTITY.Pack;
import ENTITY.Utilisateur;
import animation.FadeInLeftTransition;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author siwar-pc
 */
public class AdvertisingController implements Initializable {

    @FXML
    private TableView<Pack> pack_tab;
    @FXML
    private TableColumn<Pack, Integer> id;
    @FXML
    private TableColumn<Pack, Double> price;
    @FXML
    private TableColumn<Pack, Integer> dispo;
    @FXML
    private TableColumn<Pack, String> description;
  
    @FXML
    private Button btnedit;
    
    @FXML
    private TextArea txtdesc;
    @FXML
    private TextField txtprice;
    @FXML
    private TextField txtid;
    @FXML
    private CheckBox check_disp;
    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Button btnBack;
    @FXML
    private Text textNameUser;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private Text TextLocation;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());

             listMenuOfGestion.getItems().addAll("  Manage advertising");
        listMenuOfGestion.getSelectionModel().select(0);
             
        TextLocation.setText("Manage advertising");
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        
    });
        btnedit.setOpacity(0);
       
        afficherinfo(null);
        pack_tab.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> afficherinfo(newValue));
        // TODO
        afficher_pack();
        txtid.setDisable(true);
    }

    public void afficher_pack() {
        DPack dpack = new DPack();
        System.out.print(dpack.getpack().size());
        id.setCellValueFactory(
                new PropertyValueFactory<Pack, Integer>("id"));

        price.setCellValueFactory(
                new PropertyValueFactory<Pack, Double>("prixjour"));

        dispo.setCellValueFactory(
                new PropertyValueFactory<Pack, Integer>("disponible"));
        description.setCellValueFactory(
                new PropertyValueFactory<Pack, String>("description"));

        pack_tab.setItems(dpack.getpack());

    }

    public void afficherinfo(Pack p) {
        if (p != null) {
          
            new FadeInLeftTransition(btnedit).play();
            txtprice.setDisable(false);
            txtdesc.setDisable(false);
            

            String s = "" + p.getId();
            txtid.setText(s);
            txtid.setDisable(true);
            String prix = "" + p.getPrixjour();
            txtprice.setText(prix);
            if (p.getDisponible() == 1) {
                check_disp.setSelected(true);
            } else {
                check_disp.setSelected(false);
            }

            txtdesc.setText(p.getDescription());

        } else {
            txtid.setText("");
            txtprice.setText("");
            check_disp.setSelected(false);
            txtdesc.setText("");

        }
    }

    public boolean isnumeric(String s) {
        try {
            double prix = Double.parseDouble(s);

        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

  

    public void reset() {
        DPack dp = new DPack();

        txtid.setText("");
        txtprice.setText("");
        check_disp.setSelected(false);
        txtdesc.setText("");
 ;
        btnedit.setOpacity(0);

        txtid.setDisable(true);
        txtprice.setDisable(true);
            txtdesc.setDisable(true);

    }

    @FXML
    private void editAction(MouseEvent event) {
        Pack p = new Pack();
        DPack dp = new DPack();
        p.setId(pack_tab.getSelectionModel().getSelectedItem().getId());
        p.setPrixjour(Double.parseDouble(txtprice.getText()));

        p.setDescription(txtdesc.getText());
        if (check_disp.isSelected()) {
            p.setDisponible(1);
        } else {
            p.setDisponible(0);
        }
        dp.updatepack(p);
        TrayNotification tray = new TrayNotification("Success", "Pack has been modified", Notifications.SUCCESS);
        tray.showAndDismiss(Duration.seconds(3));
        afficher_pack();
        reset();
    }

    @FXML
    private void deleteAction(MouseEvent event) {
        Pack p = new Pack();
        DPack dp = new DPack();
        p.setId(pack_tab.getSelectionModel().getSelectedItem().getId());

        dp.deletepack(p);
        TrayNotification tray = new TrayNotification("Success", "Pack has been deleted", Notifications.SUCCESS);
        tray.showAndDismiss(Duration.seconds(3));
        afficher_pack();
        reset();
    }

    @FXML
    private void mouseExit(MouseEvent event) {
 
    
}

    @FXML
    private void ButtonBackAction(MouseEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuAdmin.fxml"))));
    }
}