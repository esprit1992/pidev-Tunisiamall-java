/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author siwar-pc
 */
public class ProduitExemple_1Controller implements Initializable {
    @FXML
    private AnchorPane APproduit;
    @FXML
    private Text TXTproduitName;
    @FXML
    private Text TXTQuantite;
    @FXML
    private Label Idproduit;
    @FXML
    private Button pbGoToProduit;
    @FXML
    private ImageView IVproduitImage;
    @FXML
    private Text txtprix;
        private ListedesboutiqueController parent;


    ProduitExemple_1Controller(ListedesboutiqueController aThis) {
        this.parent=aThis;
    }

    public Text getTxtprix() {
        return txtprix;
    }

    public void setTxtprix(String txtprix) {
        this.txtprix.setText(txtprix); 
    }

    
    public AnchorPane getAPproduit() {   
        return APproduit;
    }    

    public void setAPproduit(AnchorPane APproduit) {
        this.APproduit = APproduit;
    }

    public Text getTXTproduitName() {
        return TXTproduitName;
    }

    public void setTXTproduitName(String TXTproduitName) {
        this.TXTproduitName.setText(TXTproduitName);
    }

    public Text getTXTQuantite() {
        return TXTQuantite;
    }

    public void setTXTQuantite(String TXTQuantite) {
        this.TXTQuantite.setText(TXTQuantite);
    }

    public Label getIdproduit() {
        return Idproduit;
    }

    public void setIdproduit(String Idproduit) {
        this.Idproduit.setText(Idproduit); 
    }

    public Button getPbGoToProduit() {
        return pbGoToProduit;
    }

    public void setPbGoToProduit(Button pbGoToProduit) {
        this.pbGoToProduit = pbGoToProduit;
    }

    public ImageView getIVproduitImage() {
        return IVproduitImage;
    }

    /**
     * Initializes the controller class.
     */
    public void setIVproduitImage(ImageView IVproduitImage) {    
        this.IVproduitImage = IVproduitImage;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void AffichierProduit(MouseEvent event) {
        parent.actionboutonproduit(Integer.parseInt(Idproduit.getText()));
    }
    
}
