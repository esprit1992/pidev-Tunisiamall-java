/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DBoutique;
import Dao.DaoUtilisateur;
import Dao.dCarte;
import ENTITY.Boutique;
import ENTITY.Carte_fidelite;
import ENTITY.Utilisateur;
import UTILS.QRCodeJava;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.swing.ImageIcon;

/**
 * FXML Controller class
 *
 * @author siwar-pc
 */
public class CartegenerateController implements Initializable {
    @FXML
    private TableView<Utilisateur> clienttable;
    @FXML
    private TableColumn<Utilisateur, Integer> id;
    @FXML
    private TableColumn<Utilisateur, String> nom;
    @FXML
    private TableColumn<Utilisateur, String> prenom;
    @FXML
    private TableColumn<Utilisateur, String> email;
    @FXML
    private ComboBox<String> combo;
    @FXML
    private Button btngenerate;
    @FXML
    private Label lblmsg;
    @FXML
    private TableView<Carte_fidelite> tablecarte;
    @FXML
    private TableColumn<Carte_fidelite, Integer> idcarte;
    @FXML
    private TableColumn<Carte_fidelite,Utilisateur> id_client_carte;
    @FXML
    private TableColumn<Carte_fidelite, Boutique> id_boutique_carte;
    @FXML
    private TableColumn<Carte_fidelite, Integer> nbrpoint;
    @FXML
    private Button btnsupprimer;
    @FXML
    private ImageView qrclient;
    @FXML
    private Label nomclient;
    @FXML
    private Label prenomclient;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        get_boutique();
        afficherclient();
        affichercarte();
        
    }    
    
     public void get_boutique()
    {
 

        ArrayList<Boutique> listboutique= new ArrayList<Boutique>();
        DBoutique p= new DBoutique();
        listboutique=p.getBoutiques();
        for(int i=0;i<listboutique.size();i++)
        {
            combo.getItems().add(listboutique.get(i).getNom());
        }
    }

    private void afficherclient() {
        DaoUtilisateur duser =new DaoUtilisateur();
System.out.print(duser.getusersclient().size());
//System.out.print(id.toString());
 id.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,Integer>("id"));

     
 nom.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("nom"));

 prenom.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("prenom"));
email.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("email"));
 
    clienttable.setItems(duser.getusersclient());
 
        
    }
    private void affichercarte() {
        dCarte dcarte =new dCarte();
System.out.print(dcarte.getcartes().size());
//System.out.print(id.toString());
 idcarte.setCellValueFactory(
            new PropertyValueFactory<Carte_fidelite,Integer>("id"));

     
 id_client_carte.setCellValueFactory(
            new PropertyValueFactory<Carte_fidelite,Utilisateur>("u"));

 id_boutique_carte.setCellValueFactory(
            new PropertyValueFactory<Carte_fidelite,Boutique>("b"));
nbrpoint.setCellValueFactory(
            new PropertyValueFactory<Carte_fidelite,Integer>("nombre_points"));
 
    tablecarte.setItems(dcarte.getcartes());
 
        
    }
    @FXML
    public void generate(){
        dCarte dc= new dCarte();
        Utilisateur u= clienttable.getSelectionModel().selectedItemProperty().getValue();
        String s= combo.getSelectionModel().selectedItemProperty().getValue();
        //Image imageicon= dc.getcarte(u);
//               if (imageicon != null)
//               {
//                   lblmsg.setText("le Client posséde déja une carte fidelité de ce boutique");
//               }
//               else{
//                QRCodeJava qrjava = new QRCodeJava();
//                //qrjava.generateqr(u, s);
//                lblmsg.setText("carte ajoutée avec succes");
                
//               
//                Image image=dc.getcarte(u);
//                qrclient.setImage(image);
//                
//               }
//               affichercarte();

    }
    @FXML
    public void supprimercarte(){
                dCarte dc= new dCarte();
        int u= tablecarte.getSelectionModel().selectedItemProperty().getValue().getId();
        dc.removecarte(u);
        affichercarte();

        
    }
}
