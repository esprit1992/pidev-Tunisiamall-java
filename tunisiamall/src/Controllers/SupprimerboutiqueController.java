/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import Dao.DBoutique;
import ENTITY.Boutique;
import animation.FadeInDowntransition;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import com.sun.javaws.Main;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class SupprimerboutiqueController implements Initializable { 
   @FXML
    private TableView<Boutique> tableaffiche;
    @FXML
    private TableColumn<Boutique,Integer> id;
    @FXML
    private TableColumn<Boutique, String> nom;
    @FXML
    private TableColumn<Boutique, String> description;
    @FXML
    private TextField nomboutiquetextfield;
    @FXML
    private TextField descriptiontextfield;
    @FXML
    private ImageView LBDisplayImage;
    @FXML
    private AnchorPane AnchorModifierSupprimer;
       private  File file=null;
    FileChooser fileChooser = new FileChooser();
    @FXML
    private Button supprimer;
    @FXML
    private Button image;
    @FXML
    private TextField recherche;
    @FXML
            private AnchorPane AnshorSupprimerBoutique;
    ActionEvent eventt;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AnchorModifierSupprimer.setDisable(true);
         id.setCellValueFactory(
            new PropertyValueFactory<Boutique,Integer>("id"));

     nom.setCellValueFactory(
            new PropertyValueFactory<Boutique,String>("nom"));

 description.setCellValueFactory(
            new PropertyValueFactory<Boutique,String>("description"));


 
 

 image.setOnAction(
            new EventHandler<ActionEvent>() {
                @Override
                public void handle(final ActionEvent e) {
                   file = fileChooser.showOpenDialog(tunisiamallMain.Tunisiamall.getInstance().getStage());
                   
                    if (file != null) {
                        
                        //new FadeInDowntransition(AnchoreUpload).play();
                        try {
                            LBDisplayImage.setImage(new Image(file.toURI().toURL().toString(),223,186,false,false));
                        } catch (MalformedURLException ex) {
                            Logger.getLogger(ControllAddProduct.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
              
                  
                    
                }
                
            });
        
        
        
       afficher();
      
        

    tableaffiche.getSelectionModel().selectedIndexProperty().addListener((obs,oldSelection,newSelection)->{
        
       if(newSelection != null && tableaffiche.getSelectionModel().getSelectedItem()!=null ){
           DBoutique boutiquedao=new DBoutique();
           file=new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\"+boutiquedao.getimageboutique(tableaffiche.getSelectionModel().getSelectedItem().getId()));
           AnchorModifierSupprimer.setDisable(false);
           
          //nomboutiquetextfield.setText(String.valueOf(tableaffiche.getSelectionModel().getSelectedItem().getId()));
            nomboutiquetextfield.setText(tableaffiche.getSelectionModel().getSelectedItem().getNom());
            descriptiontextfield.setText(tableaffiche.getSelectionModel().getSelectedItem().getDescription());
            try {
                System.out.println(boutiquedao.getimageboutique(tableaffiche.getSelectionModel().getSelectedItem().getId()));
            LBDisplayImage.setImage(new Image(new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\"+boutiquedao.getimageboutique(tableaffiche.getSelectionModel().getSelectedItem().getId())).toURI().toURL().toString(),79,79,false,false));
        } catch (MalformedURLException ex) {
            Logger.getLogger(SupprimerboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
       else
           AnchorModifierSupprimer.setDisable(true);
    });
    new FadeInDowntransition(AnshorSupprimerBoutique).play(); 
}

    @FXML
    private void supprimerAction(MouseEvent event) {
        DBoutique daoboutique=new DBoutique();
        int id_responsable=tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId();
        Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("CONFIRMATION DIALOG");
        alert.setContentText("are you sure you want to delete");
        Optional <ButtonType> action=alert.showAndWait();
        if(action.get()==ButtonType.OK)
        {
        daoboutique.deleteboutique(tableaffiche.getSelectionModel().getSelectedItem().getId());
        final TrayNotification tray = new TrayNotification("opération réussie", "la boutique a été supprimé avec succés", Notifications.SUCCESS);
                            tray.showAndDismiss(Duration.seconds(3));
                            afficher();
        }
        
    }
    public void afficher()
    {
         DBoutique daoboutique=new DBoutique();
        //System.out.print(daoboutique.getboutiqueObservabol().size());
tableaffiche.getItems().clear();
 
 //image.setCellValueFactory(
           // new PropertyValueFactory<Boutique,String>("image"));
  
    tableaffiche.setItems(daoboutique.getboutiqueObservabol(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));

        
    //imagetextfield.setText(tableaffiche.getSelectionModel().getSelectedItem());
    
    }

    @FXML
    private void modifierAction(MouseEvent event) {
        DBoutique daoboutique=new DBoutique();
        Boutique boutique=new Boutique();
        boutique.setId(tableaffiche.getSelectionModel().getSelectedItem().getId());
        boutique.setNom(nomboutiquetextfield.getText());
        boutique.setDescription(descriptiontextfield.getText());
        String img=daoboutique.getimageboutique(tableaffiche.getSelectionModel().getSelectedItem().getId());
        boutique.setImage(file.getName());
        daoboutique.updateboutique(boutique);
         controllUploadImageProduit httpPost = null;
         try {
            httpPost = new controllUploadImageProduit();
        } catch (MalformedURLException ex) {
            Logger.getLogger(GererboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
        }
          
    
           
       
            httpPost.setFileNames(new String[]{ file.getPath() });
            httpPost.post();
            System.out.println("=======");
            System.out.println(httpPost.getOutput()); 
               final TrayNotification tray = new TrayNotification("opération réussie", "la boutique a été modifiée avec succés", Notifications.SUCCESS);
                            tray.showAndDismiss(Duration.seconds(3));
                            afficher();
    }
    @FXML
    private void rechercheavance()
    {
    
    
     DBoutique daoboutique=new DBoutique();
        //System.out.print(daoboutique.getboutiqueObservabol().size());
tableaffiche.getItems().clear();
 
 //image.setCellValueFactory(
           // new PropertyValueFactory<Boutique,String>("image"));
  
    tableaffiche.setItems(daoboutique.advancedsearchboutique(recherche.getText()));
    
    
    }

    
    
   
 }   