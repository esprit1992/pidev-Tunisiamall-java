/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javax.swing.JFrame;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllDeleteAlert extends JFrame implements Runnable, ThreadFactory {

private Executor executor = Executors.newSingleThreadExecutor(this);
    @FXML
    private ImageView LabelIcon;
    @FXML
    private Label LabelMessage;
    ControllmodifierSuprimerProduct parentMSI=null;
    @FXML
    private Button ButtonNo;
    @FXML
    private Button ButtonYes;

    ControllDeleteAlert(ControllmodifierSuprimerProduct aThis) {
        super();
        parentMSI=aThis;
           setLayout(new FlowLayout());
		setTitle("Alert Before Delete");
		setDefaultCloseOperation(WebcamScanController.DISPOSE_ON_CLOSE);

		pack();
		setVisible(true);
                
                addWindowListener(new WindowListener() {
                    @Override
                    public void windowOpened(WindowEvent e) {
                    }
                    @Override
                    public void windowClosing(WindowEvent e) {
                      
                    }
                    @Override
                    public void windowClosed(WindowEvent e) {}
                    @Override
                    public void windowIconified(WindowEvent e) {
                    }
                    @Override
                    public void windowDeiconified(WindowEvent e) {
                    }
                    @Override
                    public void windowActivated(WindowEvent e) {
                    }
                    @Override
                    public void windowDeactivated(WindowEvent e) {}});
		executor.execute(this);
	}
    

    /**
     * Initializes the controller class.
     */

    @FXML
    private void ButtonNoCliked(MouseEvent event) {
    }

    @FXML
    private void ButtonYesCliked(MouseEvent event) {
        if(parentMSI!=null)
            parentMSI.ButtonDeleteConfirmer();
    }

    @Override
    public void run() {
        
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, "example-runner");
		t.setDaemon(true);
		return t;
    }


    
}
