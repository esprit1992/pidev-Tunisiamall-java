/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DAOCategorie;
import Dao.DAOCategorieProduit;
import Dao.DProduit;
import Dao.DaoImageProduct;
import ENTITY.Produit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * FXML Controller class
 *
 * @author user
 */
public class controllMenuGestionResponsableBoutique implements Initializable {

    @FXML
    private Button btnBack;
    @FXML
    private AnchorPane paneData;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Text textNameUser;
    @FXML
    private Text TextLocation;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;
    private static controllMenuGestionResponsableBoutique instance;
    /** 
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance=this;
        textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
        TextLocation.setText("Store");
        listMenuOfGestion.getItems().addAll("  Add Store", "  Delete/Edit","  loyalty card");
        listMenuOfGestion.getSelectionModel().select(0);
Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = 30;
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
           service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        service.setOnSucceeded((WorkerStateEvent event) -> {
             bar.progressProperty().unbind();
             bar.setProgress(0);
             imgLoad.setVisible(false);
             TextLocation.setText("Add Store");
                    loadFXMLintoPaneData("/GUI/Ajouterboutique.fxml");      
        
        });
       
        listMenuOfGestion.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue!=null)
                {
                if (newValue.equals("  loyalty card")) {
                    

      
             bar.progressProperty().unbind();
             bar.setProgress(0);
             imgLoad.setVisible(false);
             TextLocation.setText("loyalty card");
                    loadFXMLintoPaneData("/GUI/GestionCarteResponsable.fxml");      
        

              
                } 
                else
                    if (newValue.equals("  Add Store")) {
                    
                   
    
             bar.progressProperty().unbind();
             bar.setProgress(0);
             imgLoad.setVisible(false);
             TextLocation.setText("Add Store");
                    loadFXMLintoPaneData("/GUI/Ajouterboutique.fxml");      
        
      
              
                }
                else
                    if (newValue.equals("  Delete/Edit")) {
                    

             imgLoad.setVisible(false);
             TextLocation.setText("Delete/Edit");
                    loadFXMLintoPaneData("/GUI/Supprimerboutique.fxml");      
              
                }
                    
            }
            }
        });
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        });

        //LoadListeProduit();
    }


    
    public void loadFXMLintoPaneData(String name) {
        AnchorPane p = null;

        URL location = ControllProduitExemple.class.getResource(name);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        try {
            p = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(controllMenuGestionResponsableBoutique.class.getName()).log(Level.SEVERE, null, ex);
        }
        paneData.getChildren().clear();
        paneData.getChildren().addAll(p);
    }



    @FXML
    private void ButtonBackToMenuResponsable(ActionEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menu_responsable.fxml"))));
    }

    public  controllMenuGestionResponsableBoutique getInstance() {
        return instance;
    }

    public ListView<String> getListMenuOfGestion() {
        return listMenuOfGestion;
    }

       @FXML
    private void rechercheavanceAction()
    {
    }
    
}
