/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoUtilisateur;
import animation.FadeInLeftTransition;
import animation.FadeInLeftTransition1;
import animation.FadeInRightTransition;
import com.github.plushaze.traynotification.models.Location;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.IOException;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author kharrat
 */
public class controllLogin implements Initializable {

    @FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Text lblWelcome;
    @FXML
    private Text lblUserLogin;
    @FXML
    private Text lblUsername;
    @FXML
    private Text lblPassword;
    @FXML
    private Button btnLogin;
    @FXML
    private Button btnregister;
    @FXML

    private Label lblClose;
    Stage stage;
    @FXML
    private Button LoginWithCard;
    @FXML
    private Button ButtonFBlogin;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        Platform.runLater(() -> {
            new FadeInRightTransition(lblUserLogin).play();
            new FadeInLeftTransition(lblWelcome).play();
            new FadeInLeftTransition1(lblPassword).play();
            new FadeInLeftTransition1(lblUsername).play();
            new FadeInLeftTransition1(txtUsername).play();
            new FadeInLeftTransition1(txtPassword).play();
            new FadeInRightTransition(btnLogin).play();
            new FadeInRightTransition(btnregister).play();
            new FadeInLeftTransition(LoginWithCard).play();
            new FadeInLeftTransition(ButtonFBlogin).play();
            lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
            TrayNotification tray = new TrayNotification("Welcom To Tunisia Mall", "Please Login Or Create Your Account", Notifications.SUCCESS);

            tray.showAndDismiss(Duration.seconds(3));
        });

    }

    @FXML
    public void LoginButton() throws IOException {
        int msgNumber;
        TrayNotification tray = null;
        String style = " -fx-border-color: red;";
        String styledefault = "-fx-border-color: green;";
        txtUsername.setStyle(styledefault);
        txtPassword.setStyle(styledefault);
        msgNumber = DaoUtilisateur.getInstance().login(txtUsername.getText().toString(), txtPassword.getText().toString());

        if (msgNumber == 0) {
            tray = new TrayNotification("Error", "Sorry but we have an error in code :p ", Notifications.ERROR);
        } else 
            if (msgNumber == 1) {
                tray = new TrayNotification("Warning", "Invalid Input User And Password", Notifications.WARNING);
                txtUsername.setStyle(style);
                txtPassword.setStyle(style);
            }
        else
        if (msgNumber == 2) {
            tray = new TrayNotification("Warning", "Password Is Not Match With User", Notifications.WARNING);

            txtPassword.setStyle(style);
        }
        else
        if (msgNumber == 3) {
            tray = new TrayNotification("Welcome", "Welcom Back " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom(), Notifications.SUCCESS);
            tray.showAndDismiss(Duration.seconds(3));
             if(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getRoles().equals("user"))
            tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml")))); 
            else
            if(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getRoles().equals("admin"))
            tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuAdmin.fxml"))));
             else
            if(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getRoles().equals("responsable"))
            tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menu_responsable.fxml"))));
           
        }
        else
        if (msgNumber == 4) {
            tray = new TrayNotification("Notice", "You Account Is Blocked", Notifications.NOTICE);
        }
        tray.showAndDismiss(Duration.seconds(3));

    }

    @FXML
    public void RegisterButton() throws IOException {
 //btnLogin.fire();
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/register.fxml"))));
    }

    public void setTxtUsername(String txtUsername) {
        this.txtUsername.setText(txtUsername);
    }

    public void setTxtPassword(String txtPassword) {
        this.txtPassword.setText(txtPassword);
    }

    public Button getBtnLogin() {
        return btnLogin;
    }

    @FXML
    private void LoginWithCardAction(MouseEvent event) {
      
        new Thread(new WebcamScanControllLogin(this));
    }

    @FXML
    private void ButtonFBloginAction(MouseEvent event) {
    }

    
}
