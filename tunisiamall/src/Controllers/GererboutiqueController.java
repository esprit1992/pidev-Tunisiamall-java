/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DBoutique;
import ENTITY.Boutique;
import animation.FadeInDowntransition;
import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class GererboutiqueController implements Initializable {
    @FXML
    private AnchorPane AnchoreUpload;
    @FXML
    private ImageView LBDisplayImage;
    @FXML
    private Label nomboutiquelabel;
    @FXML
    private Label descriptionlabel;
    @FXML
    private TextField nomboutique;
    @FXML
    private TextField description;
    @FXML
    private Label imagelabel;
    @FXML
    private Button image;
    private  File file=null;
    @FXML
            private AnchorPane AnchorAddProduct;
    FileChooser fileChooser = new FileChooser();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         new FadeInDowntransition(AnchorAddProduct).play(); 
        image.setOnAction(
            new EventHandler<ActionEvent>() {
                @Override
                public void handle(final ActionEvent e) {
                   file = fileChooser.showOpenDialog(tunisiamallMain.Tunisiamall.getInstance().getStage());
                    if (file != null) {
                        
                        //new FadeInDowntransition(AnchoreUpload).play();
                        try {
                            LBDisplayImage.setImage(new Image(file.toURI().toURL().toString(),223,186,false,false));
                        } catch (MalformedURLException ex) {
                            Logger.getLogger(ControllAddProduct.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
              
                  
                    
                }
                
            });
        
    }    

    @FXML
    private void AjouterBoutiqueAction(MouseEvent event) {
        DBoutique daoboutique=new DBoutique();
        controllUploadImageProduit httpPost = null;
        int id_responsable=tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId();
        if(nomboutique.getText().length()!=0 && description.getText().length()!=0 && file != null)
        {
            
        daoboutique.addboutique(new Boutique(id_responsable,nomboutique.getText(), description.getText(), file.getName()));
        try {
            httpPost = new controllUploadImageProduit();
        } catch (MalformedURLException ex) {
            Logger.getLogger(GererboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
        }
          
    
           
       
            httpPost.setFileNames(new String[]{ file.getPath() });
            httpPost.post();
            System.out.println("=======");
            System.out.println(httpPost.getOutput());   
        
        
        
        final TrayNotification tray = new TrayNotification("opération réussie", "la boutique a été ajouté avec succes", Notifications.SUCCESS);
                            tray.showAndDismiss(Duration.seconds(3));
      
        }
        else
        {
       
final TrayNotification tray = new TrayNotification("Warring", "veuillez remplir tous les champs", Notifications.WARNING,Animations.POPUP);
                            tray.showAndDismiss(Duration.seconds(3));
        }
            
                            
    }
    
    
}
