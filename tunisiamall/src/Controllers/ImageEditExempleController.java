package Controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Dao.DaoImageProduct;
import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ImageEditExempleController implements Initializable {

    @FXML
    private AnchorPane APproduit;
    @FXML
    private AnchorPane APImage;
    @FXML
    private Label IdImage;
    @FXML
    private ImageView IVproduitImage;
    @FXML
    private ImageView IVstar;
    @FXML
    private ImageView DeleteImage;
    int fav;
ControllmodifierSuprimerProduct controllmodifsup;
    ImageEditExempleController(ControllmodifierSuprimerProduct aThis) {
       controllmodifsup=aThis;
   }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fav=1;
    }    

    @FXML
    private void IVstarMouseExited(MouseEvent event) throws MalformedURLException {
        if(fav==0)
        {
                IVstar.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\Ressources\\starDisabled.png").toURI().toURL().toString(),92,92,false,false));
        }
    }

    @FXML
    private void IVstarMouseEntred(MouseEvent event) throws MalformedURLException {
        if(fav==0)
        {
                IVstar.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\Ressources\\star.png").toURI().toURL().toString(),92,92,false,false));
        }
    }

    @FXML
    private void DeleteImage(MouseEvent event) {
        if(fav==1)
        {
            TrayNotification tray = new TrayNotification("Delete Picture", "You Can't Delete The Default Picture", Notifications.ERROR,Animations.POPUP);
           tray.showAndDismiss(Duration.seconds(2));
        }else{
        DaoImageProduct.getInstance().DeleteImage(Integer.valueOf(IdImage.getText()));
        controllmodifsup.loadListImageOfProduct();
    }
    }
    @FXML
    private void CacherButtonStarEtDelete(MouseEvent event) {
    }

    @FXML
    private void AffichierButtonStarEtDelete(MouseEvent event) {
    }

    @FXML
        private void EnableDisableEtoile(MouseEvent event) throws MalformedURLException {
       
        int sommeimageactive=DaoImageProduct.getInstance().getSommeSelectedImageVisible(Integer.valueOf(this.IdImage.getText()));
          
if(fav==0 && sommeimageactive>0){
        TrayNotification tray = new TrayNotification("Info", "Your Can Select Only One Photo To Be The Cover Photo", Notifications.INFORMATION,Animations.POPUP);
           tray.showAndDismiss(Duration.seconds(1));}
        if (fav == 0) {
            if (sommeimageactive == 0) {
                IVstar.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\Ressources\\star.png").toURI().toURL().toString(), 92, 92, false, false));
                fav = 1;
            }
        } else {
            IVstar.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\Ressources\\starDisabled.png").toURI().toURL().toString(), 92, 92, false, false));
            fav = 0;
        }
        DaoImageProduct.getInstance().updateStatueVisibleImage(Integer.valueOf(this.IdImage.getText()), fav);
        
    }

    public void setIdImage(String IdImage) {
        this.IdImage.setText(IdImage);
    }

    public void setIVstar(ImageView IVstar) {
        this.IVstar = IVstar;
    }

    public void setFav(int fav) throws MalformedURLException {
        if(fav==0)
            IVstar.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\Ressources\\starDisabled.png").toURI().toURL().toString(),92,92,false,false));
        else
            IVstar.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\Ressources\\star.png").toURI().toURL().toString(),92,92,false,false));
        this.fav = fav;
    }

    public void setIVproduitImage(File Image) throws MalformedURLException {

       this.IVproduitImage.setImage(new Image(Image.toURI().toURL().toString(),113,97,false,false));
    }
    
}
