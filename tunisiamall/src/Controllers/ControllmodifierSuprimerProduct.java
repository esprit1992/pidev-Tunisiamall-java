/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DAOCategorie;
import Dao.DAOCategorieProduit;
import Dao.DBoutique;
import Dao.DProduit;
import Dao.DaoImageProduct;
import ENTITY.Boutique;
import ENTITY.ImageProduct;
import ENTITY.Produit;
import animation.FadeInDowntransition;
import animation.FadeInUpTransition;
import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllmodifierSuprimerProduct implements Initializable {

    @FXML
    private Label LBsexcr;
    @FXML
    private Label LBsexcr1;
    @FXML
    private ComboBox<String> CBStore;
    @FXML
    private Label LBsexcr11;
    @FXML
    private Label LBsexcr12;
    @FXML
    private Label LBsexcr121;
    @FXML
    private Label IdProduit;
    @FXML
    private Label LBsexcr1211;
    @FXML
    private Label LBsexcr12111;
    @FXML
    private TextField TFproductName;
    @FXML
    private TextField TFQuantite;
    @FXML
    private CheckBox CheckBoxNewCollection;
    @FXML
    private TextField TFbarcode;
    @FXML
    private TextArea TAdescription;
    @FXML
    private TextField TFPrice;
    @FXML
    private Button ButtonScanCodeAbarre;
    @FXML
    private Label LBsexcr1212;
    @FXML
    private Button ButtonChoseImageToUpload;
    @FXML
    private AnchorPane AnchoreUpload;
    @FXML
    private AnchorPane anchordisaplListeImage;
    @FXML
    private ImageView LBDisplayImage;
    @FXML
    private Button ButtonUpload;
    @FXML
    private AnchorPane AnchorUploadProduit;
    @FXML
    private AnchorPane anshorContenu;
    @FXML
    private Button ButtonUpdate;
    @FXML
    private ProgressBar bar;
    @FXML
    private ImageView imgLoad;
    @FXML
    private AnchorPane AnchorListeImage;
    @FXML
    private AnchorPane AnshorButtonEditDelete;
    @FXML
    private Button ButtonEditProduit;
    @FXML
    private Button ButtonDeleteProduit;
    @FXML
    private ComboBox<String> CBcategorieProduit;
    private controllMenuGestionResponsableGenerale parent;
    Map<String, String> listeImageToUpload = new HashMap<String, String>();
    File file = null;
    private ArrayList<String> listeOfCategorie;
    FileChooser fileChooser = new FileChooser();

    ControllmodifierSuprimerProduct(controllMenuGestionResponsableGenerale aThis) {
        parent = aThis;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AnchorUploadProduit.setOpacity(0);
        AnshorButtonEditDelete.setOpacity(1);
        anchordisaplListeImage.setOpacity(1);
        AnchoreUpload.setOpacity(0);
        anshorContenu.setDisable(true);
        CBStore.getItems().addAll(new DBoutique().getBoutiquesnamefromidresponsable(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));
       listeOfCategorie=new DAOCategorie().getAllCategorie();
        CBcategorieProduit.getItems().addAll(listeOfCategorie);
        parent.getInstance().getListMenuOfGestion().getSelectionModel().clearSelection(0);
        ButtonChoseImageToUpload.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent e) {
                file = fileChooser.showOpenDialog(tunisiamallMain.Tunisiamall.getInstance().getStage());
                if (file != null) {

                    new FadeInDowntransition(AnchoreUpload).play();
                    try {
                        LBDisplayImage.setImage(new Image(file.toURI().toURL().toString(), 223, 186, false, false));
                        anchordisaplListeImage.setOpacity(0);
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(ControllAddProduct.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        });
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                loadListImageOfProduct();
            }
        });
    }

    @FXML
    private void ButtonScanCodeAbarre(MouseEvent event) {
        new Thread(new WebcamScanController(this));
    }

    @FXML
    private void ButtonUpload(MouseEvent event) {
        anchordisaplListeImage.setOpacity(1);
        controllUploadImageProduit httpPost = null;
        listeImageToUpload.put(file.getName(), file.getPath());
        try {
            httpPost = new controllUploadImageProduit();
            new FadeInUpTransition(AnchoreUpload).play();
            TrayNotification tray = new TrayNotification("Upload Picture", "Your Picture Was Uploaded Successfully", Notifications.SUCCESS, Animations.POPUP);
            tray.showAndDismiss(Duration.seconds(2));
        } catch (MalformedURLException ex) {
            TrayNotification tray = new TrayNotification("Upload Picture", "There Was a problem in file upload", Notifications.ERROR, Animations.POPUP);
            tray.showAndDismiss(Duration.seconds(2));
        }
        httpPost.setFileNames(new String[]{file.getPath()});
        httpPost.post();
        System.out.println("=======");
        System.out.println(httpPost.getOutput());

        LBDisplayImage.setImage(null);
        AnchoreUpload.toBack();
        DaoImageProduct.getInstance().addImage(new ImageProduct(Integer.valueOf(IdProduit.getText()), file.getName(), 0));
        loadListImageOfProduct();
        AnchorListeImage.toFront();
    }

    public boolean verifInputChamps() {
        int verif = 0;
        String style = " -fx-border-color: red;";
        String styledefault = "-fx-border-color: green;";
        CBStore.setStyle(styledefault);
        TFproductName.setStyle(styledefault);
        TFQuantite.setStyle(styledefault);
        TFbarcode.setStyle(styledefault);
        TAdescription.setStyle(styledefault);
        TFPrice.setStyle(styledefault);
        CBcategorieProduit.setStyle(styledefault);
          if(CBcategorieProduit.getEditor().getText().equals(""))
            {
                CBcategorieProduit.setStyle(style);
                verif = 1;
            }
        
        try {
            CBStore.getSelectionModel().getSelectedItem().toString().equals("");
        } catch (Exception e) {
            {
                CBStore.setStyle(style);
                verif = 1;
            }
        }
        if (TFproductName.getText().equals("")) {
            TFproductName.setStyle(style);
            verif = 1;
        }
        if (TAdescription.getText().equals("")) {
            TAdescription.setStyle(style);
            verif = 1;
        }
        try {
            if (Integer.parseInt(TFQuantite.getText()) < 0) {
                TFQuantite.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFQuantite.setStyle(style);
                verif = 1;
            }
        }
        try {
            if (Long.parseLong(TFbarcode.getText()) < 0) {
                TFbarcode.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFbarcode.setStyle(style);
                verif = 1;
            }
        }
        try {
            if (Double.compare((Double.parseDouble(TFPrice.getText())), 0.0) < 0) {
                TFPrice.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFPrice.setStyle(style);
                verif = 1;
            }
        }
        if (verif == 0) {
            return true;
        }
        TrayNotification tray = new TrayNotification("Error", "Verif Your Input Please", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
        return false;
    }

    @FXML
    private void ButtonEnableEdit(MouseEvent event) {
        anshorContenu.setDisable(false);

        new FadeInDowntransition(AnchorUploadProduit).play();
        new FadeInUpTransition(AnshorButtonEditDelete).play();   //close
    }

    @FXML
    private void ButtonDelete(MouseEvent event) {
        Alert alert = new Alert(AlertType.WARNING);
alert.setTitle("Confirmation Delete");
alert.setHeaderText("");
alert.setContentText("Are you sure you want to delete this Product ("+TFproductName.getText()+")?");
ButtonType buttonTypeYes = new ButtonType("Yes", ButtonData.OK_DONE);
ButtonType buttonTypeNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
alert.getButtonTypes().setAll(buttonTypeYes,buttonTypeNo);
alert.initStyle(StageStyle.UTILITY);
Optional<ButtonType> result = alert.showAndWait();
if (result.get() == buttonTypeYes){
    ButtonDeleteConfirmer();
} 
        //new Thread(new ControllDeleteAlert(this));
    }
    public void ButtonDeleteConfirmer()
    {
        Produit p=new Produit(new DBoutique().getboutiquee(CBStore.getSelectionModel().getSelectedItem().toString()),Integer.valueOf(TFQuantite.getText()),TAdescription.getText(),Double.valueOf(TFPrice.getText()),0,TFproductName.getText(),TFbarcode.getText(),Integer.valueOf(IdProduit.getText()));
        DProduit daoProduit=new DProduit();
        daoProduit.deleteprod(p);
        parent.getInstance().getListMenuOfGestion().getSelectionModel().select(0);
    }

    @FXML
    private void ButtonUpdate(MouseEvent event) {
        Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = 30;
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
        int newc=0;
        if(CheckBoxNewCollection.isSelected())
            newc=1;
        DProduit daoProduit=new DProduit();        
                Produit p=new Produit(new DBoutique().getboutiquee(CBStore.getSelectionModel().getSelectedItem().toString()),Integer.valueOf(TFQuantite.getText()),TAdescription.getText(),Double.valueOf(TFPrice.getText()),newc,TFproductName.getText(),TFbarcode.getText(),Integer.valueOf(IdProduit.getText()));

        int erreur=daoProduit.updateprod(p);
        if(verifInputChamps())
        {
            
            service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent eventt) -> {
            imgLoad.setVisible(true);
        });
        service.setOnSucceeded((WorkerStateEvent eventt) -> {
             bar.progressProperty().unbind();
             bar.setProgress(0);
            imgLoad.setVisible(false);
  
        if(erreur==1)
        {
            final TrayNotification tray = new TrayNotification("ERROR", "Product bar Code alredy Exist", Notifications.ERROR);
                            tray.showAndDismiss(Duration.seconds(3));
        }
        else
            if(erreur==2)
        {
            final TrayNotification tray = new TrayNotification("ERROR", "Product name alredy Exist", Notifications.ERROR);
                            tray.showAndDismiss(Duration.seconds(3));
        }
        else
            if(erreur==3)
        {
            final TrayNotification tray = new TrayNotification("ERROR", "Erreur In Sql", Notifications.ERROR);
                            tray.showAndDismiss(Duration.seconds(3));
        }
        else
            if(erreur==4)
        {
            final TrayNotification tray = new TrayNotification("Upadate OK", "Product was update succsufuly", Notifications.INFORMATION);
                            tray.showAndDismiss(Duration.seconds(3));
                            anshorContenu.setDisable(true);
        new FadeInDowntransition(AnshorButtonEditDelete).play();
        new FadeInUpTransition(AnchorUploadProduit).play();   //close
        parent.getInstance().getListMenuOfGestion().getSelectionModel().select(0);
        }
        if(!listeOfCategorie.contains(CBcategorieProduit.getEditor().getText()))
            new DAOCategorie().addnewcategorie(CBcategorieProduit.getEditor().getText());
        int idCategorie=-1;
             idCategorie=new DAOCategorie().getidCategorie(CBcategorieProduit.getEditor().getText());
                    new DAOCategorieProduit().updateCategorieProduit(p.getId(), idCategorie);
        
        
        
    });}
        else
        {
            TrayNotification tray = new TrayNotification("Error", "Verif Your Input Please", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
        }
        
    }
        

    public void loadListImageOfProduct() {
        double sizescroll = 0;
        DaoImageProduct daoImgProduct = new DaoImageProduct();
        ArrayList<ImageProduct> listeImageProduct = null;
        System.out.println("Controllers.ControllmodifierSuprimerProduct.loadListImageOfProduct()" + IdProduit.getText());
        listeImageProduct = daoImgProduct.ImageOfProduct(Integer.valueOf(IdProduit.getText()));
        URL location = ControllProduitExemple.class.getResource("/GUI/imageEditExemple.fxml");
        AnchorPane p = null;
        TextFlow test = new TextFlow();
        test.setPrefWidth(369);
        test.setLineSpacing(0);
        for (int i = 0; i < listeImageProduct.size(); i++) {
            if (i % 3 == 0) {
                sizescroll += 107;
            }
            ImageProduct imgprod = listeImageProduct.get(i);
            //.out.println(produit.getId());
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            ImageEditExempleController cpe = new ImageEditExempleController(this);

            try {
                fxmlLoader.setController(cpe);
                p = fxmlLoader.load();
                cpe.setIdImage(String.valueOf(listeImageProduct.get(i).getId()));
                cpe.setFav(listeImageProduct.get(i).getActive());
                File imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\" + listeImageProduct.get(i).getChemain());
                if (!imgecheck.exists()) {
                    imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\noavailable.png");
                }
                cpe.setIVproduitImage(imgecheck);
            } catch (IOException ex) {
                Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
            }
            test.getChildren().add(p);
        }
        AnchorListeImage.getChildren().clear();
        AnchorListeImage.getChildren().addAll(test);
        AnchorListeImage.setPrefHeight(sizescroll);
    }

    public void setCBStoreText(String CBStore) {
        this.CBStore.getSelectionModel().select(CBStore);
    }

    public void setTFproductNameText(String TFproductName) {
        this.TFproductName.setText(TFproductName);
    }

    public void setTFQuantiteText(String TFQuantite) {
        this.TFQuantite.setText(TFQuantite);
    }

    public void setTFbarcodeText(String TFbarcode) {
        this.TFbarcode.setText(TFbarcode);
    }

    public void setTAdescriptionText(String TAdescription) {
        this.TAdescription.setText(TAdescription);
    }

    public void setTFPriceText(String TFPrice) {
        this.TFPrice.setText(TFPrice);
    }

    public void setidProduit(String id) {
        this.IdProduit.setText(id);
    }

    public void setNewCollection(int id) {
        if (id == 1) {
            this.CheckBoxNewCollection.selectedProperty().set(true);
        } else {
            this.CheckBoxNewCollection.selectedProperty().set(false);
        }
    }


    public void setCBcategorieProduit(String CBcategorieProduit) {
        this.CBcategorieProduit.getSelectionModel().select(CBcategorieProduit);
        this.CBcategorieProduit.getEditor().setText(CBcategorieProduit);
    }
    
    
}
