/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javax.swing.ImageIcon;

import javax.swing.JFrame;





public class WebcamScanControllLogin extends JFrame implements Runnable, ThreadFactory {

	private static final long serialVersionUID = 6441489157408381878L;

	private Executor executor = Executors.newSingleThreadExecutor(this);

	private Webcam webcam = null;
	private WebcamPanel panel = null;
        private String barcodevalue="";
        private controllLogin cap=null;
        private ControllmodifierSuprimerProduct cmsp=null;
	public WebcamScanControllLogin(controllLogin cap) {
		super();
                this.cap=cap;
		setLayout(new FlowLayout());
		setTitle("Login With Card");
		setDefaultCloseOperation(WebcamScanControllLogin.DISPOSE_ON_CLOSE);
		Dimension size = WebcamResolution.VGA.getSize();
		webcam = Webcam.getWebcams().get(0);
		webcam.setViewSize(size);
		panel = new WebcamPanel(webcam);
		panel.setPreferredSize(size);
		add(panel);
		pack();
		setVisible(true);
                addWindowListener(new WindowListener() {
                    @Override
                    public void windowOpened(WindowEvent e) {
                    }
                    @Override
                    public void windowClosing(WindowEvent e) {
                        webcam.close();
                    }
                    @Override
                    public void windowClosed(WindowEvent e) {}
                    @Override
                    public void windowIconified(WindowEvent e) {
                    }
                    @Override
                    public void windowDeiconified(WindowEvent e) {
                    }
                    @Override
                    public void windowActivated(WindowEvent e) {
                    }
                    @Override
                    public void windowDeactivated(WindowEvent e) {}});
		executor.execute(this);
	}
        
        public WebcamScanControllLogin(ControllmodifierSuprimerProduct cap) {
		super();
                this.cmsp=cap;
		setLayout(new FlowLayout());
		setTitle("Bar Code Product");
		setDefaultCloseOperation(WebcamScanControllLogin.DISPOSE_ON_CLOSE);
		Dimension size = WebcamResolution.VGA.getSize();
		webcam = Webcam.getWebcams().get(0);
		webcam.setViewSize(size);
		panel = new WebcamPanel(webcam);
		panel.setPreferredSize(size);
		add(panel);
		pack();
		setVisible(true);
                addWindowListener(new WindowListener() {
                    @Override
                    public void windowOpened(WindowEvent e) {
                    }
                    @Override
                    public void windowClosing(WindowEvent e) {
                        webcam.close();
                    }
                    @Override
                    public void windowClosed(WindowEvent e) {}
                    @Override
                    public void windowIconified(WindowEvent e) {
                    }
                    @Override
                    public void windowDeiconified(WindowEvent e) {
                    }
                    @Override
                    public void windowActivated(WindowEvent e) {
                    }
                    @Override
                    public void windowDeactivated(WindowEvent e) {}});
		executor.execute(this);
	}

	@Override
	public void run() {
            String part1=null;
            String part2=null;
Result result = null;
		do {
			BufferedImage image = null;
			if (webcam.isOpen()) {
				if ((image = webcam.getImage()) == null) {
					continue;
				}
				LuminanceSource source = new BufferedImageLuminanceSource(image);
                                ImageIcon icon;
                            icon = new ImageIcon(image);        
				BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
                            try {
                                result = new MultiFormatReader().decode(bitmap);
                            } catch (NotFoundException ex) {                        
                            }
			}
			if (result != null) {
                           String[] parts = result.getText().split("/");
                          
//                            System.out.println(parts[1]);
//                            part1=parts[1];
//                            System.out.println(parts[1]);
                               cap.setTxtUsername(parts[1]);
                              cap.setTxtPassword(parts[2]);
                               //cap.getBtnLogin().fire();
			}
		} while (result == null);
                closescan();
	}
	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r, "example-runner");
		t.setDaemon(true);
		return t;
	}

    private void closescan() {
        this.dispose();
                webcam.close();
               // cap.getBtnLogin().fire();
    }
}
