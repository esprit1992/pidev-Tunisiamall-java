/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Controllers.BoutiqueController;
import Controllers.ControllProduitExemple;
import Controllers.controllMenuGestionResponsableGenerale;
import Dao.DBoutique;
import Dao.DProduit;
import Dao.DaoImageProduct;
import ENTITY.Boutique;
import ENTITY.Produit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * FXML Controller class
 *
 * @author siwar-pc
 */
public class ListedesboutiqueController implements Initializable {

    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Button btnBack;
    @FXML
    public AnchorPane paneData;
    @FXML
    private Text textNameUser;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private Text TextLocation;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;
    @FXML
    private AnchorPane page1;
    

    /**
     * Initializes the controller class.
     */
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
                    tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
                } catch (IOException ex) {
                    Logger.getLogger(ListedesboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
                TextLocation.setText("Liste des Boutiques");
        listMenuOfGestion.getItems().addAll("  Liste des boutiques ");
        listMenuOfGestion.getSelectionModel().select(0);

        listMenuOfGestion.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.equals(" Liste des boutiques ")) {
                    loadlisteBoutique();
                }
//                else if (newValue.equals("  liste des boutiques fidelite")) {
//                    LoadListeProduit();
//                }
            }
        });
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        });

        loadlisteBoutique();
       
    }

    public void loadlisteBoutique() {
        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
                    tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
                } catch (IOException ex) {
                    Logger.getLogger(ListedesboutiqueController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

                TextLocation.setText("Liste des Boutiques");

        File imgecheck = null;
        DBoutique daoB = new DBoutique();
        DProduit dprod= new DProduit();
        ArrayList<Boutique> listeBoutique = null;
        listeBoutique = daoB.getBoutiques();
        URL location = BoutiqueController.class.getResource("/GUI/Boutique.fxml");
        AnchorPane p = null;
        TextFlow test = new TextFlow();
        test.setPrefWidth(998);
        test.setLineSpacing(20);
        for (int i = 0; i < listeBoutique.size(); i++) {
            Boutique b = listeBoutique.get(i);
            int nbr;
            //.out.println(produit.getId());
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
      
            
            BoutiqueController storecontroll = new BoutiqueController(this);
            nbr= dprod.getprodsboutique(b.getId());
            try {
                fxmlLoader.setController(storecontroll);
                p = fxmlLoader.load();
                storecontroll.setIdboutique(String.valueOf(b.getId()));
                // storecontroll.setTXTQuantite(Integer.toString(produit.getQuantite()));
                storecontroll.setTXTStoreName(b.getNom());
                
                storecontroll.setTXTQuantite(String.valueOf(nbr));
                imgecheck = new File("C:\\wamp\\www\\PIDEV1\\uploads\\"+b.getImage());
                    if (imgecheck == null) {
                        imgecheck = new File("C:\\wamp\\www\\PIDEV1\\uploads\\noavailable.png");

            }
                    storecontroll.setBoutiqueImage(new Image(imgecheck.toURI().toURL().toString(),100,100,false,false));
            }
            catch (IOException ex) {
                Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
            }

            test.getChildren().add(p);
        }
        paneData.getChildren().clear();
        paneData.getChildren().addAll(test);
    }

    @FXML
    private void ButtonBackToMenuResponsable(ActionEvent event) {
    }
   
    public void action(int id){
                btnBack.setDisable(false);
                        btnBack.setOpacity(1);


       btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
               loadlisteBoutique();
            }
        });
        TextLocation.setText("Liste des produit");
        File imgecheck = null;
        DProduit daoproduit = new DProduit();
        DaoImageProduct daoIP = new DaoImageProduct();
        ArrayList<Produit> listeProduit = null;
        Map<Integer, String> listeProduitImage = null;
        listeProduit = daoproduit.getallprodsboutique(id);
        listeProduitImage = daoIP.ImageOfProduct();
        URL location = ProduitExemple_1Controller.class.getResource("/GUI/ProduitExemple_1.fxml");
        AnchorPane p = null;
        TextFlow test2 = new TextFlow();
        test2.setPrefWidth(998);
        test2.setLineSpacing(20);
        System.out.println(listeProduit.size());
        for (int i = 0; i < listeProduit.size(); i++) {
            Produit produit = listeProduit.get(i);
            //.out.println(produit.getId());
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            ProduitExemple_1Controller cpe = new ProduitExemple_1Controller(this);

            try {
                fxmlLoader.setController(cpe);
                p = fxmlLoader.load();
                cpe.setIdproduit(String.valueOf(produit.getId()));
                cpe.setTXTQuantite(Integer.toString(produit.getQuantite()));
                cpe.setTxtprix(String.valueOf(produit.getPrix()));
                cpe.setTXTproduitName(produit.getLibelle());
                System.out.println("key is"+produit.getId());
                if (listeProduitImage.containsKey(produit.getId())) {
                    System.out.println("d5al "+listeProduitImage.get(produit.getId()));
                    imgecheck = new File("C:\\wamp\\www\\PIDEVfinish\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\"+listeProduitImage.get(produit.getId()));
                   
                    if (!imgecheck.exists()) {
                        imgecheck = new File("C:\\wamp\\www\\PIDEVfinish\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\noavailable.png");
                    }
                } else {//fama image manijimch i7ilha mfas5a mil dossier
                    imgecheck = new File("C:\\wamp\\www\\PIDEVfinish\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\noimageavailable.png");
                }
                cpe.getIVproduitImage().setImage(new Image(imgecheck.toURI().toURL().toString(),92,92,false,false));
            
            if (produit.getQuantite() >= 10) 
                    cpe.getPbGoToProduit().setStyle("-fx-background-color:#00A600");
                    else if(produit.getQuantite() > 0)
                       cpe.getPbGoToProduit().setStyle("-fx-background-color:#DC572E");
                
            
            }
            
            
                 catch (IOException ex) {
                Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
            }
            test2.getChildren().add(p);
           
        }
        System.out.println("");
        paneData.getChildren().clear();
        paneData.getChildren().addAll(test2);
        
    }
    public void actionboutonproduit(int id){
                btnBack.setDisable(false);
        btnBack.setOpacity(1);

                TextLocation.setText("Details de produit");

        AnchorPane p = null;
                    DProduit dprod = new DProduit();

                    Produit prod= dprod.getprodbyid(id);
                
        URL location = controllMenuGestionResponsableGenerale.class.getResource("/GUI/AffichierProduit.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        ControllAffichierProduit cmg=new ControllAffichierProduit();
        fxmlLoader.setController(cmg);
        
        try {
            p=fxmlLoader.load();
            cmg.setLBIdProduit(String.valueOf(id));
            
            cmg.setLBNameBoutique(prod.getB().getNom());
        } catch (IOException ex) {
            Logger.getLogger(controllMenuUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        paneData.getChildren().clear();
        paneData.getChildren().add(p);
        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
               action(prod.getB().getId());
            }
        });
    }

}
