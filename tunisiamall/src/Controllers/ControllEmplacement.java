/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DPack;
import animation.FadeInDowntransition;
import animation.FadeInUpTransition;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllEmplacement implements Initializable {
    @FXML
    private AnchorPane APproduit;
    @FXML
    private AnchorPane AnshorF4;
    @FXML
    private Label disponible;
    @FXML 
    private Label id;
    @FXML
    private Text LBreservationText1;
    @FXML
    private Button BPBay;
    private ControllAdvertisingResponsableBuy parent=null;
    @FXML
    private Label TxtDescription;
    @FXML
    private Text txtPrix;
    @FXML
    private Label id_Personne;
    @FXML 
    private ImageView ImageProduit;
    @FXML
    private Label type;
    @FXML
    private Label datefin;
    private ArrayList<String> tableimage;
    private File imgecheck;
    private int slideshowCount=0;
   

    ControllEmplacement(ControllAdvertisingResponsableBuy aThis) {
        parent=aThis;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(disponible.getText().equals("3"))
                {
                tableimage=new DPack().getListeImage(Integer.valueOf(id.getText()));
           
          Task task = new Task<Void>() {
                    @Override
            public Void call() throws Exception {
                        for (int i = 0; i < tableimage.size(); i++) {
                          
                            Platform.runLater(new Runnable() {
                                @Override
            public void run() {
                    imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\"+tableimage.get(slideshowCount));
                                        
                    if (!imgecheck.exists()) 
                        imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\noavailable.png");
                 
                
                                    try {
                                        if(type.getText().equals("1"))
                                        ImageProduit.setImage(new Image(imgecheck.toURI().toURL().toString(),270,268,false,false));
                                        else
                                            if(type.getText().equals("2"))
                                                ImageProduit.setImage(new Image(imgecheck.toURI().toURL().toString(),270,130,false,false));
                                                else
                                                ImageProduit.setImage(new Image(imgecheck.toURI().toURL().toString(),130,130,false,false));
                                    } catch (MalformedURLException ex) {
                                        Logger.getLogger(ControllAffichierProduit.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    slideshowCount++;
                                    if (slideshowCount >= tableimage.size()) {
                                        slideshowCount = 0;
                                    }
                                }
                            });

                        Thread.sleep(4000);
                            if(i==tableimage.size()-1)
                                i=0;
                    }
                        
                    return null;
                }
            };
          Thread th = new Thread(task);
            th.setDaemon(true);
            th.start();
            }}
            });
    }    

    public Label getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible.setText(disponible);
        if(Integer.valueOf(disponible)==0)//red
        {
           
             this.BPBay.setStyle("-fx-background-color:#BF1E4B;");
             this.LBreservationText1.setStyle("-fx-fill:#AC193D;");
          
        }
        
        else
             if(Integer.valueOf(disponible)==1)//green
        {
             
        this.BPBay.setStyle("-fx-background-color:#00A600;");
        this.LBreservationText1.setStyle("-fx-fill:#008A00;");
        
        }
        else
           this.BPBay.setStyle("-fx-background-color:#bfbfbf;");     
    }

    public AnchorPane getAPproduit() {
        return APproduit;
    }

    public void setAPproduit(AnchorPane APproduit) {
        this.APproduit = APproduit;
    }

    public AnchorPane getAnshorF4() {
        return AnshorF4;
    }

    public void setAnshorF4(AnchorPane AnshorF4) {
        this.AnshorF4 = AnshorF4;
    }

    public String getId() {
        return id.getText();
    }

    public void setId(String id) {
        this.id.setText(id);
    }

    public Text getLBreservationText1() {
        return LBreservationText1;
    }

    public void setLBreservationText1(String LBreservationText1) {
       // System.out.println(LBreservationText1);
        this.LBreservationText1.setText(LBreservationText1);
    }

    public Button getBPBay() {
        return BPBay;
    }

    public void setBPBay(Button BPBay) {
        this.BPBay = BPBay;
    }

    public String getTxtDescription() {
        return TxtDescription.getText();
    }

    public void setTxtDescription(String TxtDescription) {
        this.TxtDescription.setText(TxtDescription);
    }

    public String getTxtPrix() {
        return txtPrix.getText();
    }

    public void setTxtPrix(String txtPrix) {
        this.txtPrix.setText(txtPrix);
    }

    public void setId_Personne(String id_Personne) {
        this.id_Personne.setText(id_Personne);
    }

    
    
    @FXML
    private void BPBayAction(MouseEvent event) {
        String style = " -fx-border-color: gray;";
        parent.setIDPack(String.valueOf(this.getId()));
        parent.setTFPrixDeJour(String.valueOf(this.getTxtPrix()));
        parent.setTXTDescription(String.valueOf(this.getTxtDescription()));
        parent.getPbButtonReserver().setDisable(true);
        parent.getDPdateFin().setDisable(false);
        parent.getDPdateFin().setValue(LocalDate.now());
        parent.getLBDateFin3Slide().setText(datefin.getText());
        if(Integer.valueOf(disponible.getText())!=3)
        {
            if(parent.getAnshorReservation().getOpacity()==0){
            new FadeInDowntransition(parent.getAnshorReservation()).play();
            if(parent.getAnshor3Pack().getOpacity()==1)
            new FadeInUpTransition(parent.getAnshor3Pack()).play();
            }
        if(Integer.valueOf(disponible.getText())==0)
            parent.getPbButtonReserver().setVisible(false);
        else
            parent.getPbButtonReserver().setVisible(true);
        parent.getAnshorReservation().toFront();
        }
        if(Integer.valueOf(disponible.getText())==3)
        {
            if(parent.getAnshor3Pack().getOpacity()==0){
            new FadeInDowntransition(parent.getAnshor3Pack()).play();
             if(parent.getAnshorReservation().getOpacity()==1)
            new FadeInUpTransition(parent.getAnshorReservation()).play();
            }
           
            if(type.getText().equals("1"))
            {
                parent.getCB3Slide2().setDisable(false);
                parent.getCB3Slide3().setDisable(false);
                
            }
            else
            {
                parent.getCB3Slide2().setDisable(true);
                parent.getCB3Slide3().setDisable(true);
            }
            parent.getAnshor3Pack().toFront();
            
            parent.getCB3Slide1().setStyle(style);
    parent.getCB3Slide2().setStyle(style);
    parent.getCB3Slide3().setStyle(style);
    parent.getCB3Slide1().setValue("");
    parent.getCB3Slide2().setValue("");
    parent.getCB3Slide3().setValue("");
        }
       
        
        
        
    }

    public Label getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin.setText(datefin);
    }




}
