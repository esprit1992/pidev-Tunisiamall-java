/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DBoutique;
import Dao.DaoUtilisateur;
import Dao.dCarte;
import ENTITY.Boutique;
import ENTITY.Utilisateur;
import UTILS.QRCodeJava;
import animation.FadeInDowntransition;
import animation.FadeInUpTransition;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllGestionCarteResponsable implements Initializable {
    @FXML
    private TableView<Utilisateur> clienttable;
    @FXML
    private TableColumn<Utilisateur, Integer> id;
    @FXML
    private TableColumn<Utilisateur, String> nom;
    @FXML
    private TableColumn<Utilisateur, String> prenom;
    @FXML
    private TableColumn<Utilisateur, String> email;
    @FXML
    private ComboBox<String> combo;
    @FXML
    private Button btngenerate;
    @FXML
    private ImageView qrclient;
    @FXML
    private Label nomclient;
    @FXML
    private Label prenomclient;
    @FXML
    private Label BoutiqueName;
    @FXML
    private Button print;
    @FXML
    private ScrollPane datapanescroll;
    @FXML
    private Label lbBoutique;
    @FXML
    private AnchorPane AnchorAffichierCarte;
    @FXML
    private Button Delete;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        combo.getItems().addAll(new DBoutique().getBoutiquesnamefromidresponsable(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));
        datapanescroll.setOpacity(0);
        combo.setOpacity(0);
        btngenerate.setOpacity(0);
        AnchorAffichierCarte.setOpacity(0);
        print.setOpacity(0);
        Delete.setOpacity(0);
        lbBoutique.setOpacity(0);
        afficherclient();
        new FadeInDowntransition(datapanescroll).play(); 
     clienttable.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                combo.getSelectionModel().select(null);
             if(lbBoutique.getOpacity()==0)
             {
                 new FadeInDowntransition(lbBoutique).play(); 
                 new FadeInDowntransition(combo).play(); 
             }
             if(btngenerate.getOpacity()==1)
             {
                 new FadeInUpTransition(btngenerate).play(); 
             }
             if(AnchorAffichierCarte.getOpacity()==1)
             {
                 new FadeInUpTransition(AnchorAffichierCarte).play(); 
             }
             if(print.getOpacity()==1)
             {
                 new FadeInUpTransition(print).play(); 
             }
             if(Delete.getOpacity()==1)
             {
                 new FadeInUpTransition(Delete).play(); 
             }
                 
             });
     
     combo.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                 if(newValue!=null)
             {
                 DBoutique daoboutique=new DBoutique();
                 Boutique boutique=new Boutique();
                 boutique=daoboutique.getboutiquee(combo.getSelectionModel().selectedItemProperty().getValue());
        Utilisateur u= clienttable.getSelectionModel().selectedItemProperty().getValue();
                 dCarte dc= new dCarte();
                 Image imageicon= dc.getcarte(u,boutique);
               if (imageicon != null)
               {
                   if(btngenerate.getOpacity()==1)
             new FadeInUpTransition(btngenerate).play(); 
                   nomclient.setText(u.getNom());
                prenomclient.setText(u.getPrenom());
                BoutiqueName.setText(boutique.getNom());
                BufferedImage bufimg=dc.getQRImage(u, boutique);
                Image image = SwingFXUtils.toFXImage(bufimg, null);
            qrclient.setImage(image);
            new FadeInDowntransition(AnchorAffichierCarte).play(); 
         new FadeInDowntransition(print).play(); 
         new FadeInDowntransition(Delete).play();
               }
             else{
                   if(AnchorAffichierCarte.getOpacity()==1)
             {
                 new FadeInUpTransition(AnchorAffichierCarte).play(); 
                 new FadeInUpTransition(Delete).play(); 
                 new FadeInUpTransition(print).play(); 
             }
                       
                   new FadeInDowntransition(btngenerate).play(); 
               }
                 
                 
             }
             });
    }    

    @FXML
    private void generateAction(MouseEvent event) {
        new FadeInUpTransition(btngenerate).play(); 
        dCarte dc= new dCarte();
        Boutique boutique=new Boutique();
        DBoutique daoboutique=new DBoutique();
        boutique=daoboutique.getboutiquee(combo.getSelectionModel().selectedItemProperty().getValue());
        Utilisateur u= clienttable.getSelectionModel().selectedItemProperty().getValue();
        
          
                QRCodeJava qrjava = new QRCodeJava();
                qrjava.generateqr(u,boutique);
                nomclient.setText(u.getNom());
                prenomclient.setText(u.getPrenom());
                BoutiqueName.setText(boutique.getNom());
                BufferedImage bufimg=dc.getQRImage(u, boutique);
                Image image = SwingFXUtils.toFXImage(bufimg, null);
            qrclient.setImage(image);
                new FadeInDowntransition(AnchorAffichierCarte).play(); 
         new FadeInDowntransition(print).play(); 
         new FadeInDowntransition(Delete).play(); 
               
               
        
         
         
    }

    @FXML
    private void PrintCarteAction(MouseEvent event) {
    }
    
    
        private void afficherclient() {
        DaoUtilisateur duser =new DaoUtilisateur();
System.out.print(duser.getusersclient().size());
//System.out.print(id.toString());
 id.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,Integer>("id"));

     
 nom.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("nom"));

 prenom.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("prenom"));
email.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("email"));
 
    clienttable.setItems(duser.getusersclient());
 
        
    }

    @FXML
    private void DeleteCarteAction(MouseEvent event) {
             Alert alert = new Alert(Alert.AlertType.WARNING);
alert.setTitle("Confirmation Delete");
alert.setHeaderText("");
alert.setContentText("Are you sure you want to delete this Cart");
ButtonType buttonTypeYes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
ButtonType buttonTypeNo = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
alert.getButtonTypes().setAll(buttonTypeYes,buttonTypeNo);
alert.initStyle(StageStyle.UTILITY);
Optional<ButtonType> result = alert.showAndWait();
if (result.get() == buttonTypeYes){
    supprimercarte();
} 
    }
    
    
     public void supprimercarte(){
         int row=0;
         Boutique boutique=new Boutique();
         DBoutique daoboutique=new DBoutique();
        boutique=daoboutique.getboutiquee(combo.getSelectionModel().selectedItemProperty().getValue());
        Utilisateur u= clienttable.getSelectionModel().selectedItemProperty().getValue();
        dCarte dc= new dCarte();
        dc.removecarte(u,boutique);
        row=clienttable.getSelectionModel().getSelectedIndex();
        clienttable.getSelectionModel().select(null);
        clienttable.getSelectionModel().select(row);
    }
    
}
