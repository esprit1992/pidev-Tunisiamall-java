/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DAOCategorie;
import Dao.DAOCategorieProduit;
import Dao.DProduit;
import Dao.DaoImageProduct;
import ENTITY.Produit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * FXML Controller class
 *
 * @author user
 */
public class controllMenuGestionResponsableGenerale implements Initializable {

    @FXML
    private Button btnBack;
    @FXML
    private AnchorPane paneData;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Text textNameUser;
    @FXML
    private Text TextLocation;
    @FXML
    private Label lblClose;
    @FXML
    private TextField recherche;
    @FXML
    private ProgressBar bar;
    private static controllMenuGestionResponsableGenerale instance;
    /** 
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance=this;
        textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
        TextLocation.setText("List Of Products");
        listMenuOfGestion.getItems().addAll("  List Of Products", "  Add Product");
        listMenuOfGestion.getSelectionModel().select(0);

        listMenuOfGestion.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue!=null)
                {
                if (newValue.equals("  Add Product")) {
                    loadFXMLintoPaneData("/GUI/AddProduct.fxml");
                } else if (newValue.equals("  List Of Products")) {
                    LoadListeProduit("");
                    recherche.setOpacity(1);
        recherche.setEditable(true);
        recherche.setText("");
                }
            }
            }
        });
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        });
Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = 30;
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
           service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        service.setOnSucceeded((WorkerStateEvent event) -> {
bar.progressProperty().unbind();
             bar.setProgress(0);
             imgLoad.setVisible(false);
        LoadListeProduit("");
        recherche.setText("");
        recherche.setOpacity(1);
        recherche.setEditable(true);
    });}


    
    public void loadFXMLintoPaneData(String name) {
        recherche.setOpacity(0);
        recherche.setEditable(false);
        AnchorPane p = null;

        URL location = ControllProduitExemple.class.getResource(name);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        try {
            p = fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
        }
        paneData.getChildren().clear();
        paneData.getChildren().addAll(p);
    }

    public void loadProduitIntoPanelData(int id)
    {
        recherche.setOpacity(0);
        recherche.setEditable(false);
        AnchorPane p = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
        DProduit daoproduit = new DProduit();
            DAOCategorieProduit daocatprod=new DAOCategorieProduit();
            DAOCategorie daocategorie=new DAOCategorie();
        Produit prod=null;
        URL location = ControllProduitExemple.class.getResource("/GUI/modifierSuprimerProduct.fxml");
        prod=daoproduit.getProduit(id);
        System.out.print("labelCategorie est:"+daocategorie.getlabelCategorie(daocatprod.getCategorieOfProduct(prod.getId())));
        String CategorieProduit=daocategorie.getlabelCategorie(daocatprod.getCategorieOfProduct(prod.getId()));
        ControllmodifierSuprimerProduct cpe=new ControllmodifierSuprimerProduct(this);
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        fxmlLoader.setController(cpe);
            p=fxmlLoader.load();
            cpe.setCBStoreText(prod.getB().getNom());
            cpe.setTAdescriptionText(prod.getDescription());
            cpe.setTFPriceText(String.valueOf(prod.getPrix()));
            cpe.setTFQuantiteText(String.valueOf(prod.getQuantite()));
            cpe.setTFbarcodeText(prod.getCodeabar());
            cpe.setTFproductNameText(prod.getLibelle());
            cpe.setidProduit(String.valueOf(prod.getId()));
            cpe.setNewCollection(prod.getNewcollection());
            if(!CategorieProduit.equals(""))
            cpe.setCBcategorieProduit(CategorieProduit);
            paneData.getChildren().clear();
        paneData.getChildren().addAll(p);
        } catch (IOException ex) {
            System.out.println("Controllers.controllMenuGestionResponsableGenerale.loadProduitIntoPanelData()");
            Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void LoadListeProduit(String filter) {
        
        File imgecheck = null;
        DProduit daoproduit = new DProduit();
        DaoImageProduct daoIP = new DaoImageProduct();
        ArrayList<Produit> listeProduit = null;
        Map<Integer, String> listeProduitImage = null;
        listeProduit = daoproduit.getlisteproduitofuser(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId(),filter);
        listeProduitImage = daoIP.ImageOfProduct();
        URL location = ControllProduitExemple.class.getResource("/GUI/ProduitExemple.fxml");
        AnchorPane p = null;
        TextFlow test = new TextFlow();
        test.setPrefWidth(998);
        test.setLineSpacing(14);

        for (int i = 0; i < listeProduit.size(); i++) {
            Produit produit = listeProduit.get(i);
            //.out.println(produit.getId());
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            ControllProduitExemple cpe = new ControllProduitExemple(this);

            try {
                fxmlLoader.setController(cpe);
                p = fxmlLoader.load();
                cpe.setIdproduit(String.valueOf(produit.getId()));
                cpe.setTXTQuantite(Integer.toString(produit.getQuantite()));
                cpe.setTXTStoreName(produit.getB().getNom());
                cpe.setTXTproduitName(produit.getLibelle());
                if (listeProduitImage.containsKey(produit.getId())) {
                    
                    imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\"+listeProduitImage.get(produit.getId()));
                    if (!imgecheck.exists()) {
                        imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\noavailable.png");
                    }
                } else {//fama image manijimch i7ilha mfas5a mil dossier
                    imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\noavailable.png");
                }
                cpe.getIVproduitImage().setImage(new Image(imgecheck.toURI().toURL().toString(),92,92,false,false));
                if (produit.getQuantite() >= 10) 
                    cpe.setTXTEnStock(2);
                    else if(produit.getQuantite() > 0)
                        cpe.setTXTEnStock(1);
                else 
                    cpe.setTXTEnStock(0);
                
            } catch (IOException ex) {
                Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
            }
            cpe.setIVstar(produit.getNewcollection());
            cpe.setIdproduit(String.valueOf(produit.getId()));

            test.getChildren().add(p);
        }
        paneData.getChildren().clear();
        paneData.getChildren().addAll(test);
    }

    @FXML
    private void ButtonBackToMenuResponsable(ActionEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menu_responsable.fxml"))));
    }

    public  controllMenuGestionResponsableGenerale getInstance() {
        return instance;
    }

    public ListView<String> getListMenuOfGestion() {
        return listMenuOfGestion;
    }

    @FXML
    private void rechercheavanceAction()
    {
        LoadListeProduit(recherche.getText());
    }
    
}
