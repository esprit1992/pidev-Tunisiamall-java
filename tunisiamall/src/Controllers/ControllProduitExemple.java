/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllProduitExemple implements Initializable {

    @FXML
    private AnchorPane APproduit;
    @FXML
    private Button pbGoToProduit;
    @FXML
    private ImageView IVproduitImage;
    @FXML
    private Text TXTEnStock;
    @FXML
    private Text TXTproduitName;
    @FXML
    private Text TXTStoreName;
    @FXML
    private Text TXTQuantite;
    @FXML
    private Label Idproduit;
     @FXML
    private ImageView IVstar;
    private controllMenuGestionResponsableGenerale controllerParent;
    ControllProduitExemple(controllMenuGestionResponsableGenerale aThis) {
        controllerParent=aThis;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                      //  
    }    

    @FXML
    private void AffichierProduit(MouseEvent event) {
      controllerParent.loadProduitIntoPanelData(Integer.valueOf(Idproduit.getText()));
    }



    public void setIdproduit(String id) {
        
        Idproduit.setText(id);
    }

    public AnchorPane getAPproduit() {
        return APproduit;
    }

    public ImageView getIVproduitImage() {
        return IVproduitImage;
    }

    public Text getTXTEnStock() {
        return TXTEnStock;
    }

    public Text getTXTproduitName() {
        return TXTproduitName;
    }

    public Text getTXTStoreName() {
        return TXTStoreName;
    }

    public Text getTXTQuantite() {
        return TXTQuantite;
    }

    public Label getIdproduit() {
        return Idproduit;
    }

    public void setPbGoToProduit(Button pbGoToProduit) {
        //TODO
    }

    public void setIVproduitImage(Image IVproduitImage) {
        
      this.IVproduitImage.setImage(IVproduitImage);
    }

    public void setTXTEnStock(int TXTEnStock) {
        if(TXTEnStock==0)//red
        {
            this.TXTEnStock.setStyle("-fx-fill:#AC193D");
             pbGoToProduit.setStyle("-fx-background-color:#BF1E4B");
             this.TXTEnStock.setText("Out Of Stock");
        }
        else{//orange
            this.TXTEnStock.setText("in Stock");
            if(TXTEnStock==1)
            {
                this.TXTEnStock.setStyle("-fx-fill:#D24726");
        pbGoToProduit.setStyle("-fx-background-color:#DC572E");
            }
            else if(TXTEnStock==2){//green
                this.TXTEnStock.setStyle("-fx-fill:#008A00");
        pbGoToProduit.setStyle("-fx-background-color:#00A600");
            }
        
        }
    }

    public void setTXTproduitName(String TXTproduitName) {
        this.TXTproduitName.setText(TXTproduitName);
    }

    public void setTXTStoreName(String TXTStoreName) {
        this.TXTStoreName.setText(TXTStoreName);
    }

    public void setTXTQuantite(String TXTQuantite) {
       this.TXTQuantite.setText(TXTQuantite);
    }

   public void setIVstar(int visible) {
        if(visible==1)
        this.IVstar.setVisible(true);
        else
            this.IVstar.setVisible(false);
    }
    
    
    
}
