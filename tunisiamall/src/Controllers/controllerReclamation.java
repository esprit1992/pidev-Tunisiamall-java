/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoReclamation;
import ENTITY.Reclamation;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javax.security.auth.Subject;

/**
 * FXML Controller class
 *
 * @author mirak
 */
public class controllerReclamation implements Initializable {
    @FXML
    private AnchorPane paneadduser;
    @FXML
    private Button ButtonValiderRegister;
    @FXML
    private Label LBlaberror;
    @FXML
    private TableView<Reclamation> table;
    @FXML
    private TableColumn<Reclamation,Integer> iduser;
    @FXML
    private TableColumn<Reclamation, String> sujet;
    @FXML
    private TableColumn<Reclamation, String> categorie;
    @FXML
    private TableColumn<Reclamation, Boolean> traiter;
        Reclamation recactuel=new Reclamation();
    @FXML
    private Button ButtonSupprimer;
    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Button btnBack;
    @FXML
    private Text textNameUser;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private Text TextLocation;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;
    @FXML
    private Button print;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                     textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());

             listMenuOfGestion.getItems().addAll("  Reclamation");
        listMenuOfGestion.getSelectionModel().select(0);
             
        TextLocation.setText("Reclamation");
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        
    });
        
         iduser.setCellValueFactory(
            new PropertyValueFactory<Reclamation,Integer>("id_user"));

     sujet.setCellValueFactory(
            new PropertyValueFactory<Reclamation,String>("sujet"));

 categorie.setCellValueFactory(
            new PropertyValueFactory<Reclamation,String>("categorie"));
 
  traiter.setCellValueFactory(
            new PropertyValueFactory<Reclamation,Boolean>("traiter"));
 
       table.getSelectionModel().selectedIndexProperty().addListener((obs,oldSelection,newSelection)->{
        
       if((newSelection != null) &&(!newSelection.equals(-1))){
         System.out.println("new selection:"+newSelection);
            //nomboutiquetextfield.setText(tableaffiche.getSelectionModel().getSelectedItem().getNom());
           recactuel.setId(table.getSelectionModel().getSelectedItem().getId());
             recactuel.setId_user(table.getSelectionModel().getSelectedItem().getId_user());
            recactuel.setSujet(table.getSelectionModel().getSelectedItem().getSujet());
              recactuel.setCategorie(table.getSelectionModel().getSelectedItem().getCategorie());

        
       }});
        afficher();
    }
    @FXML
    private void ButtonCreateNewAccountAction(ActionEvent event) {
      //  test.setText(String.valueOf(recactuel.getId()));
        DaoReclamation dao=new DaoReclamation();
        dao.updateReclamation(recactuel);
        afficher();
    }
    
       public void afficher(){

         DaoReclamation daoboutique=new DaoReclamation() ;
         table.getItems().clear(); 
         table.setItems(daoboutique.displayReclamation());

    }

    @FXML
    private void ButtonSupprimer(ActionEvent event) {
        DaoReclamation dao=new DaoReclamation();
          dao.deleteReclamation(recactuel);
           afficher();
    }

    @FXML
    private void ButtonBack(ActionEvent event) {
    }

    @FXML
    private void btnBackAction(MouseEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuAdmin.fxml"))));
    }

    @FXML
    private void printAction(MouseEvent event) {
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setJobName(" Print Component ");
        pj.setPrintable (new Printable() {
        

            @Override
            public int print(Graphics pg, PageFormat pf, int pageNum) throws PrinterException {
                 if (pageNum > 0){
                    return Printable.NO_SUCH_PAGE;
                }
                 Graphics2D g2 = (Graphics2D) pg;
                g2.translate(pf.getImageableX(), pf.getImageableY());
                WritableImage image = paneadduser.snapshot(new SnapshotParameters(), null);
                
                g2.drawImage(SwingFXUtils.fromFXImage(image, null),null, 1, 1);
                
                return Printable.PAGE_EXISTS;
            }
            
        });
        if (pj.printDialog() == false)
        return;
        try {
            pj.print();
        } catch (PrinterException ex) {
            // handle exception
        }
    }
}
