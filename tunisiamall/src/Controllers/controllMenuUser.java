/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DProduit;
import ENTITY.Produit;
import animation.FadeInLeftTransition;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author user
 */
public class controllMenuUser implements Initializable {
    @FXML
    private ImageView LBimguser;
    @FXML
    private Label lblClose;
    @FXML
    private Button LBProfile;
    @FXML
    private Button LBstatistique;
    @FXML
    private Button LBBoutique;
    @FXML
    private Button LBPanier;
    @FXML
    private Text lblWelcome11;
    @FXML
    private Text lblWelcome111;
    @FXML
    private Text lblWelcome112;
    @FXML
    private Text lblWelcome113;
    @FXML
    private Label lbTitre;
    @FXML
    private Label LBnomuser;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      LBnomuser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
       new FadeInLeftTransition(LBimguser).play();
       new FadeInLeftTransition(LBProfile).play();
       new FadeInLeftTransition(LBstatistique).play();
       new FadeInLeftTransition(LBPanier).play();
       new FadeInLeftTransition(LBBoutique).play();
       new FadeInLeftTransition(lbTitre).play();
       new FadeInLeftTransition(LBimguser).play();
       new FadeInLeftTransition(LBnomuser).play();
       
       lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
    }    

    @FXML
    private void ButtonProfileAction(MouseEvent event) throws IOException {
        
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/EditProdile.fxml"))));
    }

    @FXML
    private void ButtonStatistiqueAction(MouseEvent event) throws IOException {
       tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/UserStat.fxml")))); 
        
    }

    @FXML
    private void ButtonBoutiqueAction(MouseEvent event) throws IOException {

      
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/listedesboutique.fxml"))));
        
   
    }

    @FXML
    private void ButtonPanierAction(MouseEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/Panier.fxml"))));
    }
    
}
