/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DProduit;
import Dao.DaoCommentaire;
import Dao.DaoImageProduct;
import Dao.DaoUtilisateur;
import ENTITY.Commentaire;
import ENTITY.ImageProduct;
import ENTITY.Produit;
import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllAffichierProduit implements Initializable {
    @FXML
    private AnchorPane AnchorAddProduct;
    @FXML
    private Button BPAjouterPanier;
    @FXML
    private AnchorPane AnshorSlideImage;
    @FXML
    private AnchorPane AnshorCommentaire;
    @FXML
    private Label LBPrixUnite;
    @FXML
    private TextArea TextDescription;
    @FXML
    private ComboBox<Integer> CBQuantite;
    @FXML
    private Label LBPrixTotal;
    @FXML
    private Label LBIdProduit;
    @FXML
    private Label LBnameBoutique;
    @FXML
    private Button pbAjouterCommentaire;
    @FXML
    private TextArea TxtCommentaire;
    @FXML
    private Text TxtNameProduit;
    private ArrayList<ImageProduct> imageArrayList=null;
    int slideshowCount=0;
    @FXML
    private ImageView ImageViewSlide; 
    private File imgecheck;




    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CBQuantite.getSelectionModel().select(null);
        CBQuantite.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                 if(newValue!=null)
                LBPrixTotal.setText(String.valueOf(Integer.valueOf(CBQuantite.getSelectionModel().getSelectedItem())*Double.valueOf(LBPrixUnite.getText()))+"DT");
                 else
                      LBPrixTotal.setText("0DT");
            });
                
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                new DaoCommentaire().CheckThred(LBnameBoutique.getText(),LBIdProduit.getText());
                loadProduit();
                loadCommentaire();
                Task task = new Task<Void>() {
                    @Override
            public Void call() throws Exception {
                        for (int i = 0; i < imageArrayList.size(); i++) {
                            Platform.runLater(new Runnable() {
                                @Override
            public void run() {
                                                imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\"+imageArrayList.get(slideshowCount).getChemain());
                                        
                    if (!imgecheck.exists()) 
                        imgecheck = new File("C:\\wamp\\www\\PIDEV\\web\\bundles\\tunisiamall\\uploads\\ImgBoutique\\noavailable.png");
                 
                
                                    try {
                                        ImageViewSlide.setImage(new Image(imgecheck.toURI().toURL().toString(),602,231,false,false));
                                    } catch (MalformedURLException ex) {
                                        Logger.getLogger(ControllAffichierProduit.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    slideshowCount++;
                                    if (slideshowCount >= imageArrayList.size()) {
                                        slideshowCount = 0;
                                    }
                                }
                            });

                        Thread.sleep(4000);
                            if(i==imageArrayList.size()-1)
                                i=0;
                    }
                        
                    return null;
                }
            };
            Thread th = new Thread(task);
            th.setDaemon(true);
            th.start();
            }
        });
        CBQuantite.getItems().addAll(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
        LBPrixTotal.setText("0DT");
        
       
     
    

        }
        
        
       

    @FXML
    private void AjouterPanier(MouseEvent event) {
        if(CBQuantite.getSelectionModel().getSelectedItem()==null)
        {
            TrayNotification tray = new TrayNotification("Error", "Select Quantite", Notifications.ERROR,Animations.POPUP);
           tray.showAndDismiss(Duration.seconds(2));
        }
        else{
        tunisiamallMain.Tunisiamall.getInstance().addProduitToPanier(Integer.valueOf(LBIdProduit.getText()),Integer.valueOf(CBQuantite.getSelectionModel().getSelectedItem()));
        CBQuantite.getSelectionModel().select(null);
        LBPrixTotal.setText("0DT");
        TrayNotification tray = new TrayNotification("Succes", "Add Succefly To Cart", Notifications.SUCCESS,Animations.SLIDE);
           tray.showAndDismiss(Duration.seconds(2));
    }
    }
    @FXML
    private void pbAjouterCommentaireAction(MouseEvent event) {
        new DaoCommentaire().addCommentaire(new Commentaire(LBnameBoutique.getText()+LBIdProduit.getText(),TxtCommentaire.getText(), null, 0,tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId(),"http://localhost/pidev/web/app_dev.php/boutique/"+LBnameBoutique.getText()+"/"+LBIdProduit.getText(), 1));
        loadCommentaire();
        TxtCommentaire.setText("");
        
    }
    

    public Label getLBPrixUnite() {
        return LBPrixUnite;
    }

    public void setLBPrixUnite(String LBPrixUnite) {
        this.LBPrixUnite.setText(LBPrixUnite);
    }

    public TextArea getTextDescription() {
        return TextDescription;
    }

    public void setTextDescription(String TextDescription) {
        this.TextDescription.setText(TextDescription);
    }

    public String getLBIdProduit() {
        return LBIdProduit.getText();
    }

    public void setLBIdProduit(String LBIdProduit) {
        this.LBIdProduit.setText(LBIdProduit);
    }
    public void setLBNameBoutique(String LBnameBoutique) {
        this.LBnameBoutique.setText(LBnameBoutique);
    }
    void loadProduit()
    {
    
        DProduit daoproduit = new DProduit(); 
        Produit prod=new Produit();
        prod=daoproduit.getProduit(Integer.valueOf(LBIdProduit.getText()));
        TextDescription.setText(prod.getDescription());
        LBPrixUnite.setText(String.valueOf(prod.getPrix()));
        TxtNameProduit.setText(prod.getLibelle());
        DaoImageProduct daoimageproduct=new DaoImageProduct();
        imageArrayList=daoimageproduct.ImageOfProduct(prod.getId());
    }
    
    public void loadCommentaire()
    {
        DaoCommentaire daocommentaire = new DaoCommentaire();
        ArrayList<Commentaire> listeCommentaire = null;
        listeCommentaire = daocommentaire.getlisteCommentaire(LBnameBoutique.getText(),LBIdProduit.getText());
        URL location = ControllProduitExemple.class.getResource("/GUI/Commantaire.fxml");
        AnchorPane p = null;
        TextFlow test = new TextFlow();
        test.setPrefWidth(190);
        test.setLineSpacing(0);

        for (int i = 0; i < listeCommentaire.size(); i++) {
            Commentaire commentaire = listeCommentaire.get(i);
            //.out.println(produit.getId());
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            ControllCommentaire cpe = new ControllCommentaire(this);

            try {
                fxmlLoader.setController(cpe);
                p = fxmlLoader.load();
                cpe.setLBCommentId(String.valueOf(commentaire.getId_cmt()));
                cpe.setLBNameOfUserCommented(new DaoUtilisateur().getNameUserFromId(commentaire.getUser_id()));
                cpe.setTextComment(commentaire.getBody());
                cpe.setIdThred(commentaire.getId_Thred());
              
                
               
               
            } catch (IOException ex) {
                Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
            }
           

            test.getChildren().add(p);
        }
        AnshorCommentaire.getChildren().clear();
        AnshorCommentaire.getChildren().addAll(test);
    }
    
}
