/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoUtilisateur;
import ENTITY.Utilisateur;
import animation.FadeInDowntransition;
import animation.FadeInUpTransition;
import animation.FadeOutUpTransition;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author siwar-pc
 */
public class ActiveuserController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label lblClose;
    @FXML
    private AnchorPane paneData;
    @FXML
    private Text TextLocation;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnaction;
    @FXML
    private Text textNameUser;
    @FXML
    private TableView <Utilisateur>tableuser;
    @FXML
    private TableColumn<Utilisateur,Integer> id;
    @FXML
    private TableColumn <Utilisateur,String> nom;
    @FXML
    private TableColumn <Utilisateur,String> prenom;
    @FXML
    private TableColumn <Utilisateur,String>role;
    @FXML
    private TableColumn <Utilisateur,Boolean>status;
    @FXML
    private TextField txtid;
    @FXML
    private TextField txtnom;
    @FXML
    private TextField txtprenom;
    @FXML
    private TextField txtage;
    @FXML
    private TextField txtadresse;
    @FXML
    private TextField txtemail;
    @FXML
    private TextField txtusername;
    @FXML
    private TextField txtrole;
    @FXML
    private TextField txtstatus;
    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Label lblid;
    @FXML
    private Label lblrole;
    @FXML
    private Label lblstatus;
    @FXML
    private Label lblusername;
    @FXML
    private Label lbladresse;
    @FXML
    private Label lblemail;
    @FXML
    private Label lblage;
    @FXML
    private Label lblnom;
    @FXML
    private Label lblprenom;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private ProgressBar bar;
    @FXML
    private ImageView imagebtn;
    @FXML
    private AnchorPane DetailleUser;
    @FXML
    private ScrollPane SPdataTable;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SPdataTable.setOpacity(0);
             textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
DetailleUser.setOpacity(0);
             listMenuOfGestion.getItems().addAll("  Manage users");
        listMenuOfGestion.getSelectionModel().select(0);
             
        TextLocation.setText("Manage users");
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        
    });
        Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = 30;
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
        service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        service.setOnSucceeded((WorkerStateEvent event) -> {
             bar.progressProperty().unbind();
             bar.setProgress(0);
             afficher();
             new FadeInDowntransition(SPdataTable).play(); 
            imgLoad.setVisible(false);});
        
      
        tableuser.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                 try {
                     afficherinfo(newValue);
                 } catch (MalformedURLException ex) {
                     Logger.getLogger(ActiveuserController.class.getName()).log(Level.SEVERE, null, ex);
                 }
             });
       
    }    
     @FXML
        private void ButtonBack(ActionEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuAdmin.fxml"))));
    }
          public void afficher()
    {
DaoUtilisateur duser =new DaoUtilisateur();
//System.out.print(duser.getusers().size());
//System.out.print(id.toString());
 id.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,Integer>("id"));

     
 nom.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("nom"));

 prenom.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("prenom"));
 role.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,String>("roles"));
 status.setCellValueFactory(
            new PropertyValueFactory<Utilisateur,Boolean>("enabled"));

    tableuser.setItems(duser.getusers());
 
} 
        
          public void afficherinfo(Utilisateur u) throws MalformedURLException
    {
        DetailleUser.setOpacity(0); 
       if (u != null) {
        String s = "" +u.getId();        
        txtid.setText(s);
        txtnom.setText(u.getNom());
        txtprenom.setText(u.getPrenom());
        String sage=""+u.getAge();
        txtage.setText(sage);
        txtusername.setText(u.getUsername());
         txtadresse.setText(u.getAdresse());
         txtemail.setText(u.getEmail());
         txtrole.setText(u.getRoles());
         if(u.getEnabled()==0){
         
         txtstatus.setText("Locked");
          btnaction.setStyle("-fx-background-color:#00A600");      
          imagebtn.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\\\Ressources\\\\success.png").toURI().toURL().toString()));
          btnaction.setText("Unlocked");
         }
         else {
          txtstatus.setText("Unlocked");
         imagebtn.setImage(new Image(new File("C:\\Users\\user\\Documents\\tunisiamallJAVA\\tunisiamall\\src\\\\Ressources\\\\error_2.png").toURI().toURL().toString()));
         btnaction.setStyle("-fx-background-color:#BF1E4B");
         btnaction.setText("Locked");
         }
       
          new FadeInDowntransition(DetailleUser).play(); 
                    

       }
       else{
        txtid.setText("");
        txtnom.setText("");
        txtprenom.setText("");
        txtage.setText("");
        txtadresse.setText("");
        txtusername.setText("");
        txtemail.setText("");
        txtrole.setText("");
        txtstatus.setText("");
       
        
       }
    }

    @FXML
    private void changerstatus(ActionEvent event) {
                    DaoUtilisateur duser= new DaoUtilisateur();

        if(txtstatus.getText().equals("Locked"))
        {
                   //appeldao
            
            duser.acceptuser(Integer.parseInt(txtid.getText()));
        }
        else if(txtstatus.getText().equals("Unlocked")){
            duser.desactiveuser(Integer.parseInt(txtid.getText()));
        }
        new FadeInUpTransition(DetailleUser).play(); 
        afficher();
        DetailleUser.setOpacity(0);
    }
}
