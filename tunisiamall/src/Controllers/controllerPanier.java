/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import ENTITY.ProduitDePanier;
import animation.FadeInDowntransition;
import animation.FadeInUpTransition;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class controllerPanier implements Initializable {
    @FXML
    private ListView<?> listMenuOfGestion;
    @FXML
    private Button btnBack;
    @FXML
    private Text textNameUser;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private Text TextLocation;
    @FXML
    private AnchorPane paneadduser;
    @FXML
    private TableView<ProduitDePanier> table;
    @FXML
    private TableColumn<ProduitDePanier,String> ProductName;
    @FXML
    private TableColumn<ProduitDePanier, Integer> Quantiter;
    @FXML
    private TableColumn<ProduitDePanier, Double> PrixUnite;
    @FXML
    private TableColumn<ProduitDePanier, Double> PrixTotaleUniter;
    @FXML
    private TextArea AdresseLB;
    @FXML
    private Label PrixTotalLB;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;
    @FXML
    private Button pbSupprimer;
    @FXML
    private TableColumn<ProduitDePanier, Integer> id;
    @FXML
    private Button ConfirmAchat;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());

          AdresseLB.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getAdresse());
        TextLocation.setText("Panier");
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        
    });
        ProductName.setCellValueFactory(
            new PropertyValueFactory<ProduitDePanier,String>("ProductName"));
        Quantiter.setCellValueFactory(
            new PropertyValueFactory<ProduitDePanier,Integer>("Quantiter"));
        PrixUnite.setCellValueFactory(
            new PropertyValueFactory<ProduitDePanier,Double>("PrixUnite"));
        PrixTotaleUniter.setCellValueFactory(
            new PropertyValueFactory<ProduitDePanier,Double>("PrixTotaleUniter"));
        id.setCellValueFactory(
            new PropertyValueFactory<ProduitDePanier,Integer>("id"));
        table.getSelectionModel().selectedIndexProperty().addListener((obs,oldSelection,newSelection)->{
        
       if((newSelection != null) &&(!newSelection.equals(-1))){
         if(pbSupprimer.getOpacity()!=1){
             pbSupprimer.setVisible(true);
              new FadeInDowntransition(pbSupprimer).play(); 

         }
       }});
        loadTable();
        AffichierPrixTotale();
        
    }    

    @FXML
    private void btnBackAction(MouseEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
    }

    @FXML
    private void ButtonBack(ActionEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
    }
    public void loadTable()
    {
        table.getItems().clear();
        table.setItems(new Dao.DProduit().displayPanier());
    }

    @FXML
    private void pbSupprimerAction(MouseEvent event) {
        pbSupprimer.setVisible(false);
              new FadeInUpTransition(pbSupprimer).play(); 
        tunisiamallMain.Tunisiamall.getInstance().getPanier().remove(table.getSelectionModel().getSelectedItem().getId(),table.getSelectionModel().getSelectedItem().getQuantiter());
        loadTable();
    }
    void AffichierPrixTotale()
    {
      PrixTotalLB.setText(new Dao.DProduit().SommeProduitPrice());
    }

    @FXML
    private void ConfirmAchatAction(MouseEvent event) throws IOException {
        Double SoldeUSer=0.0;
        if(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getSolde()<Double.valueOf(PrixTotalLB.getText()))
        {
            TrayNotification tray = new TrayNotification("Error", "Solde Insufisant", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
        } 
        else
            if(AdresseLB.getText().equals(""))
            {
                TrayNotification tray = new TrayNotification("Error", "Adress Empty", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
            }
            else
                if(tunisiamallMain.Tunisiamall.getInstance().getPanier().size()<=0)
                {
                    TrayNotification tray = new TrayNotification("Error", "Panier Vide", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
                }
        else {
            new Dao.DProduit().passerCommande(AdresseLB.getText());
            TrayNotification tray = new TrayNotification("succes", "Commande OK", Notifications.SUCCESS);
        tray.showAndDismiss(Duration.seconds(3));
         tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
         tunisiamallMain.Tunisiamall.getInstance().getPanier().clear();
        }
    }
}
