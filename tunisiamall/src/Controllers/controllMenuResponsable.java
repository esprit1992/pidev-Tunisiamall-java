/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import animation.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

/**
 *
 * @author user
 */
public class controllMenuResponsable implements Initializable {
    
    @FXML
    private Label lbTitre; 
     @FXML
    private Label LBnomuser;
     @FXML
    private ImageView LBimguser;
    @FXML
    private Label lblClose;
     @FXML
    private Button LBstatistique;
     @FXML
    private Button LBstore;
     @FXML
    private Button LBproduct;
     @FXML
    private Button LBadvertising;
    @FXML
    private Text lblWelcome11;
    @FXML
    private Text lblWelcome111;
    @FXML
    private Text lblWelcome112;
    @FXML
    private Text lblWelcome113;
    
     
     
     
             
     

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       LBnomuser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
       new FadeInLeftTransition(LBstatistique).play();
       new FadeInLeftTransition(LBstore).play();
       new FadeInLeftTransition(LBproduct).play();
       new FadeInLeftTransition(LBadvertising).play();
       new FadeInLeftTransition(LBnomuser).play();
       new FadeInLeftTransition(lbTitre).play();
       new FadeInLeftTransition(LBimguser).play();
        
        
        
        
        lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
    }

    @FXML
    private void affichierProductMenu(MouseEvent event) throws IOException  {

        URL location = controllMenuGestionResponsableGenerale.class.getResource("/GUI/MenuGestion.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        controllMenuGestionResponsableGenerale cmg=new controllMenuGestionResponsableGenerale();
        fxmlLoader.setController(cmg);
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene((Parent) fxmlLoader.load()));
      
    }

    @FXML
    private void AffichierMenuStoreAction(MouseEvent event) {
        URL location = controllMenuGestionResponsableGenerale.class.getResource("/GUI/MenuGestion.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        controllMenuGestionResponsableBoutique cmg=new controllMenuGestionResponsableBoutique();
        fxmlLoader.setController(cmg);
        try {
            tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene((Parent) fxmlLoader.load()));
        } catch (IOException ex) {
            Logger.getLogger(controllMenuResponsable.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("iciiii");
        }
    }

    @FXML
    private void LBadvertisingAction(MouseEvent event) {
        URL location = controllMenuGestionResponsableGenerale.class.getResource("/GUI/MenuGestion.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        controllMenuGestionResponsablePackPub cmg=new controllMenuGestionResponsablePackPub();
        fxmlLoader.setController(cmg);
        try {
            tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene((Parent) fxmlLoader.load()));
        } catch (IOException ex) {
            Logger.getLogger(controllMenuResponsable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void AffichierStatistiqueAction(MouseEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/ResponsableStat.fxml"))));
    }
    
    
}
