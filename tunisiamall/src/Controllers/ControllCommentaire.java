/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoCommentaire;
import animation.FadeInDowntransition;
import animation.FadeInUpTransition;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllCommentaire implements Initializable {
     @FXML
    private AnchorPane anshorglobal;
    @FXML
    private ImageView LBimguser;
    @FXML
    private Label LBNameOfUserCommented;
    @FXML
    private TextArea TextComment;
    @FXML
    private Label LBCommentId;
    @FXML
    private Label LBSuprrimerComment;
    @FXML
    private Label LBModifierCommentaire;
    @FXML
    private Button Pbpublier;
    @FXML
    private Label IdThred;
    ControllAffichierProduit parent;

    /**
     * Initializes the controller class.
     */
    
    public ControllCommentaire() {
    }

    public ControllCommentaire(ControllAffichierProduit aThis) {
        parent=aThis;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         TextComment.setEditable(false);
        Pbpublier.setVisible(false);
        Pbpublier.setDisable(true);
        LBSuprrimerComment.setOnMouseClicked((MouseEvent event) -> {
          new DaoCommentaire().deleteCommentaire(Integer.valueOf(LBCommentId.getText()),IdThred.getText());
          parent.loadCommentaire();
        });
        LBModifierCommentaire.setOnMouseClicked((MouseEvent event) -> {
            
            Pbpublier.setVisible(true);
            new FadeInDowntransition(Pbpublier).play();
            new FadeInUpTransition(LBModifierCommentaire).play();   
            LBModifierCommentaire.setDisable(true);
            Pbpublier.setDisable(false);
            TextComment.setEditable(true);
            LBModifierCommentaire.setVisible(false);
        });
    }    

    public Label getLBNameOfUserCommented() {
        return LBNameOfUserCommented;
    }

    public void setLBNameOfUserCommented(String LBNameOfUserCommented) {
        this.LBNameOfUserCommented.setText(LBNameOfUserCommented);
    }

    public TextArea getTextComment() {
        return TextComment;
    }

    public void setTextComment(String TextComment) {
        this.TextComment.setText(TextComment);
    }

    public Label getLBCommentId() {
        return LBCommentId;
    }

    public void setLBCommentId(String LBCommentId) {
        this.LBCommentId.setText(LBCommentId);
    }

    public Label getLBSuprrimerComment() {
        return LBSuprrimerComment;
    }

    public void setLBSuprrimerComment(Label LBSuprrimerComment) {
        //toDo Delete Buttom
    }
     @FXML
    private void pbPublierAction(MouseEvent event) {
            LBModifierCommentaire.setVisible(true);
            new FadeInDowntransition(LBModifierCommentaire).play();
            new FadeInUpTransition(Pbpublier).play();  
            LBModifierCommentaire.setDisable(false);
            Pbpublier.setDisable(true);
            TextComment.setEditable(false);
            new DaoCommentaire().UpdateComment(LBCommentId.getText(),TextComment.getText());
            Pbpublier.setVisible(false);
    }

    public void setIdThred(String IdThred) {
        this.IdThred.setText(IdThred);
    }
    
}
