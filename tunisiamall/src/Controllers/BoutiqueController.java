/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author siwar-pc
 */
public class BoutiqueController implements Initializable {
    @FXML
    private AnchorPane APboutique;
    @FXML
    private Text TXTStoreName;
    @FXML
    private Label idboutique;
    @FXML
    private Text TXTQuantite;
    @FXML
    private Button GoToboutique;
    @FXML
    private ImageView boutiqueImage;
    private ListedesboutiqueController parent;

    BoutiqueController(ListedesboutiqueController aThis) {
        parent=aThis;
    }



    public AnchorPane getAPboutique() {
        return APboutique;
    }

    public void setAPboutique(AnchorPane APboutique) {
        this.APboutique = APboutique;
    }

    public Text getTXTStoreName() {
        return TXTStoreName;
    }

    public void setTXTStoreName(String TXTStoreName) {
        this.TXTStoreName.setText(TXTStoreName);
    }

    public Label getIdboutique() {
        return idboutique;
    }

    public void setIdboutique(String idboutique) {
        this.idboutique.setText(idboutique); 
    }

    public Text getTXTQuantite() {
        return TXTQuantite;
    }

    public void setTXTQuantite(String TXTQuantite) {
        this.TXTQuantite.setText(TXTQuantite);
    }

    public Button getGoToboutique() {
        return GoToboutique;
    }

    public void setGoToboutique(Button GoToboutique) {
        this.GoToboutique = GoToboutique;
    }

    public ImageView getBoutiqueImage() {
        return boutiqueImage;
    }

    public void setBoutiqueImage(Image boutiqueImage) {
        this.boutiqueImage.setImage(boutiqueImage);
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                GoToboutique.setStyle("-fx-background-color:#00A0B1");

    }    

    @FXML
    private void AffichierProduitboutique(MouseEvent event) {
        parent.action(Integer.parseInt(idboutique.getText()));
       
    }
    
}
