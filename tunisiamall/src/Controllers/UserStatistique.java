/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoUtilisateur;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author user
 */
public class UserStatistique implements Initializable {
    @FXML
    private ListView<?> listMenuOfGestion;
    @FXML
    private Button btnBack;
    @FXML
    private Text textNameUser;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private Text TextLocation;
    @FXML
    private AnchorPane paneadduser;
    @FXML
    private AnchorPane NbVenteByBoutique;
    @FXML
    private BarChart<String, Integer> barchart;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;
    private DaoUtilisateur daouser=new DaoUtilisateur();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
        TextLocation.setText("statistics");
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        });
         Platform.runLater(new Runnable() {
                                @Override
            public void run() {
                                   
               if(daouser.StatUtilisateur().getName().equals("null")){
                    System.out.println("fuckk");
        barchart.getData().add(daouser.StatUtilisateur());
        barchart.setLegendVisible(false);
        barchart.setAnimated(true);
                
               }
        
                                }
                            });
    }    

    @FXML
    private void ButtonBack(ActionEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
    }
    
}
