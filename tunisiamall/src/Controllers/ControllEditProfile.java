/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoUtilisateur;
import ENTITY.Utilisateur;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllEditProfile implements Initializable {
    @FXML
    private AnchorPane paneadduser;
    @FXML
    private Button ButtonValiderRegister;
    @FXML
    private TextField TFusernamecr;
    @FXML
    private Label LBusernamecr;
    @FXML
    private Label LBpasswordcr;
    @FXML
    private Label LBemailcr;
    @FXML
    private Label LBadressecr;
    @FXML
    private Label LBlastnameCR;
    @FXML
    private Label LBfirstnamecr;
    @FXML
    private Label LBsexcr;
    @FXML
    private Label LBagecr;
    @FXML
    private Label LBtelnumbercr;
    @FXML
    private Label LBcivilitecr;
    @FXML
    private PasswordField TFpasswordcr;
    @FXML
    private TextField TFlastnameCR;
    @FXML
    private ComboBox<String> CBcivilitecr;
    @FXML
    private TextField TFfirstnamecr;
    @FXML
    private ComboBox<String> CBsexcr;
    @FXML
    private TextField TFtelnumbercr;
    @FXML
    private TextField TFagecr;
    @FXML
    private TextField TFemailcr;
    @FXML
    private TextArea TFadressecr;
    @FXML
    private Label idUser;
    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Button btnBack;
    @FXML
    private Text textNameUser;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private Text TextLocation;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());

             listMenuOfGestion.getItems().addAll("  Edit Profile");
        listMenuOfGestion.getSelectionModel().select(0);
             
        TextLocation.setText("Edit Profile");
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        
    });
        
        
        
      TFusernamecr.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getUsername());
      TFlastnameCR.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
      TFfirstnamecr.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom());
      TFtelnumbercr.setText(String.valueOf(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getTel()));
      TFemailcr.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getEmail());
      TFagecr.setText(String.valueOf(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getAge()));
      TFadressecr.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getAdresse());
      CBcivilitecr.getItems().addAll("Maried", "Single", "devorced");
      CBsexcr.getItems().addAll("Mmale", "female");
      CBcivilitecr.getSelectionModel().select(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getCivilite());
      CBsexcr.getSelectionModel().select(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getSex());
      idUser.setText(String.valueOf(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));
      
              
    }    

    @FXML
    private void ButtonSaveChangeAction(ActionEvent event) throws IOException {
        Utilisateur user = null;
        int erreur=0;
        int iduser=tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId();
        if(verifUserChamps())
        {
            if(!new DaoUtilisateur().verifPassword(TFpasswordcr.getText()))
            {   
            TFpasswordcr.setStyle(" -fx-border-color: red;");
                TrayNotification tray = new TrayNotification("Error", "password Incorrect", Notifications.ERROR);
                 tray.showAndDismiss(Duration.seconds(3));
            }
            else
            {
               
                user=new Utilisateur(TFusernamecr.getText(), TFemailcr.getText(), TFpasswordcr.getText(), TFadressecr.getText(), TFfirstnamecr.getText(), TFlastnameCR.getText(), CBsexcr.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(TFagecr.getText()), Integer.parseInt(TFtelnumbercr.getText()), CBcivilitecr.getSelectionModel().getSelectedItem().toString(),"",0.0);
            erreur=new DaoUtilisateur().verifforedituser(user);
            if(erreur==1)
            {
                 TFemailcr.setStyle(" -fx-border-color: red;");
                TrayNotification tray = new TrayNotification("Error", "email alredy exist", Notifications.ERROR);
                 tray.showAndDismiss(Duration.seconds(3));
            }
            else
                if(erreur==2)
                {
                     TFtelnumbercr.setStyle(" -fx-border-color: red;");
                TrayNotification tray = new TrayNotification("Error", "Phone Number alredy exist", Notifications.ERROR);
                 tray.showAndDismiss(Duration.seconds(3));
                }
                else{
                new DaoUtilisateur().updateUser(user);
           TrayNotification tray = new TrayNotification("Success", "Yur Account Was Update Succesfully", Notifications.SUCCESS);
                 tray.showAndDismiss(Duration.seconds(3));
                 tunisiamallMain.Tunisiamall.getInstance().setLoggedUser(user);
                 tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().setId(iduser);
                  tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
            }
        }
        
    }
    }


    @FXML
    private void ButtonBack(ActionEvent event) throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuUser.fxml"))));
    }
 
    
    
    
     public boolean verifUserChamps() {
        int verif = 0;
        String style = " -fx-border-color: red;";

        String styledefault = "-fx-border-color: green;";

        TFusernamecr.setStyle(styledefault);
        TFemailcr.setStyle(styledefault);
        TFpasswordcr.setStyle(styledefault);
        TFadressecr.setStyle(styledefault);
        TFlastnameCR.setStyle(styledefault);
        TFfirstnamecr.setStyle(styledefault);
        CBsexcr.setStyle(styledefault);
        TFagecr.setStyle(styledefault);
        TFtelnumbercr.setStyle(styledefault);
        CBcivilitecr.setStyle(styledefault);
        
        if (TFusernamecr.getText().equals("")) {
            TFusernamecr.setStyle(style);
            verif = 1;
        }

        if (TFemailcr.getText().equals("")) {
            TFemailcr.setStyle(style);
            verif = 1;
        }

        if (TFpasswordcr.getText().equals("")) {
            TFpasswordcr.setStyle(style);
            verif = 1;
        }
        if (TFadressecr.getText().equals("")) {
            TFadressecr.setStyle(style);
            verif = 1;
        }

        if (TFlastnameCR.getText().equals("")) {
            TFlastnameCR.setStyle(style);
            verif = 1;
        }

        if (TFfirstnamecr.getText().equals("")) {
            TFfirstnamecr.setStyle(style);
            verif = 1;
        }

        try {
            CBsexcr.getSelectionModel().getSelectedItem().toString().equals("");
        } catch (Exception e) {
            {
                CBsexcr.setStyle(style);
                verif = 1;
            }
        }

        try {
            Integer.parseInt(TFagecr.getText().toString());
            if (Integer.parseInt(TFagecr.getText().toString()) > 100 || Integer.parseInt(TFagecr.getText().toString()) < 10) {
                TFagecr.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFagecr.setStyle(style);
                verif = 1;
            }
        }

        try {
            Integer.parseInt(TFtelnumbercr.getText().toString());
            if (Integer.parseInt(TFtelnumbercr.getText().toString()) > 99999999 || Integer.parseInt(TFtelnumbercr.getText().toString()) < 20000000) {
                TFtelnumbercr.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFtelnumbercr.setStyle(style);
                verif = 1;
            }
        }

        try {
            CBcivilitecr.getSelectionModel().getSelectedItem().toString().equals("");
        } catch (Exception e) {
            {
                CBcivilitecr.setStyle(style);
                verif = 1;
            }
        }

        System.out.println("verif=" + verif);
        if (verif == 0) {
            return true;
        }
        TrayNotification tray = new TrayNotification("Error", "Verif Your Input Please", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
        return false;
    }
    
    
    
}
