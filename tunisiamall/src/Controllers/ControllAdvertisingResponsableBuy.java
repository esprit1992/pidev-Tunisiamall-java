/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DPack;
import Dao.DProduit;
import ENTITY.Pack;
import ENTITY.Reservation;
import animation.*;
import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.TextFlow;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllAdvertisingResponsableBuy implements Initializable {
    @FXML
    private AnchorPane AnchorHome;
    @FXML
    private ToggleButton ToogleMen;
    @FXML
    private ToggleButton ToogleWomen;
    @FXML
    private ImageView imgLoad;
    @FXML
    private ProgressBar bar;
    @FXML
    private AnchorPane Anchorfemme;
    int select=1;
    @FXML
    private TextArea TXTDescription;
    @FXML
    private Label TFPrixDeJour;
    @FXML
    private DatePicker DPdateFin;
    @FXML
    private Label TFSomme;
    @FXML
    private Button PbButtonReserver;
    @FXML
    private Label IDPack;
    @FXML
    private AnchorPane anshorglobal;
    @FXML
    private AnchorPane AnshorReservation;
    @FXML
    private AnchorPane Anshor3Pack;
    @FXML
    private Label LBDateFin3Slide;
    @FXML 
    private ComboBox<String> CB3Slide1;
    @FXML 
    private ComboBox<String> CB3Slide2;
    @FXML 
    private ComboBox<String> CB3Slide3;
    @FXML
    private Button Update3Slide;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        ToogleMen.setSelected(true);
        CB3Slide1.getItems().addAll(new DProduit().getlisteProduitName(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));
        CB3Slide2.getItems().addAll(new DProduit().getlisteProduitName(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));
        CB3Slide3.getItems().addAll(new DProduit().getlisteProduitName(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));
        CB3Slide1.setValue("");
        CB3Slide2.setValue("");
        CB3Slide3.setValue("");
        Anchorfemme.setOpacity(0);
        AnchorHome.setOpacity(1);
        AnchorHome.toFront();
        PbButtonReserver.setDisable(true);
        DPdateFin.setDisable(true);
        LoadListepack();
    }    

    @FXML
    private void ToogleMenAction(MouseEvent event) {
     if(select==0){
            select=1;
       
            new FadeInDowntransition(AnchorHome).play();
        
         
            Anchorfemme.setOpacity(0);
            Anchorfemme.toBack();
            AnchorHome.toFront();
        try {
            
             Thread.sleep(700);
         } catch (InterruptedException ex) {
             Logger.getLogger(ControllAdvertisingResponsableBuy.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    }

    @FXML
    private void ToogleWomenAction(MouseEvent event) {
        
         if(select==1){
            select=0;
       
            new FadeInDowntransition(Anchorfemme).play();
         
            
            AnchorHome.setOpacity(0);
            AnchorHome.toBack();
            Anchorfemme.toFront();
            try {
             Thread.sleep(700);
         } catch (InterruptedException ex) {
             Logger.getLogger(ControllAdvertisingResponsableBuy.class.getName()).log(Level.SEVERE, null, ex);
         }
        
    }}
    
    
     public void LoadListepack() {
        
         DPack daopack = new DPack();
     Reservation reserva=null;
        ArrayList<Pack> listepack = null;
        ArrayList<Reservation> listereservation = null;
        listepack = daopack.getlistepack();
        listereservation = daopack.getlistereservation();
        URL location = null;
        AnchorPane p = null;
        FlowPane verticale=new FlowPane();
        FlowPane horizentale=new FlowPane();
        FlowPane testmen = new FlowPane();
        FlowPane testwomen = new FlowPane();
        ArrayList<AnchorPane> listepackAnshorMen = new ArrayList();
        ArrayList<AnchorPane> listepackAnshorWomen = new ArrayList();
        testmen.setPrefWidth(600);
        verticale.setPrefWidth(140);
        horizentale.setPrefWidth(280);
        testwomen.setPrefWidth(560);
        
   
   

        for (int i = 0; i < listepack.size(); i++) {
            
            Pack pack = listepack.get(i);
            if(pack.getType()==1)
                location = ControllProduitExemple.class.getResource("/GUI/PubPetitEmplacementExemple.fxml");
                else
                    if(pack.getType()==2)
                            location = ControllProduitExemple.class.getResource("/GUI/PubMeduimEmplacementExemple.fxml");
                            else
                                location = ControllProduitExemple.class.getResource("/GUI/PubBigEmplacementExemple.fxml");
                                    
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            ControllEmplacement cpe = new ControllEmplacement(this);

            try {
                fxmlLoader.setController(cpe);
                p = fxmlLoader.load();
                cpe.setDisponible(String.valueOf(pack.getDisponible()));
                if(pack.getDisponible()==0)
                {
                    reserva=CheckIfThisUserReservedThisPack(pack.getId(), listereservation);
                    if(reserva!=null)//kan ili connecter howa ili chari il pack
                    {  cpe.setLBreservationText1("");
                    cpe.setDisponible("3");
                    cpe.setDatefin(String.valueOf(reserva.getDatefin()));}
                    else
                cpe.setLBreservationText1("Booked");
                }
                else
                  cpe.setLBreservationText1("Reserve now "+String.valueOf(pack.getPrixjour())+"DT/D");  

                cpe.setId(String.valueOf(pack.getId()));
                cpe.setTxtDescription(pack.getDescription());
                cpe.setTxtPrix(String.valueOf(pack.getPrixjour()));
                //cpe.set
                
          
                
            } catch (IOException ex) {
                Logger.getLogger(controllMenuGestionResponsableGenerale.class.getName()).log(Level.SEVERE, null, ex);
            }
          
            if(pack.getId()/2>=5)
            listepackAnshorMen.add(p);
            else
            listepackAnshorWomen.add(p);   
        }
//        verticale
//        horizentale
//        testmen
//        testwomen
        //Women
        testwomen.getChildren().add(listepackAnshorWomen.get(0));
        verticale.getChildren().add(listepackAnshorWomen.get(1));
        verticale.getChildren().add(listepackAnshorWomen.get(3));
        testwomen.getChildren().add(verticale);
        verticale=new FlowPane();
        verticale.setPrefWidth(140);
        verticale.getChildren().add(listepackAnshorWomen.get(2));
        verticale.getChildren().add(listepackAnshorWomen.get(4));
        testwomen.getChildren().add(verticale);
        horizentale.getChildren().add(listepackAnshorWomen.get(5));
        horizentale.getChildren().add(listepackAnshorWomen.get(6));
        testwomen.getChildren().add(horizentale);
        testwomen.getChildren().add(listepackAnshorWomen.get(7));
        
        //Men
        verticale=new FlowPane();
        verticale.setPrefWidth(140);
        verticale.getChildren().add(listepackAnshorMen.get(0));
        verticale.getChildren().add(listepackAnshorMen.get(3));
        testmen.getChildren().add(verticale);
        testmen.getChildren().add(listepackAnshorMen.get(1));
        verticale=new FlowPane();
        verticale.setPrefWidth(140);
        verticale.getChildren().add(listepackAnshorMen.get(2));
        verticale.getChildren().add(listepackAnshorMen.get(4));
        testmen.getChildren().add(verticale);
        testmen.getChildren().add(listepackAnshorMen.get(5));
        testmen.getChildren().add(listepackAnshorMen.get(6));
        testmen.getChildren().add(listepackAnshorMen.get(7));
        
        
        
        
        
        
        AnchorHome.getChildren().clear();
        AnchorHome.getChildren().addAll(testmen);
        Anchorfemme.getChildren().clear();
        Anchorfemme.getChildren().addAll(testwomen);
    }

    public TextArea getTXTDescription() {
        return TXTDescription;
    }

    public void setTXTDescription(String TXTDescription) {
        this.TXTDescription.setText(TXTDescription);
    }

    public Label getTFPrixDeJour() {
        return TFPrixDeJour;
    }

    public void setTFPrixDeJour(String TFPrixDeJour) {
        this.TFPrixDeJour.setText(TFPrixDeJour);
    }

    public Label getIDPack() {
        return IDPack;
    }

    public void setIDPack(String IDPack) {
        this.IDPack.setText(IDPack);
    }

    @FXML
    private void datepickAction(MouseEvent event) {
    }

    @FXML
    private void DPdateFinAction(ActionEvent event) {
       
        double def=(double) ChronoUnit.DAYS.between(LocalDate.now(),DPdateFin.getValue());
        System.out.println("DEF="+def);
        //System.out.println("nbj"+double.valueOf(TFPrixDeJour.getText()));
        if(def>0)
        {    
        TFSomme.setText(String.valueOf(def * Double.valueOf(TFPrixDeJour.getText())));
        PbButtonReserver.setDisable(false);
        }
        else
        {
            TFSomme.setText("");
            PbButtonReserver.setDisable(true);
        }
        
    }

    public DatePicker getDPdateFin() {
        return DPdateFin;
    }

    public Button getPbButtonReserver() {
        return PbButtonReserver;
    }

    public void setPbButtonReserver(Button PbButtonReserver) {
        this.PbButtonReserver = PbButtonReserver;
    }

    public Label getTFSomme() {
        return TFSomme;
    }

    @FXML
    private void ButtonAchatClik(MouseEvent event) {
        if(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getSolde()>Double.valueOf(this.getTFSomme().getText())){
                         Alert alert = new Alert(Alert.AlertType.INFORMATION);
alert.setTitle("Confirmation Achat");
alert.setHeaderText("");
alert.setContentText("En Click Sur Oui tu va reserver lespace de pub pour "+ ChronoUnit.DAYS.between(LocalDate.now(),DPdateFin.getValue())+" jour(s) avec un montant= "+Double.valueOf(this.getTFSomme().getText())+"DT\n Confirmer l'achat?");
ButtonType buttonTypeYes = new ButtonType("oui", ButtonBar.ButtonData.OK_DONE);
ButtonType buttonTypeNo = new ButtonType("non", ButtonBar.ButtonData.CANCEL_CLOSE);
alert.getButtonTypes().setAll(buttonTypeYes,buttonTypeNo);
alert.initStyle(StageStyle.UTILITY);
Optional<ButtonType> result = alert.showAndWait();
if (result.get() == buttonTypeYes){
    tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().setSolde(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getSolde()-Double.valueOf(this.getTFSomme().getText()));
            TrayNotification tray = new TrayNotification("Success", "Achat Effectuer Avec Succes", Notifications.SUCCESS);
            tray.showAndDismiss(Duration.seconds(3));
            DPdateFin.setDisable(true);
            PbButtonReserver.setDisable(true);
            new DPack().BayPack(Integer.valueOf(IDPack.getText()),Date.valueOf(DPdateFin.getValue()),Double.valueOf(TFSomme.getText()));
            LoadListepack();
            getTXTDescription().setText("");
            getTFPrixDeJour().setText("");
            DPdateFin.setValue(LocalDate.now());
}
        }
        else{
            TrayNotification tray = new TrayNotification("Error", "Solde Insufision Recharger Votre Account STP", Notifications.WARNING);
            tray.showAndDismiss(Duration.seconds(3));
        }
    }

    @FXML
    private void Update3SlideAction(MouseEvent event) {
  
        Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = 30;
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
        if(verifInputPackPub()){
             
        service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent eventt) -> {
            imgLoad.setVisible(true);
            bar.setOpacity(1);
        });
        service.setOnSucceeded((WorkerStateEvent eventt) -> {
             bar.progressProperty().unbind();
             bar.setProgress(0);
            imgLoad.setVisible(false);
        
        

        String idProduits="";
        String ProduitWithBoutique[];
        if(CB3Slide3.isDisable())
        {
            ProduitWithBoutique=CB3Slide1.getSelectionModel().getSelectedItem().split("-");
            idProduits=new DProduit().getProduit(ProduitWithBoutique[0],ProduitWithBoutique[1]);
            System.out.println("produitWithBoutique="+idProduits);
        }
        else
        {
            idProduits=CB3Slide1.getSelectionModel().getSelectedItem()+"-"+CB3Slide2.getSelectionModel().getSelectedItem()+"-"+CB3Slide3.getSelectionModel().getSelectedItem();
            ProduitWithBoutique=idProduits.split("-");
            idProduits=new DProduit().getProduit(ProduitWithBoutique[0],ProduitWithBoutique[1])+","+new DProduit().getProduit(ProduitWithBoutique[2],ProduitWithBoutique[3])+","+new DProduit().getProduit(ProduitWithBoutique[4],ProduitWithBoutique[5]);
            
        }
        
        new DPack().UpdateReservation(idProduits,Integer.valueOf(IDPack.getText()));
    Anshor3Pack.setOpacity(0);
    TrayNotification tray = new TrayNotification("Upload Pack", "Your Pack Was Update", Notifications.SUCCESS,Animations.SLIDE);
           tray.showAndDismiss(Duration.seconds(2));
           LoadListepack();
                });
     

    }
        else{
         TrayNotification tray = new TrayNotification("Error", "verf Input", Notifications.WARNING,Animations.SLIDE);
           tray.showAndDismiss(Duration.seconds(2));   
    }
    
    }
 
    
  

   @FXML
    private void rechercheavanceAction()
    {
    }
     
     
     
     public Reservation CheckIfThisUserReservedThisPack(int id_Pack,ArrayList<Reservation> listereservation)
{
    Reservation ret=null;
    for(int i=0;i<listereservation.size();i++)
    {
        if(listereservation.get(i).getId_pack()==id_Pack && listereservation.get(i).getId_responsable()==tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId())
        {
            ret=listereservation.get(i);
            break;
        }
    }
    return ret;
}

    public AnchorPane getAnshorReservation() {
        return AnshorReservation;
    }

    public AnchorPane getAnshor3Pack() {
        return Anshor3Pack;
    }

    public Label getLBDateFin3Slide() {
        return LBDateFin3Slide;
    }

    public ComboBox<String> getCB3Slide1() {
        return CB3Slide1;
    }

    public ComboBox<String> getCB3Slide2() {
        return CB3Slide2;
    }

    public ComboBox<String> getCB3Slide3() {
        return CB3Slide3;
    }
     
     public boolean verifInputPackPub()
     {
         String style = " -fx-border-color: red;";
boolean ret=true;
        String styledefault = "-fx-border-color: green;";
         if(CB3Slide3.getOpacity()==1)
         {
             if(CB3Slide1.getSelectionModel().getSelectedItem().toString().equals("")){
                 CB3Slide1.setStyle(style);
             ret=false;
             }
             else
                 CB3Slide1.setStyle(styledefault);
             if(CB3Slide2.getSelectionModel().getSelectedItem().toString().equals("")){
                 CB3Slide2.setStyle(style);
             ret=false;}
             else
                 CB3Slide2.setStyle(styledefault);
             if(CB3Slide3.getSelectionModel().getSelectedItem().toString().equals("")){
                 CB3Slide3.setStyle(style);
             ret=false;}
             else
                 CB3Slide3.setStyle(styledefault);
         }
         else
             if(CB3Slide1.getSelectionModel().getSelectedItem().toString().equals("")){
                 CB3Slide1.setStyle(style);
             ret=false;}
             else
                 CB3Slide1.setStyle(styledefault);
         return ret;
         
     }
    
}
