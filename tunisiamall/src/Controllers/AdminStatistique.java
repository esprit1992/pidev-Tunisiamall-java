/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoUtilisateur;
import animation.FadeInDowntransition;
import animation.FadeInUpTransition;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class AdminStatistique implements Initializable {
    @FXML
    private ListView<String> listMenuOfGestion;
    @FXML
    private Button btnBack;
    @FXML
    private Text textNameUser;
    @FXML
    private ImageView LBimguser;
    @FXML
    private ImageView imgLoad;
    @FXML
    private Text TextLocation;
    @FXML
    private AnchorPane paneadduser;
    @FXML
    private ComboBox<String> CBChoseStat;
    @FXML
    private AnchorPane UsersParAge;
    @FXML
    private AnchorPane userbycivilite;
    @FXML
    private BarChart<String, Integer> barchart;
    @FXML
    private Label lblClose;
    @FXML
    private ProgressBar bar;
    @FXML
    private PieChart pieChart;
    private int choix=1;
    private DaoUtilisateur daouser=new DaoUtilisateur();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        textNameUser.setText(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getNom() + " " + tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getPrenom());
        TextLocation.setText("statistics");
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        });
        CBChoseStat.getItems().addAll("Users By Civilite", "Users By Age");
        CBChoseStat.getSelectionModel().select("Users By Age");
        CBChoseStat.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
         
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                 if (newValue != null) {
                if(newValue.equals("Users By Civilite"))
                showUserCivility();
                else
                showUserAge();
            }
            }
  
    });
        
        
       
        

         Platform.runLater(new Runnable() {
                                @Override
            public void run() {
        barchart.getData().add(daouser.StatAdminUserAge());
        barchart.setLegendVisible(false);
        barchart.setAnimated(true);
        
        
        
                                }
                            });
    }    

    @FXML
    private void ButtonBack(ActionEvent event) throws IOException {
         tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/menuAdmin.fxml"))));
    }
    
    public void showUserCivility()
    {
     
            new FadeInUpTransition(UsersParAge).play();
            new FadeInDowntransition(userbycivilite).play();
            choix=1;
            pieChart.getData().clear();
            pieChart.setData(daouser.getUserChartByCivilite());
        pieChart.setAnimated(true);
        pieChart.setVisible(true);
        
    }
    public void showUserAge()
    {
     
            new FadeInUpTransition(userbycivilite).play();
            new FadeInDowntransition(UsersParAge).play();
            choix=0;
            
            barchart.getData().clear();
            barchart.getData().add(daouser.StatAdminUserAge());
        barchart.setLegendVisible(false);
        barchart.setAnimated(true);
        
    }
}
