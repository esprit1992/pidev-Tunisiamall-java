/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DaoUtilisateur;
import ENTITY.Utilisateur;
import animation.*;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 *
 * @author user
 */

public class registerController implements Initializable {

    @FXML
    private TextField TFusernamecr;
    @FXML
    private TextField TFpasswordcr;
    @FXML
    private TextField TFfirstnamecr;
    @FXML
    private TextField TFlastnameCR;
    @FXML
    private ComboBox CBsexcr;
    @FXML
    private ComboBox CBcivilitecr;
    @FXML
    private TextField TFagecr;
    @FXML
    private TextField TFtelnumbercr;
    @FXML
    private TextField TFemailcr;
    @FXML
    private ComboBox CBrolecr;
    @FXML
    private TextArea TFadressecr;

    @FXML
    private Label LBusernamecr;
    @FXML
    private Label LBpasswordcr;
    @FXML
    private Label LBfirstnamecr;
    @FXML
    private Label LBlastnameCR;
    @FXML
    private Label LBsexcr;
    @FXML
    private Label LBcivilitecr;
    @FXML
    private Label LBagecr;
    @FXML
    private Label LBtelnumbercr;
    @FXML
    private Label LBemailcr;
    @FXML
    private Label LBrolecr;
    @FXML
    private Label LBadressecr;
    @FXML
    private Button ButtonValiderRegister;
    
    @FXML
    private Label lblClose;
    @FXML
    private AnchorPane paneadduser;
    @FXML
    private ProgressBar bar;
    @FXML
    private ImageView imgLoad;
    
    public boolean verifUserChamps() {
        int verif = 0;
        String style = " -fx-border-color: red;";

        String styledefault = "-fx-border-color: green;";

        TFusernamecr.setStyle(styledefault);
        TFemailcr.setStyle(styledefault);
        TFpasswordcr.setStyle(styledefault);
        CBrolecr.setStyle(styledefault);
        TFadressecr.setStyle(styledefault);
        TFlastnameCR.setStyle(styledefault);
        TFfirstnamecr.setStyle(styledefault);
        CBsexcr.setStyle(styledefault);
        TFagecr.setStyle(styledefault);
        TFtelnumbercr.setStyle(styledefault);
        CBcivilitecr.setStyle(styledefault);
        
        if (TFusernamecr.getText().equals("")) {
            TFusernamecr.setStyle(style);
            verif = 1;
        }

        if (TFemailcr.getText().equals("")) {
            TFemailcr.setStyle(style);
            verif = 1;
        }

        if (TFpasswordcr.getText().equals("")) {
            TFpasswordcr.setStyle(style);
            verif = 1;
        }

        try {
            CBrolecr.getSelectionModel().getSelectedItem().toString().equals("");
        } catch (Exception e) {
            {
                CBrolecr.setStyle(style);
                verif = 1;
            }
        }

        if (TFadressecr.getText().equals("")) {
            TFadressecr.setStyle(style);
            verif = 1;
        }

        if (TFlastnameCR.getText().equals("")) {
            TFlastnameCR.setStyle(style);
            verif = 1;
        }

        if (TFfirstnamecr.getText().equals("")) {
            TFfirstnamecr.setStyle(style);
            verif = 1;
        }

        try {
            CBsexcr.getSelectionModel().getSelectedItem().toString().equals("");
        } catch (Exception e) {
            {
                CBsexcr.setStyle(style);
                verif = 1;
            }
        }

        try {
            Integer.parseInt(TFagecr.getText().toString());
            if (Integer.parseInt(TFagecr.getText().toString()) > 100 || Integer.parseInt(TFagecr.getText().toString()) < 10) {
                TFagecr.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFagecr.setStyle(style);
                verif = 1;
            }
        }

        try {
            Integer.parseInt(TFtelnumbercr.getText().toString());
            if (Integer.parseInt(TFtelnumbercr.getText().toString()) > 99999999 || Integer.parseInt(TFtelnumbercr.getText().toString()) < 20000000) {
                TFtelnumbercr.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFtelnumbercr.setStyle(style);
                verif = 1;
            }
        }

        try {
            CBcivilitecr.getSelectionModel().getSelectedItem().toString().equals("");
        } catch (Exception e) {
            {
                CBcivilitecr.setStyle(style);
                verif = 1;
            }
        }

        System.out.println("verif=" + verif);
        if (verif == 0) {
            return true;
        }
        TrayNotification tray = new TrayNotification("Error", "Verif Your Input Please", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
        return false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lblClose.setOnMouseClicked((MouseEvent event) -> {
            Platform.exit();
            System.exit(0);
        });
        
        TFusernamecr.setText("");
        TFpasswordcr.setText("");
        TFfirstnamecr.setText("");
        CBcivilitecr.getItems().addAll("Maried", "Single", "devorced");
        CBsexcr.getItems().addAll("Mmale", "female");
        CBrolecr.getItems().addAll("user", "responsable");
//all this annimation dont care about it ;)
        new FadeInLeftTransition1(TFusernamecr).play();
        new FadeInLeftTransition1(TFpasswordcr).play();
        new FadeInLeftTransition1(TFfirstnamecr).play();
        new FadeInLeftTransition1(TFlastnameCR).play();
        new FadeInLeftTransition1(CBsexcr).play();
        new FadeInLeftTransition1(CBcivilitecr).play();
        new FadeInLeftTransition1(TFagecr).play();
        new FadeInLeftTransition1(TFemailcr).play();
        new FadeInLeftTransition1(CBrolecr).play();
        new FadeInLeftTransition1(TFadressecr).play();
        new FadeInLeftTransition1(TFtelnumbercr).play();

        new FadeInLeftTransition1(LBusernamecr).play();
        new FadeInLeftTransition1(LBpasswordcr).play();
        new FadeInLeftTransition1(LBfirstnamecr).play();
        new FadeInLeftTransition1(LBlastnameCR).play();
        new FadeInLeftTransition1(LBsexcr).play();
        new FadeInLeftTransition1(LBcivilitecr).play();
        new FadeInLeftTransition1(LBagecr).play();
        new FadeInLeftTransition1(LBemailcr).play();
        new FadeInLeftTransition1(LBrolecr).play();
        new FadeInLeftTransition1(LBadressecr).play();
        new FadeInLeftTransition1(LBtelnumbercr).play();
 
new FadeInLeftTransition(ButtonValiderRegister).play();
         
        
        
        
        
        
        
    }

    public void ButtonBackRegisterToLogin() throws IOException {
        tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/login_1.fxml"))));
    }

    public void ButtonCreateNewAccountAction() throws IOException {
        int msgNumber;
        
        if (verifUserChamps()) {
            Utilisateur user;
            user = new Utilisateur();
            try {
                user = new Utilisateur(TFusernamecr.getText(), TFemailcr.getText(), TFpasswordcr.getText(), TFadressecr.getText(), TFfirstnamecr.getText(), TFlastnameCR.getText(), CBsexcr.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(TFagecr.getText()), Integer.parseInt(TFtelnumbercr.getText()), CBcivilitecr.getSelectionModel().getSelectedItem().toString(), CBrolecr.getSelectionModel().getSelectedItem().toString(),0.0);
            } catch (Exception e) {
                final TrayNotification tray = new TrayNotification("Error", "Error In creation of new user", Notifications.ERROR);
                tray.showAndDismiss(Duration.seconds(3));
            }
            
            Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = 30;
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
            
          
                
 msgNumber = DaoUtilisateur.getInstance().adduser(user);

        service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        service.setOnSucceeded((WorkerStateEvent event) -> {
             bar.progressProperty().unbind();
             bar.setProgress(0);
            imgLoad.setVisible(false);
             
                 if (msgNumber == 0) {
                final TrayNotification tray = new TrayNotification("congratulation", "Your Account has been created", Notifications.SUCCESS);
                tray.showAndDismiss(Duration.seconds(3));
                try {
                    tunisiamallMain.Tunisiamall.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/GUI/login_1.fxml"))));
                } catch (IOException ex) {
                    Logger.getLogger(registerController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                if (msgNumber == 1) {
                    final TrayNotification tray = new TrayNotification("Error", "This User Is alredy Existe", Notifications.ERROR);
                    tray.showAndDismiss(Duration.seconds(3));
                } else {
                    if (msgNumber == 2) {
                        final TrayNotification tray = new TrayNotification("Error", "This Email Is alredy Existe", Notifications.ERROR);
                        tray.showAndDismiss(Duration.seconds(3));
                    } else {
                        if (msgNumber == 3) {
                            final TrayNotification tray = new TrayNotification("Error", "This Number Phone Is alredy Existe", Notifications.ERROR);
                            tray.showAndDismiss(Duration.seconds(3));
                        } else {
                            if (msgNumber == 4) {
                                final TrayNotification tray = new TrayNotification("Error", "Error With Data Base", Notifications.ERROR);
                                tray.showAndDismiss(Duration.seconds(3));
                            }
                        }
                    }
                }
            }
            
    
            
        });
            
        }   }
            
           
        

}
