/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;


public class Pack {
    private int id;
    private String description;
    private double prixjour;
    private int disponible;
    private int type;
    
    
    
    public Pack()
    {
        
    }

    public Pack( String description, double prixjour, int disponible) {
        //this.id = id;
        this.description = description;
        this.prixjour = prixjour;
        this.disponible = disponible;
    }
    
    public Pack(int id, String description, double prixjour, int disponible,int type) {
        this.id = id;
        this.description = description;
        this.prixjour = prixjour;
        this.disponible = disponible;
        this.type=type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrixjour() {
        return prixjour;
    }

    public void setPrixjour(double prixjour) {
        this.prixjour = prixjour;
    }

    public int getDisponible() {
        return disponible;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    
    
    
    
    
    
}
