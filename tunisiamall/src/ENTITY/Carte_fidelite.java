/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.awt.Image;
import java.sql.Blob;

/**
 *
 * @author siwar-pc
 */
public class Carte_fidelite {
    
    
    public int id;
    public Utilisateur u;
    public Boutique b;
    public int nombre_points;
    public Blob prix_achat_totale;

    public Carte_fidelite() {
    }

    public Carte_fidelite( Utilisateur u, Boutique b, int nombre_points, Blob prix_achat_totale) {
        
        this.u = u;
        this.b = b;
        this.nombre_points = nombre_points;
        this.prix_achat_totale = prix_achat_totale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Utilisateur getU() {
        return u;
    }

    public void setU(Utilisateur u) {
        this.u = u;
    }

    public Boutique getB() {
        return b;
    }

    public void setB(Boutique b) {
        this.b = b;
    }

    public int getNombre_points() {
        return nombre_points;
    }

    public void setNombre_points(int nombre_points) {
        this.nombre_points = nombre_points;
    }

    public Blob getPrix_achat_totale() {
        return prix_achat_totale;
    }

    public void setPrix_achat_totale(Blob prix_achat_totale) {
        this.prix_achat_totale = prix_achat_totale;
    }
    
    
    
    
    
}
