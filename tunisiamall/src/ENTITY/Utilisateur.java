/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.sql.Date;
import java.util.Objects;
import UTILS.*;

public class Utilisateur {

    private int id;
    private String username;
    private String username_canonical;
    private String email;
    private String email_canonical;
    private int enabled;
    private String salt;
    private String password;
    private Date last_login;
    private Boolean locked;
    private Boolean expired;
    private Date expires_at;
    private String confirmation_token;
    private Date password_requested_at;
    private String roles;
    private Boolean credentials_expired;
    private Date credentials_expire_at;
    private String adresse;
    private String nom;
    private String prenom;
    private String sex;
    private int age;
    private int tel;
    private String civilite;
    private Double solde=0.0;
    public Utilisateur(String username, String email, String roles, String adresse, String nom, String prenom, int age, int tel, String civilite,Double soulde) {
        this.username = username;
        this.email = email;
        if (roles.equals("a:0:{}")) {
            this.roles = "user";
        } else 
            if (roles.equals("a:1:{i:0;s:16:\"ROLE_RESPONSABLE\";}")) {
                this.roles = "responsable";
            } else {
                this.roles = "admin";
            }
        
        this.adresse = adresse;
        this.nom = nom;
        this.prenom = prenom;
        this.sex = sex;
        this.age = age;
        this.tel = tel;
        this.civilite = civilite;
        this.solde=solde;
    }
    
     public Utilisateur(int id,String username, String email, String roles, String adresse, String nom, String prenom, int age, int tel, String civilite,Double soulde,String sex,String password) {
        this.username = username;
        this.email = email;

        if (roles.equals("a:0:{}")) {
            this.roles = "user";
        } else {
            if (roles.equals("a:1:{i:0;s:16:\"ROLE_RESPONSABLE\";}")) {
                this.roles = "responsable";
            } else {
                this.roles = "admin";
            }
        }
        this.sex=sex;
        this.password=password;
        this.adresse = adresse;
        this.nom = nom;
        this.prenom = prenom;
        this.sex = sex;
        this.age = age;
        this.tel = tel;
        this.civilite = civilite;
        this.id=id;
        this.solde=soulde;
    }

    public Utilisateur(String username, String email, String password, String adresse, String nom, String prenom, String sex, int age, int tel, String civilite, String role,Double soulde) {
        int enabled = 1;
        if (role.equals("user")) {
            role = "a:0:{}";
        } else {
            enabled = 0;
            role = "a:1:{i:0;s:16:\"ROLE_RESPONSABLE\";}";
        }
        String slat = BCrypt.gensalt(13);;

        this.username = username;
        this.username_canonical = username;
        this.email = email;
        this.email_canonical = email;
        this.enabled = enabled;
        this.salt = slat;
        this.password = BCrypt.hashpw(password, salt);
        this.locked = false;
        this.expired = false;
        this.roles = role;
        this.credentials_expired = false;
        this.adresse = adresse;
        this.nom = nom;
        this.prenom = prenom;
        this.sex = sex;
        this.age = age;
        this.tel = tel;
        this.civilite = civilite;
        this.solde=soulde;
    }

    public Utilisateur() {

    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getUsername_canonical() {
        return username_canonical;
    }

    public String getEmail() {
        return email;
    }

    public String getEmail_canonical() {
        return email_canonical;
    }

    public int getEnabled() {
        return enabled;
    }

    public String getSalt() {
        return salt;
    }

    public String getPassword() {
        return password;
    }

    public Date getLast_login() {
        return last_login;
    }

    public Boolean getLocked() {
        return locked;
    }

    public Boolean getExpired() {
        return expired;
    }

    public Date getExpires_at() {
        return expires_at;
    }

    public String getConfirmation_token() {
        return confirmation_token;
    }

    public Date getPassword_requested_at() {
        return password_requested_at;
    }

    public String getRoles() {
        return roles;
    }

    public Boolean getCredentials_expired() {
        return credentials_expired;
    }

    public Date getCredentials_expire_at() {
        return credentials_expire_at;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public int getTel() {
        return tel;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUsername_canonical(String username_canonical) {
        this.username_canonical = username_canonical;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmail_canonical(String email_canonical) {
        this.email_canonical = email_canonical;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public void setExpires_at(Date expires_at) {
        this.expires_at = expires_at;
    }

    public void setConfirmation_token(String confirmation_token) {
        this.confirmation_token = confirmation_token;
    }

    public void setPassword_requested_at(Date password_requested_at) {
        this.password_requested_at = password_requested_at;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public void setCredentials_expired(Boolean credentials_expired) {
        this.credentials_expired = credentials_expired;
    }

    public void setCredentials_expire_at(Date credentials_expire_at) {
        this.credentials_expire_at = credentials_expire_at;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        hash = 89 * hash + Objects.hashCode(this.username);
        hash = 89 * hash + Objects.hashCode(this.username_canonical);
        hash = 89 * hash + Objects.hashCode(this.email);
        hash = 89 * hash + Objects.hashCode(this.email_canonical);
        hash = 89 * hash + this.enabled;
        hash = 89 * hash + Objects.hashCode(this.salt);
        hash = 89 * hash + Objects.hashCode(this.password);
        hash = 89 * hash + Objects.hashCode(this.last_login);
        hash = 89 * hash + Objects.hashCode(this.locked);
        hash = 89 * hash + Objects.hashCode(this.expired);
        hash = 89 * hash + Objects.hashCode(this.expires_at);
        hash = 89 * hash + Objects.hashCode(this.confirmation_token);
        hash = 89 * hash + Objects.hashCode(this.password_requested_at);
        hash = 89 * hash + Objects.hashCode(this.roles);
        hash = 89 * hash + Objects.hashCode(this.credentials_expired);
        hash = 89 * hash + Objects.hashCode(this.credentials_expire_at);
        hash = 89 * hash + Objects.hashCode(this.adresse);
        hash = 89 * hash + Objects.hashCode(this.nom);
        hash = 89 * hash + Objects.hashCode(this.prenom);
        hash = 89 * hash + Objects.hashCode(this.sex);
        hash = 89 * hash + this.age;
        hash = 89 * hash + this.tel;
        hash = 89 * hash + Objects.hashCode(this.civilite);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Utilisateur other = (Utilisateur) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.enabled != other.enabled) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        if (this.tel != other.tel) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.username_canonical, other.username_canonical)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.email_canonical, other.email_canonical)) {
            return false;
        }
        if (!Objects.equals(this.salt, other.salt)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.confirmation_token, other.confirmation_token)) {
            return false;
        }
        if (!Objects.equals(this.roles, other.roles)) {
            return false;
        }
        if (!Objects.equals(this.adresse, other.adresse)) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.sex, other.sex)) {
            return false;
        }
        if (!Objects.equals(this.civilite, other.civilite)) {
            return false;
        }
        if (!Objects.equals(this.last_login, other.last_login)) {
            return false;
        }
        if (!Objects.equals(this.locked, other.locked)) {
            return false;
        }
        if (!Objects.equals(this.expired, other.expired)) {
            return false;
        }
        if (!Objects.equals(this.expires_at, other.expires_at)) {
            return false;
        }
        if (!Objects.equals(this.password_requested_at, other.password_requested_at)) {
            return false;
        }
        if (!Objects.equals(this.credentials_expired, other.credentials_expired)) {
            return false;
        }
        if (!Objects.equals(this.credentials_expire_at, other.credentials_expire_at)) {
            return false;
        }
        return true;
    }

}
