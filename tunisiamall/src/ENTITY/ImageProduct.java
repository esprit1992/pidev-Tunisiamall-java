/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

/**
 *
 * @author user
 */
public class ImageProduct {
    
    private int id;
    private int idProduct;
    private String chemain;
    private int active;

    public ImageProduct() {
    }

    public ImageProduct(int idProduct, String chemain, int active) {
        this.idProduct = idProduct;
        this.chemain = chemain;
        this.active = active;
    }

    public ImageProduct(int id, int idProduct, String chemain, int active) {
        this.id = id;
        this.idProduct = idProduct;
        this.chemain = chemain;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getChemain() {
        return chemain;
    }

    public void setChemain(String chemain) {
        this.chemain = chemain;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
    
}
