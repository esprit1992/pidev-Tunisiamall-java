/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

/**
 *
 * @author user
 */
public class ProduitDePanier {
    private int id;
    private String ProductName;
    private int Quantiter;
    private Double PrixUnite;
    private Double PrixTotaleUniter;

    public ProduitDePanier(int id,String ProductName, int Quantiter, Double PrixUnite, Double PrixTotaleUniter) {
        this.id=id;
        this.ProductName = ProductName;
        this.Quantiter = Quantiter;
        this.PrixUnite = PrixUnite;
        this.PrixTotaleUniter = PrixTotaleUniter;
    }

    public ProduitDePanier() {
        
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public int getQuantiter() {
        return Quantiter;
    }

    public void setQuantiter(int Quantiter) {
        this.Quantiter = Quantiter;
    }

    public Double getPrixUnite() {
        return PrixUnite;
    }

    public void setPrixUnite(Double PrixUnite) {
        this.PrixUnite = PrixUnite;
    }

    public Double getPrixTotaleUniter() {
        return PrixTotaleUniter;
    }

    public void setPrixTotaleUniter(Double PrixTotaleUniter) {
        this.PrixTotaleUniter = PrixTotaleUniter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
