/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.util.Objects;

/**
 *
 * @author mirak
 */
public class Reclamation {
    private int  id;
    
    private int  id_user;
    
    private String sujet;
    
    private String  categorie;
    
    private boolean traiter ;
public Reclamation()
{
    
}
    public Reclamation(int id, int id_user, String sujet, String categorie, boolean traiter) {
        this.id = id;
        this.id_user = id_user;
        this.sujet = sujet;
        this.categorie = categorie;
        this.traiter = traiter;
    }

    public Reclamation(int id_user, String sujet, String categorie, boolean traiter) {
        this.id_user = id_user;
        this.sujet = sujet;
        this.categorie = categorie;
        this.traiter = traiter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public boolean isTraiter() {
        return traiter;
    }

    public void setTraiter(boolean traiter) {
        this.traiter = traiter;
    }

    @Override
    public String toString() {
        return "Reclamation{" + "id=" + id + ", id_user=" + id_user + ", sujet=" + sujet + ", categorie=" + categorie + ", traiter=" + traiter + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.id;
        hash = 59 * hash + this.id_user;
        hash = 59 * hash + Objects.hashCode(this.sujet);
        hash = 59 * hash + Objects.hashCode(this.categorie);
        hash = 59 * hash + (this.traiter ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reclamation other = (Reclamation) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.id_user != other.id_user) {
            return false;
        }
        if (!Objects.equals(this.sujet, other.sujet)) {
            return false;
        }
        if (!Objects.equals(this.categorie, other.categorie)) {
            return false;
        }
        if (this.traiter != other.traiter) {
            return false;
        }
        return true;
    }
    
    
}
