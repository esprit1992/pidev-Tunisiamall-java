/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.sql.Date;


public class Reservation {
    private int id;
    private int id_pack;
    private int id_responsable;
    private String id_produit;
    private Date 	datefin;
    
    
    
    
    public Reservation()
    {
        
    }

    public Reservation(int id, int id_pack, int id_responsable, String id_produit, Date datefin) {
        this.id = id;
        this.id_pack = id_pack;
        this.id_responsable = id_responsable;
        this.id_produit = id_produit;
        this.datefin = datefin;
    }

    public int getId_pack() {
        return id_pack;
    }

    public int getId_responsable() {
        return id_responsable;
    }

    public Date getDatefin() {
        return datefin;
    }


    
    
    
    
}
