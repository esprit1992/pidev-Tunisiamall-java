/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

import java.sql.Date;

/**
 *
 * @author siwar-pc
 */
public class Commentaire {
 
    
    
    
    private int id_cmt;
    private String id_Thred;
    private String body;
    private Date DateCreation;
    private int State;
    private int user_id;
    private String permlink;
    private int numerocomment;

    public Commentaire(int id_cmt, String id_Thred, String body, Date DateCreation, int State, int user_id, String permlink, int numerocomment) {
        this.id_cmt = id_cmt;
        this.id_Thred = id_Thred;
        this.body = body;
        this.DateCreation = DateCreation;
        this.State = State;
        this.user_id = user_id;
        this.permlink = permlink;
        this.numerocomment = numerocomment;
    }
      
    
     public Commentaire(String id_Thred, String body, Date DateCreation, int State, int user_id, String permlink, int numerocomment) {
      
        this.id_Thred = id_Thred;
        this.body = body;
        this.DateCreation = DateCreation;
        this.State = State;
        this.user_id = user_id;
        this.permlink = permlink;
        this.numerocomment = numerocomment;
    }

    public int getId_cmt() {
        return id_cmt;
    }

    public void setId_cmt(int id_cmt) {
        this.id_cmt = id_cmt;
    }

    public String getId_Thred() {
        return id_Thred;
    }

    public void setId_Thred(String id_Thred) {
        this.id_Thred = id_Thred;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDateCreation() {
        return DateCreation;
    }

    public void setDateCreation(Date DateCreation) {
        this.DateCreation = DateCreation;
    }

    public int getState() {
        return State;
    }

    public void setState(int State) {
        this.State = State;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPermlink() {
        return permlink;
    }

    public void setPermlink(String permlink) {
        this.permlink = permlink;
    }

    public int getNumerocomment() {
        return numerocomment;
    }

    public void setNumerocomment(int numerocomment) {
        this.numerocomment = numerocomment;
    }
    
    
}
