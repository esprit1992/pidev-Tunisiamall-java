/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

/**
 *
 * @author siwar-pc
 */
public class Produit {
    private int id;
    private Boutique b ;
    private int quantite;
    private String description;
    private double prix;
    private int newcollection;
    private String libelle;
    private String codeabar;

    public Produit( Boutique b, int quantite, String description, double prix, int newcollection, String libelle,String codeabar) {
      
        this.b = b;
        this.quantite = quantite;
        this.description = description;
        this.prix = prix;
        this.newcollection = newcollection;
        this.libelle = libelle;
        this.codeabar=codeabar;
    }
       public Produit( Boutique b, int quantite, String description, double prix, int newcollection, String libelle,String codeabar,int id) {
      
        this.b = b;
        this.quantite = quantite;
        this.description = description;
        this.prix = prix;
        this.newcollection = newcollection;
        this.libelle = libelle;
        this.codeabar=codeabar;
        this.id=id;
    }
    
    public Produit()
    {
        
    }

    public Produit(int id, Boutique b, int quantite, String libelle,int newcollection) {
        this.id = id;
        this.b = b;
        this.quantite = quantite;
        this.libelle = libelle;
        this.newcollection=newcollection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boutique getB() {
        return b;
    }

    public void setB(Boutique b) {
        this.b = b;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public int getNewcollection() {
        return newcollection;
    }

    public void setNewcollection(int newcollection) {
        this.newcollection = newcollection;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCodeabar() {
        return codeabar;
    }

    public void setCodeabar(String codeabar) {
        this.codeabar = codeabar;
    }
    
    
    
    
}
