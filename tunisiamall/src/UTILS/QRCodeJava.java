package UTILS;

import Dao.dCarte;
import ENTITY.Boutique;
import ENTITY.Utilisateur;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
 
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

public class QRCodeJava {
    public void generateqr(Utilisateur u, Boutique  boutique) {
        ByteArrayOutputStream out = QRCode.from(""+u.getId() +"/"+ u.getUsername() +"/"+ boutique.getNom())
                                        .to(ImageType.PNG).stream();
 
        try {
            FileOutputStream fout = new FileOutputStream(new File(
                    "D:\\QR_Codde.PNG"));
 
            fout.write(out.toByteArray());
 
            fout.flush();
            fout.close();
            
            dCarte j= new dCarte();
            j.addcarte(u,"D:\\QR_Codde.PNG",boutique);
        } catch (FileNotFoundException e) {
            // Do Logging
        	e.printStackTrace();
        } catch (IOException e) {
            // Do Logging
        	e.printStackTrace();
        }
    }
}
