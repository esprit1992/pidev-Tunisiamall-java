package UTILS;

/**
 *********************************************************************
 * Module: Connexion.java Author: Belguith Purpose: Defines the Class Connexion
 * ********************************************************************
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class DataSource {

    private String url = "jdbc:mysql://localhost:3306/tunisiamall";
    private String login = "root";
    private String password = "";
    private Connection connection;
    private static DataSource instance;

    private DataSource() {
        PreparedStatement pst = null;
        try {
            connection = DriverManager.getConnection(url, login, password);
             try {
          
            pst = connection.prepareStatement("CREATE  EVENT IF NOT EXISTS `TunisiaMallTriggerReservationPack` ON SCHEDULE EVERY 1 MINUTE STARTS '2016-01-01 00:04:42' ON COMPLETION NOT PRESERVE ENABLE DO update pack_pub p,reservation r set p.disponible=1 where p.id=r.id_pack and now()>r.datefin");       
             pst.executeUpdate();
           
        } catch (SQLException e) {
             System.out.println(e+"Create Trigger For Reservation");
        }
            
        } catch (SQLException ex) {
            System.out.println("data base not mal9ahech :/");
        }
    }
public final boolean testexistdatabase()
{
    try {
            connection = DriverManager.getConnection(url, login, password);
        } catch (SQLException ex) {
            return false;
        }
    return true;
}
    public Connection getConnection() {
        return connection;
    }

    public static DataSource getInstance() {
        if (instance == null) {
            instance = new DataSource();
        }
        return instance;
    }

}
