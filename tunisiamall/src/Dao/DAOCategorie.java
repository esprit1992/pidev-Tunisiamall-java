/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import UTILS.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author user
 */
public class DAOCategorie {
    private DataSource connection;

    public DAOCategorie() {
        this.connection = DataSource.getInstance();
    }

    
   public ArrayList<String> getAllCategorie()
    {
     ArrayList<String> strings= new ArrayList<String>();
        PreparedStatement pst = null;
        try {
            pst = connection.getConnection().prepareStatement("SELECT `label` FROM `categorie`");
            ResultSet res;
            res = pst.executeQuery();
            while(res.next())
            strings.add(res.getString("label"));
        } catch (SQLException e) {
             System.out.println(e);
        }
        return strings;
    }
   
   public void addnewcategorie(String newcat)
   {
       PreparedStatement pst = null;
        try {
            pst = connection.getConnection().prepareStatement("insert into `categorie` (`label`) values (?)");
            pst.setString(1, newcat);
            pst.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
   
   
   public int getidCategorie(String label)
   {
       PreparedStatement pst = null;
       ResultSet res = null;
       int ret=0;
        try {
            pst = connection.getConnection().prepareStatement("SELECT `id` FROM `categorie` WHERE `label`=?");
            pst.setString(1, label);
            
            res = pst.executeQuery();
             res.next();
             ret=res.getInt("id");
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
   }
   
   
   public String getlabelCategorie(int id)
   {
       PreparedStatement pst = null;
       ResultSet res = null;
       String ret="";
        try {
            pst = connection.getConnection().prepareStatement("SELECT `label` FROM `categorie` WHERE `id`=?");
            pst.setInt(1, id);
            
            res = pst.executeQuery();
             while(res.next())
             ret=res.getString("label");
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategorie.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("daocat");
        }
        System.out.println("labelCategorie est"+ret);
        return ret;
   }
   
}



