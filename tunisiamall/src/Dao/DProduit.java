package Dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import ENTITY.Boutique;
import ENTITY.Produit;
import ENTITY.ProduitDePanier;
import ENTITY.Utilisateur;
import INTERFACES.IProduit;
import UTILS.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author kharrat
 */
public class DProduit implements IProduit{
private DataSource connection;
private static DProduit instance;
    public static DProduit getInstance() {
        if (instance == null) {
            instance = new DProduit();
        }
        return instance;
    }
    
    
    public DProduit(){
        connection= DataSource.getInstance();
    }
    
    public Produit getProduit(int id)
    {
        PreparedStatement pst=null;
        ResultSet res=null;
    try {
        pst = connection.getConnection().prepareStatement("select p.*,b.nom from `produit` p,`boutique` b where p.id=? and b.id=p.id_boutique");
        pst.setInt(1, id);
        res = pst.executeQuery();
        res.next();
        return new Produit(new Boutique(res.getString("nom")),res.getInt("quantite"),res.getString("description"),res.getDouble("prix"),res.getInt("newcollection"),res.getString("libelle"),res.getString("barcode"),res.getInt("id"));
    } catch (SQLException ex) {
        System.out.println("Dao.DProduit.getProduit()");
        Logger.getLogger(DProduit.class.getName()).log(Level.SEVERE, null, ex);
    }
        
            return null;
    }
    
        public int getprodsboutique(int id) {
        int nbr=0;
    
        try{
        PreparedStatement pst=connection.getConnection().prepareStatement("select count(*) from produit where id_boutique = ? ");
            pst.setInt(1, id);
            ResultSet result = pst.executeQuery();
 
while (result.next()){
    nbr= result.getInt(1);
    }

        }
       catch( Exception e)
 {
     
 }
        System.out.println(nbr);
        return nbr;
}
            public  ArrayList<Produit> getallprodsboutique(int id) {
        
        ArrayList<Produit> listeProduit= new ArrayList();
         
    try{
        PreparedStatement pst=connection.getConnection().prepareStatement("select * from produit where id_boutique = ? ");
            pst.setInt(1, id);
            ResultSet result = pst.executeQuery();
 
while (result.next()){
    Produit prod=new Produit();
    prod.setId(result.getInt(1)); 
    Boutique b=new Boutique();
    b.setId(result.getInt(2));

 
  
   prod.setB(b); 
   prod.setQuantite(result.getInt(3)); 
   prod.setDescription(result.getString(4));
   prod.setPrix(result.getInt(5));
   prod.setNewcollection(result.getInt(6));
   prod.setLibelle(result.getString(7));
   
   listeProduit.add(prod);
    
    }

 
   
    
        
    
 }catch( Exception e)
 {
     
 }
 return listeProduit;    
   
    }
                public Produit getprodbyid(int id) {
        Produit p= new Produit();
        try{
        PreparedStatement pst=connection.getConnection().prepareStatement("select * from produit where id = ? ");
            pst.setInt(1, id);
            ResultSet result = pst.executeQuery();
 DBoutique dboutique=new DBoutique();
while (result.next()){
    p.setId(result.getInt(1));
    Boutique b = new Boutique();
 
    b=(dboutique.getboutique(result.getInt(2)));
    p.setId(result.getInt(1));
    p.setB(b);
    p.setQuantite(result.getInt(3));
    p.setDescription(result.getString(4));
    p.setPrix(result.getDouble(5));
     p.setNewcollection(result.getInt(6));
         p.setLibelle(result.getString(7));


    }

        }
       catch( Exception e)
 {
     
 }
        
        return p;
    }
    
@Override
    public int addProd(Produit p) {
        int ret=0;
       PreparedStatement pst=null;
    //0:produit add succ   1 bar code alredy exist  2name alredy exist  3erreur request 4erreur req id

    
     try {
            
            pst = connection.getConnection().prepareStatement("select * from `produit` where `barcode`=?");
            pst.setString(1, p.getCodeabar());
            if (pst.executeQuery().next()) 
                return  1; //barcode existe
          
            pst = connection.getConnection().prepareStatement("select * from `produit` where `libelle`=?");
            pst.setString(1, p.getLibelle());
            if (pst.executeQuery().next()) 
                return  2; //barcode existe
         
         
         
         pst=connection.getConnection().prepareStatement("insert into produit(id_boutique,quantite,description,prix,newcollection,libelle,barcode) values(?,?,?,?,?,?,?) ");
            pst.setInt(1, p.getB().getId());
            pst.setInt(2, p.getQuantite());
            pst.setString(3, p.getDescription());
            pst.setDouble(4, p.getPrix());
            pst.setInt(5, p.getNewcollection());
            pst.setString(6, p.getLibelle());
            pst.setString(7, p.getCodeabar());
            pst.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace();  
           return 3;
        }
      try {
     pst=connection.getConnection().prepareStatement("SELECT `id` FROM `produit` where (id_boutique=? and barcode=?)");
            pst.setInt(1, p.getB().getId());
            pst.setString(2, p.getCodeabar());
            ResultSet res;
            res = pst.executeQuery();
            if (res.last()) 
                p.setId(res.getInt("id"));
       } catch (SQLException ex) {
           ex.printStackTrace();  
           return 4;
        }
        System.out.println("Dao.DProduit.addProd() "+p.getId());
    return ret;
    }

  

    public int updateprod (Produit p){
        PreparedStatement pst=null;
        // 1:barcode 2:libelle 3:erreur 4:ok
        
        try {
          
            pst = connection.getConnection().prepareStatement("select * from `produit` where (`barcode`=? and id!=?)");
            pst.setString(1, p.getCodeabar());
            pst.setInt(2, p.getId());
            if (pst.executeQuery().next()) 
                return  1; //barcode existe
          
            pst = connection.getConnection().prepareStatement("select * from `produit` where (`libelle`=? and id!=?)");
            pst.setString(1, p.getLibelle());
            pst.setInt(2, p.getId());
            if (pst.executeQuery().next()) 
                return  2; //barcode existe
            
            
            pst=connection.getConnection().prepareStatement("update  produit set quantite= ? , prix= ? , description = ? ,newcollection= ?, id_boutique= ?, libelle= ?,barcode=? where id = ? ");
            pst.setInt(1, p.getQuantite());
            pst.setDouble(2, p.getPrix());
            pst.setInt(4, p.getNewcollection());
            pst.setString(3, p.getDescription());
            pst.setInt(5,p.getB().getId());
            pst.setString(6, p.getLibelle());
            pst.setString(7, p.getCodeabar());
            pst.setInt(8,p.getId());

            pst.executeUpdate();
        }
        catch (SQLException ex) {
            ex.printStackTrace(); 
            return 3;        
        }
        
    return 4;
    }

    public void getprod() {
       
    }

@Override
    public ArrayList<Produit> getprods() {
 ArrayList<Produit> listeProduit= new ArrayList();
         
    String sql = "SELECT id, id_boutique, quantite, description,  prix, newcollection, libelle FROM produit ";
 try{
Statement statement = connection.getConnection().createStatement();
ResultSet result = statement.executeQuery(sql);
 
while (result.next()){
    Produit kk=new Produit();
    kk.setId(result.getInt(1)); 
    Boutique b=new Boutique();
    b.setId(result.getInt(2));

 
  
   kk.setB(b); 
   kk.setQuantite(result.getInt(3)); 
   kk.setDescription(result.getString(4));
   kk.setPrix(result.getInt(5));
   kk.setNewcollection(result.getInt(6));
   kk.setLibelle(result.getString(7));
   
   listeProduit.add(kk);
    
    }

 
   
    
        
    
 }catch( Exception e)
 {
     
 }
 return listeProduit;    
    }

@Override
    public void getprodsboutique() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

@Override
    public void getprodscat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

@Override
    public void getprodsnom() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

@Override
    public void deleteprod(Produit p) {

        try{
            

  PreparedStatement pst=connection.getConnection().prepareStatement("delete from  produit  where id = ? ");
            pst.setInt(1, p.getId());
            pst.executeUpdate();
        }
        catch (SQLException ex) {
        }
    }
    
    
    
    
    
         public ArrayList<Produit> getlisteproduitofuser(int id,String filter) {
    
    ArrayList<Produit> listeProduit= new ArrayList<>();
    PreparedStatement pst = null;
    try {
        pst = connection.getConnection().prepareStatement("SELECT distinct(p.id),b.nom,p.quantite,p.libelle,p.newcollection FROM `produit` p,`boutique` b WHERE b.id_responsable=? and b.id=p.id_boutique and( b.nom like '%' ? '%' or p.id like '%' ? '%' or p.libelle like '%' ? '%' or p.quantite like '%' ? '%' )");
        pst.setInt(1, id);
        pst.setString(2, filter);
        pst.setString(3, filter);
        pst.setString(4, filter);
        pst.setString(5, filter);

            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){
   listeProduit.add(new Produit(res.getInt("id"),new Boutique(res.getString("nom")),res.getInt("quantite"),res.getString("libelle"),res.getInt("newcollection")));
}
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
 return listeProduit; }
    
         
         
             public ArrayList<String> getlisteProduitName(int id) {
    
    ArrayList<String> listeProduit= new ArrayList<>();
    PreparedStatement pst = null;
    try {
        pst = connection.getConnection().prepareStatement("SELECT p.id,b.nom,p.quantite,p.libelle,p.newcollection FROM `produit` p,`boutique` b WHERE b.id_responsable=? and b.id=p.id_boutique ORDER BY b.nom" );
        pst.setInt(1, id);
     

            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){
   listeProduit.add(res.getString("nom")+"-"+res.getString("libelle"));
}
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
 return listeProduit; }
         
         
         
         
         
         public int getnextidofproduct()
         {
              PreparedStatement pst = null;
              int ret=0;
    try {
        pst = connection.getConnection().prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE table_name = 'produit'");
         ResultSet res=pst.executeQuery();
         res.next();
         ret=res.getInt("id");
    } catch (SQLException ex) {
        Logger.getLogger(DProduit.class.getName()).log(Level.SEVERE, null, ex);
    }
            return ret;
         }
    
    
    public String getProduit(String nameBoutique,String nameProduit)
    {
        PreparedStatement pst=null;
        ResultSet res=null;
        String ret="";
    try {
        pst = connection.getConnection().prepareStatement("select p.id from `produit` p,`boutique` b where p.libelle=? and b.nom=? and b.id=p.id_boutique");
        pst.setString(1, nameProduit);
        pst.setString(2, nameBoutique);
        res = pst.executeQuery();
        res.next();
        ret=String.valueOf(res.getInt("id"));
    } catch (SQLException ex) {
        ret="";
    }
        
            return ret;
    }
    
    
      public ObservableList<ProduitDePanier> displayPanier(){
           ProduitDePanier Produit=null;
            PreparedStatement pst;
            ResultSet res;
            
           List<ProduitDePanier> vrec = new ArrayList<ProduitDePanier>();
           for (int mapKey : tunisiamallMain.Tunisiamall.getInstance().getPanier().keySet()) 
      {
      
        try {
            pst = connection.getConnection().prepareStatement("SELECT * FROM produit where id=?");
            pst.setInt(1, mapKey);
            res = pst.executeQuery();
            while(res.next()){
        
                Produit =new ProduitDePanier();
                Produit.setId(mapKey);
                Produit.setProductName(res.getString(7));
                Produit.setQuantiter(tunisiamallMain.Tunisiamall.getInstance().getPanier().get(mapKey));
                Produit.setPrixUnite(res.getDouble(5));
                Produit.setPrixTotaleUniter(res.getDouble(5)*tunisiamallMain.Tunisiamall.getInstance().getPanier().get(mapKey));
                vrec.add(Produit);
            }
   
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
            //fin boucle for
      }
         ObservableList<ProduitDePanier> observableList = FXCollections.observableList(vrec);
        return observableList;
    }
   
      
      public  Double getPrixArticleTotal(int id,int Quantite)
      {
          PreparedStatement pst;
            ResultSet res;
          Double somme=0.0;
    
          try {
            pst = connection.getConnection().prepareStatement("SELECT * FROM produit where id=?");
            pst.setInt(1, id);
            res = pst.executeQuery();
            while(res.next()){
               somme+=res.getDouble(5)*Quantite;
            }
   
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
      
        return somme;
    }
      
      public  int getIdResponsable(int idProduit)
      {
          PreparedStatement pst;
            ResultSet res;
          int id=0;
  
          try {
            pst = connection.getConnection().prepareStatement("select b.id_responsable from boutique b,produit p where p.id_boutique=b.id and p.id=?");
            pst.setInt(1, idProduit);
            res = pst.executeQuery();
            while(res.next()){
               id=res.getInt(1);
            }
   
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
      
        return id;
    }
      
      
      
      public  String SommeProduitPrice()
      {
          PreparedStatement pst;
            ResultSet res;
          Double somme=0.0;
        for (int mapKey : tunisiamallMain.Tunisiamall.getInstance().getPanier().keySet()) 
      {
          try {
            pst = connection.getConnection().prepareStatement("SELECT * FROM produit where id=?");
            pst.setInt(1, mapKey);
            res = pst.executeQuery();
            while(res.next()){
               somme+=res.getDouble(5)*tunisiamallMain.Tunisiamall.getInstance().getPanier().get(mapKey);
            }
   
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
      }
        return String.valueOf(somme);
    }
      public  void passerCommande(String Adresse)
      {System.out.println("ici");
          PreparedStatement pst;
            ResultSet res;
            int idLivraison = 0;
          Double somme=0.0;
          try {
            pst = connection.getConnection().prepareStatement("INSERT INTO `livraison`(`id_client`, `etat`, `DestinationAdresse`,Date_creation) VALUES (?,?,?,?)");
            pst.setInt(1, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
            pst.setString(2,"En Cour");
            pst.setString(3, Adresse);
            pst.setDate(4,new java.sql.Date(System.currentTimeMillis()));
            pst.executeUpdate();
              System.out.println("fin Update");
        } catch (SQLException ex) {
            
              System.out.println(ex);
        }
          
          try {
            pst = connection.getConnection().prepareStatement("select max(id) from livraison");
            res = pst.executeQuery();
            while(res.next()){
               idLivraison=res.getInt(1);
            }
   
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
          
          System.out.println("idLivraison="+idLivraison);
        for (int mapKey : tunisiamallMain.Tunisiamall.getInstance().getPanier().keySet()) 
      {System.out.println("boucle");
          try {
            pst = connection.getConnection().prepareStatement("INSERT INTO `commande_produit`(`idLivraison`, `idProduit`, `quantite`) VALUES (?,?,?)");
            pst.setInt(1, idLivraison);
            pst.setInt(2, mapKey);
            pst.setDouble(3, tunisiamallMain.Tunisiamall.getInstance().getPanier().get(mapKey));
            pst.executeUpdate();
              System.out.println("fin Insert Commande");
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
          
          try {
            pst = connection.getConnection().prepareStatement("update utilisateurs set Solde=? where id=?");
           tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().setSolde(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getSolde()-getPrixArticleTotal(mapKey, tunisiamallMain.Tunisiamall.getInstance().getPanier().get(mapKey)));
            pst.setDouble(1,tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getSolde());
            pst.setDouble(2, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
            pst.executeUpdate();
              System.out.println("fin moin solde");
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
          try {
            pst = connection.getConnection().prepareStatement("update utilisateurs set Solde=solde+? where id=?");

            pst.setDouble(1, getPrixArticleTotal(mapKey, tunisiamallMain.Tunisiamall.getInstance().getPanier().get(mapKey)));
            pst.setDouble(2, getIdResponsable(mapKey));
            pst.executeUpdate();
              System.out.println("fin plus solde");
        } catch (SQLException ex) {
            ex.getErrorCode();
        } 
      }
       
    }
         
}
