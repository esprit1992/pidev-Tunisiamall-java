/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ENTITY.Boutique;
import ENTITY.Carte_fidelite;
import ENTITY.Utilisateur;
import INTERFACES.Icartefide;
import UTILS.DataSource;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;


/**
 *
 * @author siwar-pc
 */
public class dCarte implements  Icartefide{
private Connection connection;

    
    
    public dCarte(){
        connection= DataSource.getInstance().getConnection();
    }
    public void addcarte(Utilisateur p, String g,Boutique b) {

     try {
            PreparedStatement pst=connection.prepareStatement("insert into carte_fidelite(id_client,id_boutique,nombre_points,qrcodeclient) values(?,?,0,?) ");
            pst.setInt(1, p.getId());
            pst.setInt(2, b.getId());
            File blobFile = new File(g);
            InputStream in = new FileInputStream(blobFile);
            pst.setBinaryStream(3, in, (int)blobFile.length());
            pst.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace();        
        } catch (FileNotFoundException ex) {
        Logger.getLogger(dCarte.class.getName()).log(Level.SEVERE, null, ex);
    }
        
    
    
    
    }
public ObservableList<Carte_fidelite> getcartes(){
    List<Carte_fidelite> list= new ArrayList<Carte_fidelite>();
        
    String sql = "SELECT id, id_client,id_boutique,nombre_points FROM carte_fidelite";
 try{
Statement statement = connection.createStatement();
ResultSet result = statement.executeQuery(sql);
 
while (result.next()){
    Boutique b= new Boutique();
    Utilisateur u= new Utilisateur();
    u.setId(result.getInt(2));
    b.setId(result.getInt(3));
Carte_fidelite carte=new Carte_fidelite();
  carte.setId(result.getInt(1)); 
   carte.setB(b); 
   carte.setU(u); 
   carte.setNombre_points(result.getInt(4));
   
   
   list.add(carte);
}

}
 catch( Exception e)
 {
     
 }
 ObservableList<Carte_fidelite> observableList;
        observableList = FXCollections.observableList(list);
return observableList;
    
}
    public Image getcarte(Utilisateur p,Boutique b) {
        System.out.print("getcarte");
        //Image jj=null;
        WritableImage wr = null;
        
       
   
      

 try{
Statement statement = connection.createStatement();
PreparedStatement pst=connection.prepareStatement("SELECT qrcodeclient FROM carte_fidelite where id_client =? and id_boutique=?");
pst.setInt(1,p.getId());
pst.setInt(2,b.getId());
ResultSet result = pst.executeQuery();

while (result.next()){

 Blob imageBlob = result.getBlob(1);
    
InputStream binaryStream = imageBlob.getBinaryStream(1, imageBlob.length());
  

 BufferedImage image1 = ImageIO.read(binaryStream);
 
        if (image1 != null) {
            wr = new WritableImage(image1.getWidth(), image1.getHeight());
            PixelWriter pw = wr.getPixelWriter();
            for (int x = 0; x < image1.getWidth(); x++) {
                for (int y = 0; y < image1.getHeight(); y++) {
                    pw.setArgb(x, y, image1.getRGB(x, y));
                }
            }
        }

//jj= new ImageIcon(image);
 
/*
 
Bitmap image = BitmapFactory.decodeStream(binaryStream);
imageView.setImageBitmap(image);
*/
}
 
 

    
 }catch( Exception e)
 {
     
 }
 //System.out.print(jj);
    return wr;
    
 
    }

   
    public void removecarte(Utilisateur p,Boutique b) {
try {
            java.sql.Statement st=connection.createStatement();
            PreparedStatement pst=connection.prepareStatement("delete from carte_fidelite where (id_client=? and id_boutique=?)");
            pst.setInt(1,p.getId());
            pst.setInt(2, b.getId());
            pst.executeUpdate();
       
}
catch( Exception e)
 {
   
    }
    

   
    
    }

    
    
    
        public BufferedImage getQRImage(Utilisateur p,Boutique b)
        {
    BufferedImage image = null;  //Buffered image coming from database
        InputStream fis = null; //Inputstream//Inputstream
        ResultSet databaseResults;  //Returned results from DB

        try{

            
            
            PreparedStatement pst=connection.prepareStatement("select qrcodeclient from carte_fidelite where (id_client=? and id_boutique=?)");
            pst.setInt(1,p.getId());
            pst.setInt(2, b.getId());
            ResultSet result = pst.executeQuery();
            result.next();
            fis = result.getBinaryStream("qrcodeclient");  //It happens that the 3rd column in my database is where the image is stored (a BLOB)

            image = javax.imageio.ImageIO.read(fis);  //create the BufferedImaged

        } catch (Exception e){
                 //print error if caught
        }

       return image ; //My function returns a BufferedImage object
        }
    @Override
    public void addcarte(Utilisateur p, String g) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Image getcarte(Utilisateur p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removecarte(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   

    
}

