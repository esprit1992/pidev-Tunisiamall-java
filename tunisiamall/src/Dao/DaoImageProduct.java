/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ENTITY.ImageProduct;
import UTILS.DataSource;
import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Duration;

/**
 *
 * @author user
 */
public class DaoImageProduct {
    
    private DataSource connection;
    private static DaoImageProduct instance;
    public DaoImageProduct(){
        connection= DataSource.getInstance();
    }
      public static DaoImageProduct getInstance() {
        if (instance == null) {
            instance = new DaoImageProduct();
        }
        return instance;
    }
      
      public void  addImage(ImageProduct i) {
          
          try {
                PreparedStatement pst = connection.getConnection().prepareStatement("INSERT INTO `image_produit"
                        + "`(`id_produit`, `chemin`, `active`) "
                        + "VALUES(?,?,?)");
                pst.setInt(1, i.getIdProduct());
                pst.setString(2, i.getChemain());
                pst.setInt(3, i.getActive());
                pst.executeUpdate();
        } catch (SQLException ex) {
                ex.printStackTrace();
                
            }
          System.out.println("Dao.DaoImageProduct.addImage()");
    }
      
      
public Map<Integer, String> ImageOfProduct() {
    
    Map<Integer, String> listeProduit= new HashMap<Integer, String>();
    PreparedStatement pst = null;
    try {
        pst = connection.getConnection().prepareStatement("select id_produit,chemin FROM `image_produit` where active=1 group by id_produit");
      
            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){
    
    listeProduit.put(res.getInt("id_produit"),res.getString("chemin"));
}
    } catch (SQLException ex) {  
    }
 return listeProduit; }



public ArrayList<ImageProduct> ImageOfProduct(int idProduct)
{
    ArrayList<ImageProduct> arrayofimg=new ArrayList<ImageProduct> ();
    
    PreparedStatement pst = null;
    try {
        pst = connection.getConnection().prepareStatement("select * FROM `image_produit` where id_produit=?");
      pst.setInt(1, idProduct);
            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){
    
    arrayofimg.add(new ImageProduct(res.getInt("id"), idProduct, res.getString("chemin"),res.getInt("active")));
       
}
    } catch (SQLException ex) {  
    }
    return arrayofimg;
}


public int getSommeSelectedImageVisible(int idimage)
{
    int somme=0;
    System.out.println("idimage="+idimage);
     PreparedStatement pst = null;
        try {
            pst = connection.getConnection().prepareStatement("select count(*) as somme from image_produit where (active=1 and id_produit=(select id_produit from image_produit where id=?))");
            pst.setInt(1, idimage);
            ResultSet res;
            res = pst.executeQuery();
            if(res.next())
            somme=res.getInt("somme");
        } catch (SQLException e) {
             System.out.println("Dao.DaoImageProduct.updateStatueVisibleImage() Select Count ERROR"+e);
        }
        return somme;
}


public void updateStatueVisibleImage(int idimage,int Statue)
{
    int somme=0;
    PreparedStatement pst = null;
    somme=getSommeSelectedImageVisible(idimage); 
     if(somme==0 || Statue== 0)
      {
      try {
        pst = connection.getConnection().prepareStatement("update image_produit set active=? where id=?");
      pst.setInt(1, Statue);
      pst.setInt(2, idimage);
            ResultSet res;
   pst.executeUpdate();
    }catch(Exception e)
    {
        System.out.println("Dao.DaoImageProduct.updateStatueVisibleImage() ERROR"+e);
    }
}
}

      public void  DeleteImage(int id) {
      PreparedStatement pst;
        try {
            pst = connection.getConnection().prepareStatement("delete from `image_produit` where id=?");
      
                pst.setInt(1, id);
                pst.executeUpdate();
     } catch (SQLException ex) {
            Logger.getLogger(DaoImageProduct.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Dao.DaoImageProduct.DeleteImage()");
        }
          
    }

      public String cheminimageProduit(int idProduct)
{
 
    String ret="";
    PreparedStatement pst = null;
    try {
        pst = connection.getConnection().prepareStatement("select chemin FROM `image_produit` where id_produit=? and active=1");
      pst.setInt(1, idProduct);
            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){
    
   ret=res.getString("chemin");
       System.out.println("chemin="+res.getString("chemin"));
}
    } catch (SQLException ex) {  
    }
   
       
    return ret;
}
      
      
}
      
      

