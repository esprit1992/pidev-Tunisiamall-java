/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ENTITY.Reclamation;
import ENTITY.Utilisateur;
import UTILS.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author mirak
 */
public class DaoReclamation {
    private static DaoReclamation instance;
    private DataSource connection;
    
    
    public DaoReclamation(){
        connection = DataSource.getInstance();
    }
    public static DaoReclamation getInstance(){
         if (instance == null) {
            instance = new DaoReclamation();
        }
        return instance;
    }
    
    
    public void addReclamation(Reclamation r){
        System.out.print(r.getId_user());
        try {
            PreparedStatement pst = connection.getConnection().prepareStatement("INSERT INTO reclamation(id_user,sujet,categorie,traiter) VALUES (?,?,?,?);");
               pst.setInt(1,r.getId_user());
                pst.setString(2,r.getSujet());
                pst.setString(3, r.getCategorie());
               pst.setBoolean(4,r.isTraiter());
         //   PreparedStatement pst = connection.getConnection().prepareStatement("INSERT INTO `tunisiamall`.`reclamation` (`id_user` ,`sujet` ,`categorie` ,`traiter`)VALUES ('6', 'ss', 'qsdqsd', '0');");
           
           pst.executeUpdate();   
        } catch (SQLException ex) {
            ex.getLocalizedMessage();
        }
    }
    public ObservableList<Reclamation> displayReclamation(){
           Reclamation rec=null;
            PreparedStatement pst;
            ResultSet res;
           List<Reclamation> vrec = new ArrayList<Reclamation>();
        try {
            pst = connection.getConnection().prepareStatement("SELECT id, id_user, sujet, categorie, traiter FROM reclamation");
            res = pst.executeQuery();
            while(res.next()){
        
                rec=new Reclamation();
                rec.setId(res.getInt(1));
                rec.setId_user(res.getInt(2));
                rec.setSujet(res.getString(3));
                rec.setCategorie(res.getString(4));
                rec.setTraiter(res.getBoolean(5));
                vrec.add(rec);
            }
   
        } catch (SQLException ex) {
            ex.getErrorCode();
        }
         ObservableList<Reclamation> observableList = FXCollections.observableList(vrec);
        return observableList;
    }
    public void deleteReclamation(Reclamation r){
        try {
            PreparedStatement pst = connection.getConnection().prepareStatement("DELETE FROM reclamation WHERE id=?");
            pst.setInt(1, r.getId());
            pst.executeUpdate();                    
        } catch (SQLException ex) {
            ex.getLocalizedMessage();
        }
    }
    public void updateReclamation(Reclamation r){
        try {
            PreparedStatement pst = connection.getConnection().prepareStatement("UPDATE reclamation SET traiter=true WHERE id=?");
            pst.setInt(1,r.getId());
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoReclamation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
