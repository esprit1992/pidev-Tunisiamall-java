/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import UTILS.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author user
 */
public class DAOCategorieProduit {
    private DataSource connection;

    public DAOCategorieProduit() {
        this.connection = DataSource.getInstance();
    }

    
   public void addNewCategorieProduit(int idProduit,int idCategorie)
    {
        PreparedStatement pst = null;
        try {
            pst = connection.getConnection().prepareStatement("INSERT INTO `categorie_produit` (`id_categorie`,`id_produit`) VALUES (?,?)");
            pst.setInt(1, idCategorie);
            pst.setInt(2, idProduit);
             pst.executeUpdate();
           
        } catch (SQLException e) {
             System.out.println(e);
        }

    }
   

   
     public int getCategorieOfProduct(int idProduit)
    {
        System.out.println("idproduit="+idProduit);
        PreparedStatement pst = null;
         ResultSet res = null;
         int ret = 0;
        try {
            pst = connection.getConnection().prepareStatement("SELECT `id_categorie` FROM `categorie_produit` WHERE (id_produit!=?)");

            pst.setInt(1, idProduit);
             res = pst.executeQuery();
             while(res.next())
             ret=res.getInt("id_categorie");
        } catch (SQLException e) {
             System.out.println(e);
             System.out.println("daocatprod"+ret);
        }
        System.out.println("id_categorie="+ret);
return ret;
    }

    public void updateCategorieProduit(int idprod, int idCategorie) {
        
         PreparedStatement pst = null;
        try {
            pst = connection.getConnection().prepareStatement("update  `categorie_produit` set `id_categorie`=? where (`id_produit`!=?)");
            pst.setInt(1, idCategorie);
            pst.setInt(2, idprod);
             pst.executeUpdate();
           
        } catch (SQLException e) {
             System.out.println(e+"iciiiiiii");
        }
        
    }
   
}



