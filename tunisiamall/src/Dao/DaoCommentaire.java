package Dao;

import ENTITY.Commentaire;
import UTILS.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class DaoCommentaire {
private DataSource connection;
    public void CheckThred(String nameBoutique,String idProduit)
    {connection= DataSource.getInstance();
        PreparedStatement pst;
        ResultSet res;
         
    try {
        pst = connection.getConnection().prepareStatement("select * from `thread` where `id`=?");
   
            pst.setString(1, nameBoutique+idProduit);
            res = pst.executeQuery();
            if (!res.last())//kan mal9ach thred
            {
                insertnewThred(nameBoutique,idProduit);
            }
     } catch (SQLException ex) {
        Logger.getLogger(DaoCommentaire.class.getName()).log(Level.SEVERE, null, ex);
    }
}
    void insertnewThred(String nameBoutique,String idProduit)
    {
        connection= DataSource.getInstance();
        PreparedStatement pst;
      
         
    try {
        pst = connection.getConnection().prepareStatement("INSERT INTO `thread`(`id`, `permalink`, `is_commentable`, `num_comments`) VALUES (?,?,1,0)");
   
            pst.setString(1, nameBoutique+idProduit);
            pst.setString(2, "http://localhost/pidev/web/app_dev.php/boutique/"+nameBoutique+"/"+idProduit+"");
            pst.executeUpdate();
      } catch (SQLException ex) {
        Logger.getLogger(DaoCommentaire.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    

    
    public void addCommentaire(Commentaire c)
    {
        connection= DataSource.getInstance();
        PreparedStatement pst;
        ResultSet res;
         
    try {
        pst = connection.getConnection().prepareStatement("INSERT INTO `comment`(`thread_id`, `body`, `depth`, `created_at`, `state`, `author_id`, `score`,`ancestors`) VALUES (?,?,0,NOW(),0,?,0,'')");
   
            pst.setString(1, c.getId_Thred());
            pst.setString(2, c.getBody());
            pst.setInt(3, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
            pst.executeUpdate();
      } catch (SQLException ex) {
        Logger.getLogger(DaoCommentaire.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    try {
        pst = connection.getConnection().prepareStatement("UPDATE `thread` SET `num_comments`=`num_comments`+1  WHERE id=?");
   
            pst.setString(1, c.getId_Thred());
            pst.executeUpdate();
      } catch (SQLException ex) {
        Logger.getLogger(DaoCommentaire.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    }
    
            public ArrayList<Commentaire> getlisteCommentaire(String nameBoutique,String idProduit) {
                connection= DataSource.getInstance();
    ArrayList<Commentaire> listeCommentaire= new ArrayList<>();
    PreparedStatement pst = null;
    String id=nameBoutique+idProduit;
    try {
        pst = connection.getConnection().prepareStatement("select c.state,t.id as tid,t.permalink,t.num_comments,c.id,c.body,c.created_at,c.author_id  from thread t,comment c where c.state=0 and c.thread_id=t.id and t.id=?");
        pst.setString(1,id);
            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){
   listeCommentaire.add(new Commentaire(res.getInt("id"), res.getString("tid"),res.getString("body"),res.getDate("created_at"),res.getInt("state"),res.getInt("author_id"),res.getString("permalink"),res.getInt("num_comments")));
}
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
 return listeCommentaire; }
    
    
            
            
      public void deleteCommentaire(int idcmt,String idthred)
    {
        System.out.println("idcmt="+idcmt+"idthred="+idthred);
        connection= DataSource.getInstance();
        PreparedStatement pst;
        ResultSet res;
         
    try {
        pst = connection.getConnection().prepareStatement("DELETE FROM `comment` WHERE id=?");
   
            pst.setInt(1, idcmt);
         
            pst.executeUpdate();
      } catch (SQLException ex) {
        Logger.getLogger(DaoCommentaire.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    try {
        pst = connection.getConnection().prepareStatement("UPDATE `thread` SET `num_comments`=`num_comments`-1,last_comment_at=NOW()  WHERE id=?");
   
            pst.setString(1, idthred);
            pst.executeUpdate();
      } catch (SQLException ex) {
        Logger.getLogger(DaoCommentaire.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    }
            
            
             public void UpdateComment(String  id,String body)
    {
        connection= DataSource.getInstance();
        PreparedStatement pst;
        ResultSet res;
         
    try {
        pst = connection.getConnection().prepareStatement("UPDATE `comment` SET `body`=? where `id`=?");
   
            pst.setString(1, body);
         pst.setString(2, id);
           pst.executeUpdate();
      } catch (SQLException ex) {
        Logger.getLogger(DaoCommentaire.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
            
            
            
            
}


