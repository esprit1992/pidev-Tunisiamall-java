package Dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import ENTITY.Boutique;

import INTERFACES.IBoutique;
import UTILS.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author siwar-pc
 */
public class DBoutique implements IBoutique{
private DataSource connection;
    public void addboutique(Boutique b) {
        try {
            //        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
            
            PreparedStatement ps = connection.getConnection().prepareStatement("insert into Boutique (nom,id_responsable,description,image) values (?,?,?,?)");
            ps.setString (1, b.getNom());
            ps.setInt(2, b.getIdresponsable());
            ps.setString (3, b.getDescription());
            ps.setString (4, b.getImage());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("    ajout non effectue");    
        //Logger.getLogger(BoutiqueDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 
        }
    
    
    public DBoutique(){
        connection= DataSource.getInstance();
    }
    public void addboutique() {
        }

    public void updateboutique(Boutique b) {
        
    }


    public String deleteboutique(Integer id) {
      String result=null;
        try
        {
            PreparedStatement ps=connection.getConnection().prepareStatement("delete from boutique where id=?  ");
            ps.setInt(1, id);
            ps.executeUpdate();
        }catch(SQLException e){
            result=e.getMessage();
        }
        return result;   
    }

    public ArrayList<Boutique> getBoutiques() {
       
    ArrayList<Boutique> jj= new ArrayList<Boutique>();
         
    String sql = "SELECT id,nom FROM Boutique";
 try{
Statement statement = connection.getConnection().createStatement();
ResultSet result = statement.executeQuery(sql);
 
while (result.next()){
    
    
Boutique kk=new Boutique();
  kk.setId(result.getInt(1)); 
   kk.setNom(result.getString(2)); 
   
   jj.add(kk);
}
 }catch( Exception e)
 { 
 }
 return jj; }

    

    public Boutique getboutique(int id) {
             Boutique kk=new Boutique();
    String sql = "SELECT * FROM Boutique where id="+id;
 try{
Statement statement = connection.getConnection().createStatement();
ResultSet result = statement.executeQuery(sql);
 
while (result.next()){
  kk.setId(result.getInt(1)); 
   kk.setNom(result.getString(3)); 
   kk.setDescription(result.getString(4)); 
   //kk.set(result.getString(2)); 
} 
}catch( Exception e)
 {
     
 }
 return kk;
    } 


    public  Boutique getboutiquee(String name) {
               Boutique boutique=null;
               ResultSet res;
    try {
        PreparedStatement pst = connection.getConnection().prepareStatement("SELECT * FROM `Boutique` where `nom`=?");
        pst.setString(1, name);
        res = pst.executeQuery();
            if (res.last()) {
               boutique=new Boutique();
               boutique.setNom(res.getString("nom"));
               boutique.setId(res.getInt("id"));
               
            }

    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
    return boutique;
    } 
    
    
     public ArrayList<String> getBoutiquesnamefromidresponsable(int id) {
    
    ArrayList<String> jj= new ArrayList<String>();
    PreparedStatement pst = null;
    try {
        pst = connection.getConnection().prepareStatement("SELECT nom FROM Boutique where id_responsable=?");
        pst.setInt(1, id);
            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){
 
   jj.add(res.getString("nom"));
}
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
 return jj; }

    @Override
    public void updateboutique() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        public String getimageboutique(int id)
    {
   
    String img="";
        try
        {
            PreparedStatement st=connection.getConnection().prepareStatement("SELECT `image` FROM `boutique` WHERE id=?");
            st.setInt(1, id);
            ResultSet rs=st.executeQuery();
         rs.next();
         
            img=rs.getString("image");
             
        }catch (SQLException e){
            System.out.println("error :"+e.getMessage());
        }
        
        return img;
    }

    @Override
    public void deleteboutique() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     public ObservableList<Boutique> getboutiqueObservabol(Integer id) {
        List<Boutique> list = new ArrayList<Boutique>();
 
        // Now add observability by wrapping it with ObservableList.
    
        
        try
        {
            PreparedStatement pst = connection.getConnection().prepareStatement("select * from boutique where id_responsable=?");
        pst.setInt(1, id);
            ResultSet rs;
   rs = pst.executeQuery();
         
            while(rs.next())
            {
                Boutique boutique=new Boutique();
                boutique.setId(rs.getInt(1));
                boutique.setNom(rs.getString(3));
                boutique.setDescription(rs.getString(4));
                //boutique.setImage(rs.getString(5));
                list.add(boutique);
            }
        }catch (SQLException e){
            System.out.println("error :"+e.getMessage());
        }
        ObservableList<Boutique> observableList = FXCollections.observableList(list);
        return observableList;
    }
     
    public ObservableList<Boutique> advancedsearchboutique(String nom) {
        List<Boutique> list = new ArrayList<Boutique>();
 
        // Now add observability by wrapping it with ObservableList.
    
        
        try
        {

            PreparedStatement st=connection.getConnection().prepareStatement("select * from boutique where nom like '%' ? '%'");
            st.setString(1, nom);
            ResultSet rs=st.executeQuery();
         
            while(rs.next())
            {   
                Boutique boutique=new Boutique();
                boutique.setId(rs.getInt(1));
                boutique.setNom(rs.getString(3));
                boutique.setDescription(rs.getString(4));
                //boutique.setImage(rs.getString(5));
                list.add(boutique);
            }
        }catch (SQLException e){
            System.out.println("error :"+e.getMessage());
            //System.out.println("erreur :p");
        }
        ObservableList<Boutique> observableList = FXCollections.observableList(list);
        return observableList;
    }
}
