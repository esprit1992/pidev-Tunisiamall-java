/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ENTITY.Utilisateur;
import UTILS.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import UTILS.*;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

/**
 *
 * @author user
 */
public class DaoUtilisateur {

    private static DaoUtilisateur instance;
    private DataSource connection;

    public DaoUtilisateur() {
        connection = DataSource.getInstance();
    }

    public static DaoUtilisateur getInstance() {
        if (instance == null) {
            instance = new DaoUtilisateur();
        }
        return instance;
    }

    public boolean verifPassword(String password)
    {
        
    PreparedStatement pst;
        ResultSet res;
        Boolean ret=false;
        System.out.println("passwordis"+password);
        try {
            pst = connection.getConnection().prepareStatement("select * from `utilisateurs` where `id`=?");
            pst.setInt(1, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
            res = pst.executeQuery();
            if (res.last())//kan il9a il user
            {
                if (BCrypt.checkpw(password, res.getString("password"))) 
                ret=true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
        }
         return ret;
    }
    
    public int verifforadduser(Utilisateur u) {
        int ret = 0;
        PreparedStatement pst;
        try {
            pst = connection.getConnection().prepareStatement("select * from `utilisateurs` where `username_canonical`=?");
            pst.setString(1, u.getUsername_canonical());
            if (pst.executeQuery().next()) {
                ret = 1; //userexiste
            }
            pst = connection.getConnection().prepareStatement("select * from `utilisateurs` where `email_canonical`=?");
            pst.setString(1, u.getEmail_canonical());
            if (pst.executeQuery().next()) {
                ret = 2; //email existe
            }
            pst = connection.getConnection().prepareStatement("select * from `utilisateurs` where `tel`=?");
            pst.setInt(1, u.getTel());
            if (pst.executeQuery().next()) {
                ret = 3; //numberphoneexiste
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }
    
    
    
       public int verifforedituser(Utilisateur u) {
        int ret = 0;
        PreparedStatement pst;
        try {
       
            pst = connection.getConnection().prepareStatement("select * from `utilisateurs` where `email_canonical`=? and username<>?");
            pst.setString(1, u.getEmail_canonical());
            pst.setString(2, u.getUsername());
            if (pst.executeQuery().next()) {
                ret = 1; //email existe
            }
            pst = connection.getConnection().prepareStatement("select * from `utilisateurs` where `tel`=? and username<>?");
            pst.setInt(1, u.getTel());
             pst.setString(2, u.getUsername());
            if (pst.executeQuery().next()) {
                ret = 2; //numberphoneexiste
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }
    
    

    public int  adduser(Utilisateur u) {
        int ret = verifforadduser(u);
        if (ret == 0) {
            try {

                PreparedStatement pst = connection.getConnection().prepareStatement("INSERT INTO `utilisateurs"
                        + "`(`username`, `username_canonical`, `email`, `email_canonical`, `enabled`, "
                        + "`salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`"
                        + ", `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, "
                        + "`adresse`, `nom`, `prenom`, `sex`, `age`, `tel`, `civilite`, `Solde`) "
                        + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
                pst.setString(1, u.getUsername());
                pst.setString(2, u.getUsername_canonical());
                pst.setString(3, u.getEmail());
                pst.setString(4, u.getEmail_canonical());
                pst.setInt(5, u.getEnabled());
                pst.setString(6, u.getSalt());
                pst.setString(7, u.getPassword());
                pst.setDate(8, u.getLast_login());
                pst.setBoolean(9, u.getLocked());
                pst.setBoolean(10, u.getExpired());
                pst.setDate(11, u.getExpires_at());
                pst.setString(12, u.getConfirmation_token());
                pst.setDate(13, u.getPassword_requested_at());
                pst.setString(14, u.getRoles());
                pst.setBoolean(15, u.getCredentials_expired());
                pst.setDate(16, u.getCredentials_expire_at());
                pst.setString(17, u.getAdresse());
                pst.setString(18, u.getNom());
                pst.setString(19, u.getPrenom());
                pst.setString(20, u.getSex());
                pst.setInt(21, u.getAge());
                pst.setInt(22, u.getTel());
                pst.setString(23, u.getCivilite());
                pst.setDouble(24, u.getSolde());
                pst.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
                ret = 4;
            }
        }
        return ret;
    }

    public int login(String username, String password) {
        Utilisateur user = null;
        PreparedStatement pst;
        ResultSet res;

        try {
            pst = connection.getConnection().prepareStatement("select * from `utilisateurs` where `username_canonical`=?");
            pst.setString(1, username);
            res = pst.executeQuery();
            if (res.last())//kan il9a il user
            {
                if (BCrypt.checkpw(password, res.getString("password"))) {
                    if (res.getInt("enabled") == 0) {
                        return 4;//login correct but compte blocker
                    }
                    user = new Utilisateur(res.getInt("id"),res.getString("username"), res.getString("email"), res.getString("roles"), res.getString("adresse"), res.getString("nom"), res.getString("prenom"), res.getInt("age"), res.getInt("tel"), res.getString("civilite"),res.getDouble("Solde"),res.getString("sex"),res.getString("password"));
                    tunisiamallMain.Tunisiamall.getInstance().setLoggedUser(user);
                    return 3;//login correct
                } else {
                    return 2;//user s7i7 pass 8alet
                }
            } else {
                return 1;//user mal9ahech
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
            return 0;//error
        }

    }
    
     public ObservableList<Utilisateur> getusers() {
          
        List<Utilisateur> listuser= new ArrayList<Utilisateur>();
         
    String sql = "SELECT id, nom, prenom, age, adresse, email, username,enabled,roles FROM Utilisateurs where roles not like '%ROLE_ADMIN%'";
 try{
Statement statement = connection.getConnection().createStatement();
ResultSet result = statement.executeQuery(sql);
 String role;
while (result.next()){
    if(result.getString(9).equals("a:0:{}")){
        
    role="Client";
}else{
            role="Responsable";

    }
    
Utilisateur kk=new Utilisateur();
  kk.setId(result.getInt(1)); 
   kk.setNom(result.getString(2)); 
   kk.setPrenom(result.getString(3)); 
   kk.setAdresse(result.getString(5));
   kk.setAge(result.getInt(4));
   kk.setUsername(result.getString(7));
   kk.setEmail(result.getString(6));
   kk.setEnabled(result.getInt(8));
   kk.setRoles(role);
  
   
   listuser.add(kk);
    
    }
 }catch( Exception e)
 {
     
 }
 ObservableList<Utilisateur> observableList;
        observableList = FXCollections.observableList(listuser);
return observableList;

}
     
     public void acceptuser(int u) {
        
         System.out.println("unlock"+u);
     try {
           
            PreparedStatement pst=connection.getConnection().prepareStatement("update  Utilisateurs set enabled= 1 where id = ? ");
            pst.setInt(1, u);
            
            pst.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace();        
        }
    
    }
public void desactiveuser(int u){
     System.out.println("lock"+u);
    try {
          
            PreparedStatement pst=connection.getConnection().prepareStatement("update  Utilisateurs set enabled= 0 where id = ? ");
            pst.setInt(1, u);
            
            pst.executeUpdate();
        } catch (SQLException ex) {
           ex.printStackTrace();        
        }
    
}

public ObservableList<Utilisateur> getusersclient(){
    List<Utilisateur> listuser= new ArrayList<Utilisateur>();
         String u="a:0:{}";
    String sql = "SELECT id, nom, prenom,email FROM Utilisateurs where roles='a:0:{}'";
 try{
Statement statement = connection.getConnection().createStatement();
ResultSet result = statement.executeQuery(sql);
 
while (result.next()){
    
Utilisateur kk=new Utilisateur();
  kk.setId(result.getInt(1)); 
   kk.setNom(result.getString(2)); 
   kk.setPrenom(result.getString(3)); 
   kk.setEmail(result.getString(4));
   
   
   listuser.add(kk);
    
    }
 }catch( Exception e)
 {
     
 }
 ObservableList<Utilisateur> observableList;
        observableList = FXCollections.observableList(listuser);
return observableList;
    
}


public String getNameUserFromId(int id) {
        Utilisateur user = null;
        PreparedStatement pst;
        ResultSet res;
        String ret="";
        try {
            pst = connection.getConnection().prepareStatement("SELECT nom, prenom FROM Utilisateurs where id=?");
            pst.setInt(1, id);
            res =pst.executeQuery();
            if (res.last())//kan il9a il user
            {
                ret=res.getString("nom")+" "+res.getString("prenom");
            } 
        } catch (SQLException ex) {
            Logger.getLogger(DaoUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
            return "erreur";
        }
return ret;
    }



    public int  updateUser(Utilisateur u) {
        int ret=0;
       
            try {

                PreparedStatement pst = connection.getConnection().prepareStatement("UPDATE utilisateurs SET username=?,username_canonical=?,email=?,email_canonical=?,adresse=?,nom=?,prenom=?,sex=?,age=?,tel=?,civilite=? WHERE id=?");
                pst.setString(1, u.getUsername());
                pst.setString(2, u.getUsername());
                pst.setString(3, u.getEmail());
                pst.setString(4, u.getEmail());
                pst.setString(5, u.getAdresse());
                pst.setString(6, u.getNom());
                pst.setString(7, u.getPrenom());
                pst.setString(8, u.getSex());
                pst.setInt(9, u.getAge());
                pst.setInt(10, u.getTel());
                pst.setString(11, u.getCivilite());
                pst.setInt(12, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
                pst.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.err.println("ici ya 5ra?");
                ret = 1;
            }
             return ret;
        }
       
    
public XYChart.Series<String, Integer> StatAdminUserAge()
    {
    try {
        XYChart.Series<String,Integer> series=new XYChart.Series<>();
        PreparedStatement st=connection.getConnection().prepareStatement("SELECT\n" +
"    CASE\n" +
"        WHEN age BETWEEN 10 and 19 THEN '10 - 19'\n" +
"        WHEN age BETWEEN 20 and 29 THEN '20 - 29'\n" +
"        WHEN age BETWEEN 30 and 39 THEN '30 - 39'\n" +
"        WHEN age BETWEEN 40 and 49 THEN '40 - 49'\n" +
"        WHEN age BETWEEN 50 and 59 THEN '50 - 59'\n" +
"        WHEN age BETWEEN 60 and 69 THEN '60 - 69'\n" +
"        WHEN age BETWEEN 70 and 79 THEN '70 - 79'\n" +
"        WHEN age BETWEEN 80 and 89 THEN '80 - 89'\n" +
"        WHEN age >= 90 THEN 'Over 90'\n" +
"        WHEN age IS NULL THEN 'Not Filled In (NULL)'\n" +
"    END as age_range,\n" +
"    COUNT(*) AS count\n" +
"\n" +
"    FROM (SELECT age FROM utilisateurs) as derived\n" +
"\n" +
"    GROUP BY age_range\n" +
"\n" +
"    ORDER BY age_range");
        
        ResultSet rs=st.executeQuery();
        while (rs.next())
        {
            series.getData().add(new XYChart.Data<>(rs.getString(1),rs.getInt(2)));
           
            
        }
        return series;
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
        return null;
    }



public ObservableList<PieChart.Data> getUserChartByCivilite() {

        ArrayList<PieChart.Data> list = new ArrayList<PieChart.Data>();
        
    try {
      
        PreparedStatement st=connection.getConnection().prepareStatement("select count(*),civilite from utilisateurs group by civilite");
        
        ResultSet rs=st.executeQuery();
        while (rs.next())
        {
            list.add(new PieChart.Data(rs.getString(2),rs.getInt(1)));
        }
       ObservableList<PieChart.Data> observableList;
        observableList = FXCollections.observableList(list);
        System.out.println("ici"+observableList.size());
        return observableList;
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
   
        return null;

    }



public XYChart.Series<String, Integer> StatResponsable()
    {
        XYChart.Series<String,Integer> series=new XYChart.Series<>();
    try {
        
        PreparedStatement st=connection.getConnection().prepareStatement("select b.nom,sum(c.quantite) from commande_produit c,produit p,boutique b where c.idproduit=p.id and p.id_boutique=b.id and b.id_responsable=?");
        st.setInt(1, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
        ResultSet rs=st.executeQuery();
        while (rs.next())
        {
            series.getData().add(new XYChart.Data<>(rs.getString(1),rs.getInt(2)));
           
            
        }
        return series;
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
       series.getData().add(new XYChart.Data<>(null,null));
        return series;
    }


public XYChart.Series<String, Integer> StatUtilisateur()
    {
        XYChart.Series<String,Integer> series=new XYChart.Series<>();
    try {
        
        PreparedStatement st=connection.getConnection().prepareStatement("select b.nom,sum(c.quantite) from livraison l,commande_produit c,produit p,boutique b where   c.idlivraison=l.id and l.id_client=? and c.idproduit=p.id and p.id_boutique=b.id");
        st.setInt(1, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
        ResultSet rs=st.executeQuery();
        while (rs.next())
        {
            series.getData().add(new XYChart.Data<>(rs.getString(1),rs.getInt(2)));
           
            
        }
        series.setName("");
        return series;
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
        series.setName("null");
        return series;
    }



}
