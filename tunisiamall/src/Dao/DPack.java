/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import ENTITY.Carte_fidelite;
import ENTITY.Pack;
import ENTITY.Reservation;
import ENTITY.Utilisateur;
import INTERFACES.Ipack;
import UTILS.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author siwar-pc
 */
public class DPack implements Ipack {

    private Connection connection;

    public DPack() {
        connection = DataSource.getInstance().getConnection();
    }

    public void addpack(Pack p) {

        try {
            java.sql.Statement st = connection.createStatement();
            PreparedStatement pst = connection.prepareStatement("insert into pack_pub(Description,prixjour,disponible) values(?,?,?) ");
            pst.setString(1, p.getDescription());
            pst.setDouble(2, p.getPrixjour());
            pst.setInt(3, p.getDisponible());
            pst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public int getmaxid() {
        int max = 0;
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("select max(id) from pack_pub");

            while (result.next()) {
                max = result.getInt(1);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return max;
    }

    public void deletepack(Pack p) {
        try {

            java.sql.Statement st = connection.createStatement();
            PreparedStatement pst = connection.prepareStatement("delete from  pack_pub  where id = ? ");
            pst.setInt(1, p.getId());
            pst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void updatepack(Pack p) {

        try {
            java.sql.Statement st = connection.createStatement();
            PreparedStatement pst = connection.prepareStatement("update  pack_pub set Description= ? , prixjour= ? , disponible = ? where id = ? ");
            pst.setString(1, p.getDescription());
            pst.setDouble(2, p.getPrixjour());
            pst.setInt(3, p.getDisponible());
            pst.setInt(4, p.getId());
            pst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public ObservableList<Pack> getpack() {

        ArrayList<Pack> list = new ArrayList<Pack>();

        String sql = "SELECT * FROM pack_pub";
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()) {

                Pack kk = new Pack();
                kk.setId(result.getInt(1));
                kk.setDescription(result.getString(2));
                kk.setDisponible(result.getInt(4));
                kk.setPrixjour(result.getDouble(3));

                list.add(kk);

            }

        } catch (Exception e) {

        }
        ObservableList<Pack> observableList;
        observableList = FXCollections.observableList(list);
        return observableList;

    }
    
    
             public ArrayList<Pack> getlistepack() {
    
    ArrayList<Pack> listePack= new ArrayList<>();
    PreparedStatement pst = null;
    try {
        pst = connection.prepareStatement("SELECT * FROM `pack_pub`");
            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){     
   listePack.add(new Pack(res.getInt("id"),res.getString("Description"),res.getDouble("prixjour"),res.getInt("disponible"),res.getInt("type")));
}
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
 return listePack; }
             
             
             public ArrayList<Reservation> getlistereservation() {
    
    ArrayList<Reservation> listereservation= new ArrayList<>();
    PreparedStatement pst = null;
    try {
        pst = connection.prepareStatement("SELECT * FROM `reservation` where datefin > now()");
            ResultSet res;
   res = pst.executeQuery();
     
while (res.next()){     
   listereservation.add(new Reservation(res.getInt("id"),res.getInt("id_pack"),res.getInt("id_responsable"),res.getString("id_produit"),res.getDate("datefin")));
}
    } catch (SQLException ex) {
        Logger.getLogger(DBoutique.class.getName()).log(Level.SEVERE, null, ex);
    }
 return listereservation; }
             
             
             
             
             
    
    public void BayPack(int id_pub,Date datefin,Double somme){

        PreparedStatement pst = null;
        try {
            pst = connection.prepareStatement("update  `pack_pub` set `disponible`=0 where (`id`=?)");
            pst.setInt(1, id_pub);
            
             pst.executeUpdate();
           
        } catch (SQLException e) {
             System.out.println(e+"iciiiiiii");
        }
        
        try {
            pst = connection.prepareStatement("INSERT INTO `reservation`(`id_pack`, `id_responsable`, `datefin`) VALUES (?,?,?)");
            pst.setInt(1, id_pub);
            pst.setInt(2, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
            pst.setDate(3, datefin);
             pst.executeUpdate();
           
        } catch (SQLException e) {
             System.out.println(e+"iciiiiiii");
        }
        try {
          
            pst = connection.prepareStatement("update utilisateurs set Solde=Solde-? where (id=?)");
            pst.setDouble(1, somme);
            pst.setInt(2, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
          
             pst.executeUpdate();
           
        } catch (SQLException e) {
             System.out.println(e+"iciiiiiii");
        }
         try {
          
            pst = connection.prepareStatement("update utilisateurs set Solde=Solde+? where username='admin'");
            pst.setDouble(1, somme);
          
          
             pst.executeUpdate();
           
        } catch (SQLException e) {
             System.out.println(e+"iciiiiiii");
        }
    }

    
          public ArrayList<String> getListeImage(int id_pack) {
    
    ArrayList<String> listeimage = new ArrayList<>();
    PreparedStatement pst = null;
    String produitid="0";
    String[]listeidproduit=null;
    try {
        pst = connection.prepareStatement("SELECT id_produit FROM `reservation` where id_pack=? and datefin>now()");
        pst.setInt(1, id_pack);
            ResultSet res;
   res = pst.executeQuery();
     
      
            while (res.next()){
                produitid=res.getString("id_produit");
                
            }       } catch (SQLException ex) {
            Logger.getLogger(DPack.class.getName()).log(Level.SEVERE, null, ex);
        }
              System.out.println("id_produit="+produitid);
              if(produitid==null||produitid.equals("")||produitid.equals("null"))
                produitid="0";  
    listeidproduit=produitid.split(",");
    for(String str : listeidproduit)
    {
       produitid=new DaoImageProduct().cheminimageProduit(Integer.valueOf(str));
       
        if(produitid.equals(""))
            produitid="empty";
        listeimage.add(produitid);
    }
          
    
              
    return listeimage; 
   
          }
          
          
            public void UpdateReservation(String id_Produit,int id_pack) {

        try {
            java.sql.Statement st = connection.createStatement();
            PreparedStatement pst = connection.prepareStatement("UPDATE `reservation` SET `id_produit`=? where `datefin`>now() and `id_pack`=? and`id_responsable`=?");
            pst.setString(1, id_Produit);
            pst.setInt(2, id_pack);
            pst.setInt(3, tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId());
            pst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
    }
          
}
 
    
    



