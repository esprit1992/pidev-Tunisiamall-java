/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testhash;

/**
 *
 * @author user
 */
public class Testhash {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String plainpass = "siwars";
        String hashpass = "$2y$10$d2WnDC1jec3YOkblxp1utOwWRRXIUfjH18UgrpBfBmoFLJyhSCI1a";
        //System.out.println(BCrypt.checkpw(plainpass,hashpass));
        String salt = BCrypt.gensalt(13);
        System.out.println(salt + "\n");
        System.out.println(BCrypt.hashpw("Slaheddine", salt));
    }

}
