/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Dao.DBoutique;
import Dao.DProduit;
import Dao.DaoImageProduct;
import ENTITY.ImageProduct;
import ENTITY.Produit;
import animation.*;
import com.github.plushaze.traynotification.animations.Animations;

import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ControllAddProduct implements Initializable {

    @FXML
    private Label LBsexcr;
    @FXML
    private Label LBsexcr1;
    @FXML
    private ComboBox<String> CBStore;
    @FXML
    private Label LBsexcr11;
    @FXML
    private Label LBsexcr12;
    @FXML
    private Label LBsexcr121;
    @FXML
    private Label LBsexcr1211;
    @FXML
    private Label LBsexcr12111;
    @FXML
    private TextField TFproductName;  
    @FXML
    private TextField TFQuantite;
    @FXML
    private CheckBox CheckBoxNewCollection;
    @FXML
    private TextField TFbarcode;
    @FXML
    private TextArea TAdescription;
    @FXML
    private TextField TFPrice;
    @FXML//ici
    private Button ButtonScanCodeAbarre;
    @FXML
    private Button ButtonAddProduct;
    @FXML
    private Label LBsexcr1212;
    @FXML
    private Button ButtonChoseImageToUpload;
    @FXML
    private AnchorPane AnchoreUpload;
    @FXML
    private ImageView LBDisplayImage;
    @FXML
    private Button ButtonUpload;
    File file =null;
    Map<String, String> listeImageToUpload = new HashMap<String, String>();
    FileChooser fileChooser = new FileChooser();
    @FXML
    private ImageView imgLoad;
    @FXML
    private ProgressBar bar;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       AnchoreUpload.setOpacity(0);
        
       
        CBStore.getItems().addAll(new DBoutique().getBoutiquesnamefromidresponsable(tunisiamallMain.Tunisiamall.getInstance().getLoggedUser().getId()));
        
        
          ButtonChoseImageToUpload.setOnAction(
            new EventHandler<ActionEvent>() {
                @Override
                public void handle(final ActionEvent e) {
                   file = fileChooser.showOpenDialog(tunisiamallMain.Tunisiamall.getInstance().getStage());
                    if (file != null) {
                        
                        new FadeInDowntransition(AnchoreUpload).play();
                        try {
                            LBDisplayImage.setImage(new Image(file.toURI().toURL().toString(),223,186,false,false));
                        } catch (MalformedURLException ex) {
                            Logger.getLogger(ControllAddProduct.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                  
                    
                }
                
            });
     
    }    

    @FXML
    private void ButtonScanCodeAbarre(MouseEvent event) {
        new Thread(new WebcamScanController(this));
        
    }

    @FXML
    private void ButtonAddProduct(MouseEvent event) {
        int  newcollection;
        ImageProduct img= new ImageProduct();
        int erreur;
        
        if(verifInputChamps())
        {
            Produit p;
             p=new Produit();
             if(CheckBoxNewCollection.isSelected())
                 newcollection=1;
             else
                 newcollection=0;
          try {
                p = new Produit(new DBoutique().getboutiquee(CBStore.getSelectionModel().getSelectedItem().toString()),Integer.parseInt(TFQuantite.getText()), TAdescription.getText(),Double.parseDouble(TFPrice.getText()),newcollection, TFproductName.getText(), TFbarcode.getText());
            } catch (Exception e) {
                final TrayNotification tray = new TrayNotification("Error", "Error creation product", Notifications.ERROR);
                tray.showAndDismiss(Duration.seconds(3));
            }
        
          Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = 30;
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
          final int msgNumber = DProduit.getInstance().addProd(p);
          img.setIdProduct(p.getId());
          service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent eventt) -> {
            imgLoad.setVisible(true);
        });
        service.setOnSucceeded((WorkerStateEvent eventt) -> {
             bar.progressProperty().unbind();
             bar.setProgress(0);
            imgLoad.setVisible(false);
                switch (msgNumber) {
                    case 0:
                        {
                            int i=0;
                         
                            for (String mapKey : listeImageToUpload.keySet()) {
                            img.setChemain(mapKey);
                            img.setActive(i);
                                System.out.println("Controllers.ControllAddProduct.ButtonAddProduct() "+img.getChemain());
                             DaoImageProduct.getInstance().addImage(img);
                              if(i==0)
                                i++;
                            }
                            final TrayNotification tray = new TrayNotification("DONE", "Product Has Been Addet Successfully", Notifications.SUCCESS);
                            tray.showAndDismiss(Duration.seconds(3));
                            break;
                        }
                    case 1:
                        {
                            final TrayNotification tray = new TrayNotification("ERROR", "Product bar Code alredy Exist", Notifications.ERROR);
                            tray.showAndDismiss(Duration.seconds(3));
                            break;
                        }
                    case 2:
                        {
                            final TrayNotification tray = new TrayNotification("ERROR", "Product name alredy Exist", Notifications.ERROR);
                            tray.showAndDismiss(Duration.seconds(3));
                            break;
                        }
                    case 3:
                        {
                            final TrayNotification tray = new TrayNotification("ERROR", "Error In Request", Notifications.ERROR);
                            tray.showAndDismiss(Duration.seconds(3));
                            break;
                        }
                    case 4:
                        {
                            final TrayNotification tray = new TrayNotification("WARNING", "Product has been addet without image", Notifications.WARNING);
                            tray.showAndDismiss(Duration.seconds(3));
                            break;
                        }
                    default:
                        break;
                }
        });
    }
    }

    public void setTFbarcode(String TFbarcode) {
        this.TFbarcode.setText(TFbarcode);
    }
    @FXML
    private void ButtonUpload(MouseEvent event) {
        controllUploadImageProduit httpPost = null;
        listeImageToUpload.put(file.getName(), file.getPath());
        
        try {
            httpPost = new controllUploadImageProduit();
            new FadeInUpTransition(AnchoreUpload).play();
           TrayNotification tray = new TrayNotification("Upload Picture", "Your Picture Was Uploaded Successfully", Notifications.SUCCESS,Animations.POPUP);
           tray.showAndDismiss(Duration.seconds(2));
        } catch (MalformedURLException ex) {    
            TrayNotification tray = new TrayNotification("Upload Picture", "There Was a problem in file upload", Notifications.ERROR,Animations.POPUP);
           tray.showAndDismiss(Duration.seconds(2));
        }
           for (String mapKey : listeImageToUpload.keySet()) {
            httpPost.setFileNames(new String[]{ listeImageToUpload.get(mapKey) });
            httpPost.post();
            System.out.println("=======");
            System.out.println(httpPost.getOutput());   
    }
            
            LBDisplayImage.setImage(null);

    }
    
    public boolean verifInputChamps() {
        int verif = 0;
        String style = " -fx-border-color: red;";

        String styledefault = "-fx-border-color: green;";
        //CheckBoxNewCollection    
        CBStore.setStyle(styledefault);
        TFproductName.setStyle(styledefault);
        TFQuantite.setStyle(styledefault);
        TFbarcode.setStyle(styledefault);
        TAdescription.setStyle(styledefault);
        TFPrice.setStyle(styledefault);
     
           try {
            CBStore.getSelectionModel().getSelectedItem().toString().equals("");
        } catch (Exception e) {
            {
                CBStore.setStyle(style);
                verif = 1;
            }
        }
        
        if (TFproductName.getText().equals("")) {
            TFproductName.setStyle(style);
            verif = 1;
        }
        if (TAdescription.getText().equals("")) {
            TAdescription.setStyle(style);
            verif = 1;
        }
     
      
        
        try {
            if (Integer.parseInt(TFQuantite.getText()) < 0) {
                TFQuantite.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFQuantite.setStyle(style);
                verif = 1;
            }
        }
        
        try {
            if (Long.parseLong(TFbarcode.getText()) < 0) {
                TFbarcode.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFbarcode.setStyle(style);
                verif = 1;
            }
        }
        
        
        try {
            if (Double.compare((Double.parseDouble(TFPrice.getText())), 0.0) <0){
                TFPrice.setStyle(style);
                verif = 1;
            }
        } catch (NumberFormatException e) {
            {
                TFPrice.setStyle(style);
                verif = 1;
            }
        }

        if (verif == 0) {
            return true;
        }
        TrayNotification tray = new TrayNotification("Error", "Verif Your Input Please", Notifications.ERROR);
        tray.showAndDismiss(Duration.seconds(3));
        return false;
    }
}
